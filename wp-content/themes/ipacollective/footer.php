<footer class="site-foot">
	
	<div class="site-foot__upper">
		
		<h3 class="site-foot__title">About</h3>
		<div class="site-foot__content">
			The IPA Collective is owned and operated by <a href="http://www.ipa-connect.com/" target="_blank">IPA Connect</a> out of Hood River, OR USA.
			Please send any questions, concerns, issues, or inquiries to <a href="mailto:andy_marker@ipa-connect.com">Andy Marker</a>.
		</div>
		
		<h3 class="site-foot__title">Privacy</h3>
		<div class="site-foot__content">
			Information gathered here is shared with the brands the user has chosen, as well as kept on file by IPA Connect.
			We will not sell or rent this data to anyone outside of our normal business.
		</div>
		
		<div class="site-foot__links">
			<a href="https://www.linkedin.com/in/andymarker"><i class="fab fa-linkedin-in"></i></a>
		</div>
		
	</div>
	
	
	<div class="site-foot__lower">
		
		<p class="site-foot__copyright">&copy; IPA Connect <?php echo date('Y'); ?></p>
		
	</div>
	
</footer>

<?php wp_footer(); ?>

</body>
</html>