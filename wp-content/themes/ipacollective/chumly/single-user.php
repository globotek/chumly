<?php
/**
 * Template Name: Profile Page
 * Created by Globotek.
 * User: alex
 * Date: 06/11/18
 * Time: 12:13 PM
 */ ?>

<?php do_action( 'chumly_before_content' ); ?>

<div class="wrapper">
	
	<div class="content chunk--double">
		
		<div class="profile-template">
			
			<h1>Your Profile</h1>
			
			<div class="grid">
				
				<div class="grid__item--one-whole grid__item--palm-one-whole grid__item--lap--one-whole grid__item--desk--one-whole">
					
					<section class="user-profile" data-module="chumly-modal">
						
						<header class="user-profile__header">
							
							<?php chumly_get_template( 'user', 'picture' ); ?>
							
							<div class="user-profile__header__content">
								<h2 class="user-profile__heading"><?php echo chumly_profile_field( 'first_name' ) . ' ' . chumly_profile_field( 'last_name' ); ?></h2>
							</div>
						
						</header>
					
					</section>
				
				</div>
							
			</div>
			
			<div class="grid">
				
				<div class="grid__item--one-whole grid__item--palm-one-whole grid__item--lap--one-whole grid__item--desk--one-whole">
					
					<h2>Your Details</h2>
					
					<div class="site-form">
						
						<?php chumly_form_header( NULL, NULL, 'multipart/form-data' ); ?>
						
						<?php chumly_edit_profile(); ?>
						
						<?php chumly_form_footer( 'update_profile', NULL, 'Update Profile' ); ?>
					
					</div>
				
				
				</div>
			
			</div>
		
		</div>
	
	</div>

</div>
<div id="ajax_testing"></div>
<?php do_action( 'chumly_after_content' ); ?>
