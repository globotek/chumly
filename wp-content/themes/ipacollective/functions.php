<?php
/**
 * Created by PhpStorm.
 * User: matthew
 * Date: 10/10/18
 * Time: 3:38 PM
 */

function dev_output() {
	
	global $template;
	
	echo '<strong>' . $template . '</strong>';
	
	echo '<script id="__bs_script__">//<![CDATA[
    document.write("<script async src=\'http://HOST:3000/browser-sync/browser-sync-client.js?v=2.26.0\'><\/script>".replace("HOST", location.hostname));
//]]></script>';
	
}

//add_action( 'wp_footer', 'dev_output' );


function theme_styles() {
	
	wp_enqueue_style( 'typekit', 'https://use.typekit.net/crn2dtz.css' );
	wp_enqueue_style( 'font-awesome', 'https://use.fontawesome.com/releases/v5.3.1/css/all.css' );
	wp_enqueue_style( 'global', get_template_directory_uri() . '/css/global.css' );
	
}

add_action( 'wp_enqueue_scripts', 'theme_styles' );


add_theme_support( 'post-thumbnails' );
add_theme_support( 'custom-logo' );


function exclude_profile_fields_admin( $exclude ) {
	
	$exclude[] = 'profile_picture';
	$exclude[] = 'profile_introduction';
	
	return $exclude;
	
}

add_filter( 'chumly_edit_profile_exclude_fields', 'exclude_profile_fields_admin' );


function load_signup_industry_value( $value, $input ) {
	
	if ( $input->input_name == 'signup_industry' && isset( $_GET[ 'industry' ] ) ) {
		
		return get_term_by( 'slug', $_GET[ 'industry' ], 'industry' )->name;
		
	}
	
	return $value;
	
}

add_filter( 'chumly_load_field_value', 'load_signup_industry_value', 10, 2 );


function acf_brand_choices( $field ) {
	
	$field[ 'choices' ] = array();
	$industry           = get_term_by( 'name', chumly_get_profile_field( 'signup_industry', 'name', chumly_user_id() )->value, 'industry' );
	$approved_brands    = get_user_meta( get_current_user_id(), 'approved_brands', TRUE );
	
	if ( isset( $_GET[ 'industry' ] ) ) {
		
		$brands = get_posts( array(
			'post_type'      => 'brand',
			'posts_per_page' => 1000,
			'tax_query'      => array(
				array(
					'taxonomy' => 'industry',
					'field'    => 'slug',
					'terms'    => $_GET[ 'industry' ]
				)
			)
		) );
		
		foreach ( $brands as $brand ) {
			
			$field[ 'choices' ][ $brand->ID ] = $brand->post_title;
			
		}
		
		return $field;
		
	}
	
	
	if ( $industry ) {
		
		$brands = get_posts( array(
			'post_type'      => 'brand',
			'posts_per_page' => 1000,
			'post__not_in'   => $approved_brands,
			'tax_query'      => array(
				array(
					'taxonomy' => 'industry',
					'field'    => 'slug',
					'terms'    => $industry->slug
				)
			)
		) );
		
		foreach ( $brands as $brand ) {
			
			$field[ 'choices' ][ $brand->ID ] = $brand->post_title;
			
		}
		
		return $field;
		
	}
	
	$brands = get_posts( array(
		'post_type'      => 'brand',
		'posts_per_page' => 1000
	) );
	
	foreach ( $brands as $brand ) {
		
		$field[ 'choices' ][ $brand->ID ] = $brand->post_title;
		
	}
	
	return $field;
	
}

add_filter( 'acf/load_field/name=requested_brands', 'acf_brand_choices' );


function acf_approved_brands_choices( $field ) {
	
	$field[ 'choices' ] = array();
	$industry           = get_term_by( 'name', chumly_get_profile_field( 'signup_industry', 'name', chumly_user_id() )->value, 'industry' );
	$approved_brands    = get_user_meta( get_current_user_id(), 'approved_brands', TRUE );
	
	$brands = get_posts( array(
		'post_type'      => 'brand',
		'posts_per_page' => 1000,
		'post__in'       => $approved_brands
	) );
	
	foreach ( $brands as $brand ) {
		
		$field[ 'choices' ][ $brand->ID ] = $brand->post_title;
		
	}
	
	return $field;
	
}

add_filter( 'acf/load_field/name=approved_brands', 'acf_approved_brands_choices' );


function update_chumly_brands_field( $value, $user ) {
	
	$user_id  = explode( '_', $user )[ 1 ];
	$input_id = chumly_get_profile_field( 'brands', 'name', $user_id )->field_id;
	
	
	$data[ 'value' ] = $value;
	
	chumly_update_field( $user_id, $input_id, $data );
	
	return $value;
	
}

add_filter( 'acf/update_value/name=requested_brands', 'update_chumly_brands_field', 10, 2 );


function update_chumly_approved_brands_field( $value, $user ) {
	
	$user_id = explode( '_', $user )[ 1 ];
	
	$data[ 'value' ] = $value;
	$input_id        = chumly_get_profile_field( 'approved_brands', 'name', $user_id )->field_id;
	
	chumly_update_field( $user_id, $input_id, $data );
	
	return $value;
	
	
}

add_filter( 'acf/update_value/name=approved_brands', 'update_chumly_approved_brands_field', 10, 2 );


function send_request_emails( $user_id, $profile_data ) {
	
	$brands   = chumly_get_profile_field( 'brands', 'name', $user_id )->value;
	$username = chumly_username( $user_id );
	
	foreach ( $brands as $brand ) {
		
		$recipient = get_post_meta( $brand->post_ID, 'application_email', TRUE );
		
		if ( $recipient ) {
			
			$message_body = $username . ' is applying to be a ' . $brand->title . ' Pro User – please set them up.<br><br>';
			$message_body .= $username . '\'s application details:<br>{{profile_fields}}';
			
			new Chumly_Email( array(
				array(
					'id'    => $user_id,
					'email' => $recipient
				)
			), 'New Discount Code Application from IPA Collective', 'blank', $message_body );
			
		}
		
	}
	
}

add_action( 'chumly_after_user_approved', 'send_request_emails', 10, 2 );


/*function chumly_account_approved_admin_email($user_id){
	
	$random_number = rand(0, 100);
	new Chumly_Email( array(
		'id'           => get_user_by( 'email', get_option( 'admin_email' ) )->ID,
		'email'        => get_option( 'admin_email' ),
		'message_body' => 'Special Message Body ' . $random_number
	), bloginfo( 'name' ) . ' Account Approved', 'account-approved' );
	
	
}

add_action( 'chumly_after_user_approved', 'chumly_account_approved_admin_email' );*/