<?php
/**
 * Created by PhpStorm.
 * User: matthew
 * Date: 10/10/18
 * Time: 8:19 PM
 */

get_header(); ?>

<?php $term = get_queried_object(); ?>

    <div class="wrapper">

        <div class="chunk chunk--double">

            <div class="content">

				<?php the_field( 'content' ); ?>

            </div>

        </div>

        <div class="chunk">
            <div class="cta">
                <a href="<?php echo home_url( '/' ) . chumly_get_option( 'registration_page' ) . '?industry=' . $term->slug; ?>"
                   class="button">Apply Now</a>
            </div>
        </div>

        <div class="chunk chunk--double">

            <div class="grid">

				<?php
				$query = new WP_Query( array(
					'post_type'      => 'brand',
					'orderby'        => 'name',
					'order'          => 'ASC',
					'posts_per_page' => - 1,
					'tax_query'      => array(
						array(
							'taxonomy' => 'industry',
							'field'    => 'term_id',
							'terms'    => $term->term_id
						)
					)

				) );

				if ( $query->have_posts() ) : while ( $query->have_posts() ) : $query->the_post(); ?>

                    <div class="grid__item">

                        <a href="<?php the_field('external_link'); ?>" target="_blank">

							<?php the_post_thumbnail( 'medium' ); ?>

							<?php the_content(); ?>

                        </a>

                    </div>

				<?php endwhile; endif; ?>

            </div>

        </div>

    </div>

<?php get_footer(); ?>