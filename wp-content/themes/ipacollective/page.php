<?php
/**
 * Created by PhpStorm.
 * User: matthew
 * Date: 10/10/18
 * Time: 5:35 PM
 */

get_header(); ?>
	
	<div class="wrapper">
		
		<div class="content chunk--double">
			
			<?php the_post(); ?>
			<?php the_content(); ?>
			
			<?php $industries = get_terms( 'industry', array( 'hide_empty' => FALSE ) ); ?>
			
			<ul class="content__split content__split--narrow">
			
			<?php foreach ( $industries as $industry ) { ?>
			
				<li class="content__split__item">
				
					<a href="<?php echo get_term_link($industry->term_id); ?>"><?php echo $industry->name; ?></a>
					
				</li>
				
			<?php } ?>
				
			</ul>
		
		</div>
	
	</div>

<?php get_footer();