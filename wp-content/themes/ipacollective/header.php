<?php
/**
 * Created by PhpStorm.
 * User: matthew
 * Date: 10/10/18
 * Time: 3:38 PM
 */
?>

<!DOCTYPE html>
<html <?php language_attributes(); ?>>

<head>
	
	<title><?php wp_title(); ?></title>
	
	<meta charset="<?php bloginfo( 'charset' ); ?>">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	
	<?php wp_head(); ?>

</head>

<body <?php body_class(); ?>>

<?php $fields = get_fields(); ?>

<?php if ( $fields[ 'hero_image' ][ 'url' ] ) {
	
	$hero_image = $fields[ 'hero_image' ][ 'url' ];
	
} else {
	
	$hero_image = get_template_directory_uri() . '/images/hero-image.jpg';
	
} ?>

<header class="site-head site-head--tall" style="background-image: url(<?php echo $hero_image; ?>)">
	
	<nav class="site-head__nav">
		
		<a href="<?php echo home_url(); ?>" class="site-head__logo"><?php echo bloginfo(); ?></a>
		
		<div class="site-head__nav__items">
			
			<?php if ( is_user_logged_in() ) { ?>
				
				<a href="<?php echo wp_logout_url( home_url() ); ?>" class="site-head__nav__item">Logout</a>
			
			<?php } else { ?>
				
				<a href="<?php echo chumly_login_url(); ?>" class="site-head__nav__item">Login</a>
			
			<?php } ?>
		
		</div>
	
	</nav>
	
	<?php if ( $fields[ 'strap_line' ] ) { ?>
		
		<p class="site-head__intro"><?php echo $fields[ 'strap_line' ]; ?></p>
	
	<?php } ?>


</header>