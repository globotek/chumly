<?php
/**
 * Created by PhpStorm.
 * User: matthew
 * Date: 10/10/18
 * Time: 3:38 PM
 */

get_header(); ?>

	<div class="wrapper">
		
		<div class="content">
			
			<?php the_post(); ?>
			<?php the_content(); ?>
			
			<?php $industries = get_terms( 'industry' ); ?>
		
		</div>
	
	</div>

<?php get_footer();