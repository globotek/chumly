<!-- TILE COMPONENT -->
<a class="content-tile content-tile--eggshell content-tile--inner-stroke" href="<?php the_permalink(); ?>">

	<div class="content-tile__media">
		   <div class="content-tile__media__image" style="background-image: url(<?php echo wp_get_attachment_url(get_post_thumbnail_id()); ?>)"></div>
	</div>

	<div class="content-tile__inner">
		<div class="content-tile__content">
			<h3 class="content-tile__heading"><?php the_title(); ?></h3>
			<p class="content-tile__summary"><?php the_content(); ?></p>
		</div>
		<div class="content-tile__meta content-tile__meta--wide">

			<!-- CONTENT META COMPONENT -->
			<div class="content-meta content-meta--wide">
				<ul class="content-meta__row">
					<li class="content-meta__item">

						<!-- USER AVATAR COMPONENT -->
						<div class="user-avatar">
							<div class="user-avatar__image">
								<?php chumly_avatar($post->post_author); ?>
							</div>

							<div class="user-avatar__content">
								<?php the_author(); ?>
							</div>
						</div>
						<!-- USER AVATAR COMPONENT -->

					</li>

					<li class="content-meta__item">
						<?php echo 'Intensity: ' . get_post_meta($post->ID, 'class_level', true); ?>
					</li>
				</ul>

				<?php
				$post_ratings = get_post_meta($post->ID, 'post_ratings', true);

				if(!empty($post_ratings)){

					$average_rating = number_format(array_sum($post_ratings) / count($post_ratings), 1);
					$rounded_average = round($average_rating);
					
				}
				?>

				<ul class="content-meta__row">
					<li class="content-meta__item">
						<?php echo get_post_meta($post->ID, 'video_runtime', true) . ' Mins'; ?>
					</li>

					<li class="content-meta__item">

						<!-- STAR RATING COMPONENT -->
						<div class="star-rating">
							<div class="star-rating__figure">
								<?php
								if(!empty($post_ratings)){

									echo $average_rating . '&nbsp;<span class="is-hidden--text">out of 5 rating</span>';

								} else {

									echo '&nbsp;No rating yet';

								} ?>
							</div>

							<div class="star-rating__decor" role="presentation">
								<?php
								if(!empty($post_ratings)){

									for($ia = 0; $ia < $rounded_average; $ia++){
										echo '<span class="star-rating__icon" data-icon="star"></span>';

									}

									for($ib = 5; $ib > $rounded_average; $ib--){
										echo '<span class="star-rating__icon star-rating__icon--muted" data-icon="star"></span>';
									}

								}
								?>
							</div>
						</div>
						<!-- STAR RATING COMPONENT -->

					</li>
				</ul>
			</div>
			<!-- CONTENT META COMPONENT -->

		</div>
	</div>
</a>
<!-- TILE COMPONENT -->
