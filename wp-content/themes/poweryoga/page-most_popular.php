<?php
/**
 * Created by PhpStorm.
 * User: matthew
 * Date: 16/8/18
 * Time: 12:54 PM
 */

$dynamic_data = array(
	'page_title' => 'Most Popular',
	'query_args' => array(
		'post_type'      => 'chumly_lesson',
		'posts_per_page' => 16
	)
);

dynamic_archive( $dynamic_data );