<?php //Template Name: Studio Schedule ?>
<?php get_header(); ?>

<?php the_post(); ?>

<!-- HERO -->
<section class="hero">
	<div class="wrapper">
		<header class="hero__content">
			<h1 class="hero__heading"><?php echo get_the_title(); ?></h1>
			<h2 class="hero__summary"><?php echo get_post_meta(get_the_ID(), 'hero_subheading', true); ?></h2>
		</header>
	</div>
	<aside class="hero__image" role="presentation" style="background-image: url(<?php echo wp_get_attachment_url(get_post_thumbnail_id()); ?>)"></aside>
</section>
<!-- HERO -->


<!-- PAGE CONTENT -->
<main class="generic-page">
	<div class="generic-page__primary">
		<div class="wrapper">
			
			<?php the_content(); ?>
			
            <script src="https://widgets.healcode.com/javascripts/healcode.js" type="text/javascript"></script>
			<healcode-widget data-type="schedules" data-widget-partner="mb" data-widget-id="5924097e2d1" data-widget-version="0.1"></healcode-widget>

        </div>
	</div>
</main>
<!-- PAGE CONTENT -->

<?php get_footer(); ?>
