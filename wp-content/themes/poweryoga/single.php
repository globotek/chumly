<?php
/**
 * Created by PhpStorm.
 * User: matthew
 * Date: 2/12/16
 * Time: 11:53 AM
 */

get_header();

$post_views = get_post_meta( $post->ID, 'post_views', TRUE );
++$post_views;
update_post_meta( $post->ID, 'post_views', $post_views );
?>

<!-- PAGE CONTENT -->
<main class="generic-page">
	
	<div class="wrapper">
		
		<?php yoast_breadcrumb( '<p class="content__header">', '</p>' ); ?>
		
		<div class="content__body">
			<h1 class="content__body__heading" style="padding-bottom: 20px;"><?php echo get_the_title(); ?></h1>
		</div>
	</div>
	
	<div class="wrapper content-sidebar">
		
		<div class="content-sidebar__content">
			
			
			<?php the_post_thumbnail( NULL, array( 'class' => 'content__featured-image' ) ); ?>
			
			<div class="content__body content__body__peek">
				<?php the_content(); ?>
				<div class="content__body__peek__overlay"></div>
				<button id="content_reveal" class="button content__body__peek__trigger">View More</button>
			</div>
			
			
			<!-- SHARING -->
			<?php get_template_part( 'sharing', 'blog' ); ?>
			<!-- SHARING -->
			
			
			<!-- MOBILE SIDEBAR -->
			<div class="content-sidebar__sidebar--mobile">
				
				<ul class="sidebar">
					<?php dynamic_sidebar( 'blog_mobile_single' ); ?>
				</ul>
			
			</div>
			<!-- MOBILE SIDEBAR -->
			
			<div class="headline headline--left-align headline--breathe-small">
				<h3 class="headline__heading">Comments</h3>
			</div>
			
			<div class="chumly content__body">
				
				<?php if( !is_user_logged_in() ) { ?>
					
					<h3 class="user-profile__sub-heading breathe">Register for free &amp; join the discussion</h3>
					<?php chumly_registration_form(array('location' => array('required'))); ?>
				
				<?php } else { ?>
					
					<?php wp_reset_query(); ?>
					
					<?php chumly_post_form( 'chumly_status_post', array(
						'target_id'            => get_the_ID(),
						'post_format'          => 'comment',
						'media_classification' => 'photos'
					) ); ?>
				
				<?php } ?>
				
				<?php chumly_post_feed( array( 'post_id' => get_the_ID() ) ); ?>
				
				<?php wp_reset_query(); ?>
			
			</div>
			
			<div class="headline headline--left-align headline--breathe-small">
				<h3 class="headline__heading">Related posts</h3>
			</div>
			
			<?php
			$post_terms = get_the_terms( get_the_ID(), 'category' );
			
			foreach( $post_terms as $term ) {
				if( $term->parent > 0 ) {
					$related_term = get_term_by( 'id', $term->term_id, 'category' );
				}
			}
			?>
			
			<div class="content-tiles">
				
				<ol class="content-tiles__list">
					
					<?php
					$query = new WP_Query( array(
						'post_type'      => 'post',
						'posts_per_page' => 3,
						'cat'            => $related_term->term_id,
						'post__not_in'   => array( $post->ID ),
						'orderby'        => 'rand'
					) );
					
					while( $query->have_posts() ) : $query->the_post(); ?>
						
						<?php get_template_part( 'content', 'blog' ); ?>
					
					<?php endwhile; ?>
					<?php wp_reset_query(); ?>
				
				</ol>
			</div>
			
			<div class="video__button breathe--bottom">
				<a href="<?php echo home_url() . '/blog'; ?>" class="button button--large">View all posts</a>
			</div>
		
		</div>
		
		<div class="content-sidebar__sidebar">
			
			<ul class="sidebar">
				<?php dynamic_sidebar( 'blog_single' ); ?>
			</ul>
		
		</div>
	
	</div>
</main>
<!-- PAGE CONTENT -->

<?php get_footer(); ?>
