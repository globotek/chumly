jQuery(document).ready(function($){
	"use strict";
	
	$('.pricing__item').on('click', function(){
		var checkout_url = $(this).find('.checkout_url').val();

		$('.proceed_to_checkout').attr('href', checkout_url);
		console.log(checkout_url);
	});
	
	$('.room_quantity').on('change', function(){
		var checkout_url = $('.checkout_submit').attr('href'),
			room_quantity = $(this).val(),
			final_url = checkout_url.replace(/quantity=[0-9]/g, 'quantity=' + room_quantity);
		
		$('.checkout_submit').attr('href', final_url);
	});
	
	
	// Reveal content on SEO content blocks
	$('#home-content__trigger').on('click', function(event){
		event.preventDefault();
	
	 	$(document).find($('#home-content__target')).show();
		$(document).find($('#home-content__trigger')).remove();

	});
	
	
	// Reveal content hidden by mobile overlay
	$('#content_reveal').on('click', function(){
		console.log('Reveal');
		$('#content_reveal, .content__body__peek__overlay').hide();
		$('.content__body').css('height', 'auto');
	});
});
