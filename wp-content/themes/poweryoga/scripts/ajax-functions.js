jQuery(document).ready(function ($) {
	
	$('#login_submit').on('click', function (e) {
				
		e.preventDefault();
		
		var prompt   = $('.prompt'),
		    username = $('#login_user').val(),
		    password = $('#login_password').val();
		
		prompt.hide();
		
		$('#login_submit').text('Please wait...');
		
		if ($('#login_remember').is(':checked')) {
			var remember = true;
		} else {
			var remember = false;
		}
		
		$.ajax({
			url:     ajax_variables.ajax_url,
			type:    'POST',
			data:    {
				'action':   'log_user_in',
				'username': username,
				'password': password,
				'remember': remember
			},
			success: function (data) {
				
				$('#login_submit').text('Login');
				
				if (data == true) {
					
					$('#login-window').hide();
					location.replace(ajax_variables.home_url + '/video-library');
					
				}
				
				if (data != null) {
					
					prompt.show();
					prompt.find($('.prompt__content').html(data));
					
				}
				
			}
		});
	});
	
	
	$('.content-tile__action-button').on('click', function (event) {
		
		event.preventDefault();
		
		var elem = $(this);
		
		elem.css({'opacity': '0.5'});
		elem.hover(function () {
			$(this).css({'opacity': '0.5'});
		});
		
		var post_id = $(this).attr('data-post');
		
		if ($(this).attr('data-status') == 'add') {
			
			$.ajax({
				url:     ajax_variables.ajax_url,
				type:    'POST',
				data:    {
					'action':  'add_favorite_class',
					'post_id': post_id,
					'user_id': ajax_variables.user_id
				},
				success: function (data) {
					
					elem.css({'opacity': '1'});
					elem.hover(function () {
						
						$(this).css({'opacity': '0.5'});
						
					}, function () {
						
						$(this).css({'opacity': '1'});
						
					});
					
					$('.content-tile__action-button').attr('data-status', 'remove');
					$('.content-tile__action-button__text').html('Added To Favorites');
					$('.content-tile__action-button__icon').attr('data-icon', 'heart_fill');
					$('.content-tile__action-button__icon').icon();
								
				}
			});
			
		} else {
			
			$.ajax({
				url:     ajax_variables.ajax_url,
				type:    'POST',
				data:    {
					'action':  'remove_favorite_class',
					'post_id': post_id,
					'user_id': ajax_variables.user_id
				},
				success: function (data) {
					
					elem.css({'opacity': '1'});
					
					elem.hover(function () {
						
						$(this).css({'opacity': '0.5'});
						
					}, function () {
						
						$(this).css({'opacity': '1'});
						
					});
					
					$('.content-tile__action-button').attr('data-status', 'add');
					$('.content-tile__action-button__text').html('Add To Favorites');
					$('.content-tile__action-button__icon').attr('data-icon', 'heart');
					$('.content-tile__action-button__icon').icon();
										
				}
			});
			
		}
		
	});
	
});
