<?php
/**
 * Created by PhpStorm.
 * User: wmjc1
 * Date: 11/02/2019
 * Time: 16:53
 */
function load_blog_posts(){
	$count = 0;
	foreach ( $_POST['term_object'] as $term_type_key => $term_type_value ) {
		foreach ( $term_type_value as $term_id => $term_title ) {
			
			if ( $term_id != 'null' ) {
				$return_data['terms'][ $count ]['term_id']    = sanitize_text_field( $term_id );
				$return_data['terms'][ $count ]['term_title'] = sanitize_text_field( $term_title );
				$return_data['terms'][ $count ]['term_type']  = sanitize_text_field( $term_type_key );
				
				$term_set[ $term_type_key ][] = sanitize_text_field( $term_id );
				
				$count ++;
			}
		}
	}
	
	foreach ( $_POST['term_object'] as $term_type_key => $term_type_value ) {
		if($term_type_key == 'keyword'){
			$return_data['search'] = 1;
			$keyword_query = sanitize_text_field( $_POST['term_object']['keyword'] );
		}
		
		
		if ( $term_type_key == 'category' ) {
			$return_data['search'] = 1;
			$term_queries[] = array(
				'taxonomy' => 'category',
				'field'    => 'term_id',
				'terms'    => $term_set[ $term_type_key ],
				'relation' => 'OR'
			);
		}
		
		
		if ( $term_type_key == 'duration' || $term_type_key == 'intensity' || $term_type_key == 'type' ) {
			$return_data['search'] = 1;
			$term_queries[] = array(
				'taxonomy' => 'class_filter',
				'field'    => 'term_id',
				'terms'    => $term_set[ $term_type_key ],
				'relation' => 'OR'
			);
		}
		
		if ( $term_type_key == 'teacher' ) {
			$return_data['search'] = 1;
			$return_data['teacher'] = sanitize_text_field( $_POST['term_object']['teacher'] );
			$author_query = $term_set[$term_type_key];
		}
		
		if ( $term_type_key == 'favorite' ) {
			$return_data['search'] = 1;
			$favorites_query = get_user_meta( get_current_user_id(), 'favorited_classes', TRUE );
		}
	}
	
	if ( ! empty( $term_queries ) ) {
		
		$term_queries[] = array(
			'relation' => 'AND'
		);
		
	}
	
	if ( empty( $keyword_query ) && empty( $term_queries ) && empty( $author_query ) && empty( $favorites_query ) ) {
		$return_data['search'] = 0;
	}
	
	$archive_query_args = array(
		'post_type'      => esc_attr($_POST['post_type']),
		'posts_per_page' => 9,
		'offset'         => esc_attr($_POST['post_count']),
		's'              => $keyword_query,
		'tax_query' 	 => $term_queries,
		'author__in'     => $author_query,
		'post__in' 		 => $favorites_query
	);
	
	$query = new WP_Query( $archive_query_args );
	
	while( $query->have_posts() ) : $query->the_post();
		
		global $post;
		
		$post_views = get_post_meta($post->ID, 'post_views', true);
		
		$html_output = '
		<li class="content-tiles__item is-hidden">

		    <!-- TILE COMPONENT -->
		    <a class="content-tile content-tile--eggshell" href="' . get_the_permalink() . '">
		        <div class="content-tile__media">' .
					get_the_post_thumbnail( $post,'large', array( 'class' => 'content-tile__media__image' ) )

		            . '<!-- ADD \'is-visible\' class to show \'tick\' -->
		            <span class="content-tile__featured-flag" role="presentation">
		                <span data-icon="check"></span>
		            </span>
		        </div>

		        <div class="content-tile__inner">
		            <div class="content-tile__content">
		                <!-- ADD \'is-visible\' class  to \'content-tile__heading__icon\' to show \'heart\' -->
		                <h3 class="content-tile__heading">' . get_the_title() . ' <span class="content-tile__heading__icon" role="presentation" data-icon="heart_fill"></span></h3>
		                <div class="content-tile__summary">' . get_the_excerpt() . '</div>
		            </div>

		            <div class="content-tile__meta">';
		
		if(esc_attr($_POST['post_type']) == 'post'){
			
			$html_output .= '
							<!-- CONTENT META COMPONENT -->
							<div class="content-meta">
								<ul class="content-meta__row">
									<li class="content-meta__item">
			
										<!-- USER AVATAR COMPONENT -->
										<div class="user-avatar user-avatar--lap-up-left">
											<div class="user-avatar__image"><img src="' . chumly_get_avatar($post->post_author) . '"/></div>
			
											<div class="user-avatar__content">'
				. get_the_author() .
				'</div>
										</div>
										<!-- USER AVATAR COMPONENT -->
			
									</li>
									<li class="content-meta__item">
										Read: ' . ($post_views > 0 ? $post_views : '0') . ' times' .
				'</li>
								</ul>
			
							</div>
							<!-- CONTENT META COMPONENT -->';
			
		} else {
			
			$html_output .= '
							<!-- CONTENT META COMPONENT -->
							<div class="content-meta">
								<ul class="content-meta__row">
									<li class="content-meta__item">
	
										<!-- USER AVATAR COMPONENT -->
										<div class="user-avatar user-avatar--lap-up-left">
											<div class="user-avatar__image">' . chumly_get_avatar($post->post_author) . '</div>
	
											<div class="user-avatar__content">' . get_the_author() . '</div>
										</div>
										<!-- USER AVATAR COMPONENT -->
	
									</li>
	
									<li class="content-meta__item">Intensity: ' . get_post_meta($post->ID, "class_level", TRUE) . '</li>
								</ul>
	
								<ul class="content-meta__row">
									<li class="content-meta__item">
										' . get_post_meta($post->ID, "video_runtime", TRUE) . ' Mins
									</li>
	
									<li class="content-meta__item">
	
										<!-- STAR RATING COMPONENT -->
										<div class="star-rating">
											<div class="star-rating__figure">';
			
			$post_ratings = get_post_meta($post->ID, "post_ratings", TRUE);
			
			if(!empty($post_ratings)) {
				
				$average_rating = number_format(array_sum($post_ratings) / count($post_ratings), 1);
				$rounded_average = round($average_rating);
				$html_output .= $average_rating . '&nbsp;<span class="is-hidden--text">out of 5 rating</span>';
				
			} else {
				
				$html_output .= '&nbsp;No Rating Yet';
				
			}
			
			$html_output .= '</div>
	
											<div class="star-rating__decor" role="presentation">';
			
			
			if(!empty($post_ratings)) {
				
				for($ia = 0; $ia < $rounded_average; $ia++) {
					$html_output .= '<span class="star-rating__icon" data-icon="star">
															<svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 32 32" class="icon"><path class="icon__primary" d="M16 0l4.3 12.2H32l-9.5 7.2L25.9 32 16 24.4 6.1 32l3.4-12.6L0 12.2h11.7L16 0z"></path></svg>
														</span>';
					
				}
				
				for($ib = 5; $ib > $rounded_average; $ib--) {
					$html_output .= '<span class="star-rating__icon star-rating__icon--muted" data-icon="star">
															<svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 32 32" class="icon"><path class="icon__primary" d="M16 0l4.3 12.2H32l-9.5 7.2L25.9 32 16 24.4 6.1 32l3.4-12.6L0 12.2h11.7L16 0z"></path></svg>
														</span>';
				}
				
			}
			
			
			$html_output .= '</div>
										</div>
										<!-- STAR RATING COMPONENT -->
	
									</li>
								</ul>
							</div>
							<!-- CONTENT META COMPONENT -->';
			
		}
		
		$html_output .= '
		            </div>
		        </div>
		    </a>
		    <!-- TILE COMPONENT -->

		</li>';
		
		$post_markup[] = $html_output;
	
	endwhile;
	
	$return_data['post'] = $_POST;
	//$return_data['posts'] = $query->posts;
	$return_data['query']       = $query->query;
	$return_data['found_posts'] = intval( $query->found_posts );
	$return_data['total_pages'] = $query->max_num_pages;
	
	// Update our post count record for pagination offset.
	$return_data['post_count'] = $_POST['post_count'] + $query->post_count;
	$return_data['posts']      = $post_markup;
	
	echo json_encode( $return_data );
	
	die();
}

add_action( 'wp_ajax_load_blog_posts', 'load_blog_posts' );
add_action( 'wp_ajax_nopriv_load_blog_posts', 'load_blog_posts' );
