<?php
function log_user_in() {
	if ( ! empty( $_REQUEST[ 'username' ] ) && ! empty( $_REQUEST[ 'password' ] ) ) {
		
		$credentials = array(
			'user_login'    => sanitize_user( $_REQUEST[ 'username' ] ),
			'user_password' => sanitize_text_field( $_REQUEST[ 'password' ] ),
			'remember'      => $_REQUEST[ 'remember' ]
		);
		
		$login = wp_signon( $credentials, FALSE );
		
		if ( is_wp_error( $login ) ) {
			
			
			echo $login->get_error_message();
			
		} else {
			
			echo TRUE;
			
		}
		
	} else {
		
		echo 'Please fill in your login details.';
		
	}
	
	die();
}

add_action( 'wp_ajax_log_user_in', 'log_user_in' );
add_action( 'wp_ajax_nopriv_log_user_in', 'log_user_in' );


function conduct_term_search() {
	$count = 0;
	
	$user_id = get_current_user_id();
	
	if ( ! empty( $_POST[ 'term_object' ] ) ) {
		
		foreach ( $_POST[ 'term_object' ] as $term_type_key => $term_type_value ) {
			
			foreach ( $term_type_value as $term_id => $term_title ) {
				
				if ( $term_id != 'null' ) {
					$return_data[ 'terms' ][ $count ][ 'term_id' ]    = sanitize_text_field( $term_id );
					$return_data[ 'terms' ][ $count ][ 'term_title' ] = sanitize_text_field( $term_title );
					$return_data[ 'terms' ][ $count ][ 'term_type' ]  = sanitize_text_field( $term_type_key );
					
					$term_set[ $term_type_key ][] = sanitize_text_field( $term_id );
					
					$count ++;
				}
			}
		}
		
		foreach ( $_POST[ 'term_object' ] as $term_type_key => $term_type_value ) {
			
			if ( $term_type_key == 'keyword' ) {
				$return_data[ 'search' ] = 1;
				$keyword_query           = sanitize_text_field( $_POST[ 'term_object' ][ 'keyword' ] );
			}
			
			
			if ( $term_type_key == 'category' ) {
				$return_data[ 'search' ] = 1;
				$term_queries[]          = array(
					'taxonomy' => 'category',
					'field'    => 'term_id',
					'terms'    => $term_set[ $term_type_key ],
					'relation' => 'OR'
				);
			}
			
			
			if ( $term_type_key == 'duration' || $term_type_key == 'intensity' || $term_type_key == 'type' ) {
				$return_data[ 'search' ] = 1;
				$term_queries[]          = array(
					'taxonomy' => 'class_filter',
					'field'    => 'term_id',
					'terms'    => $term_set[ $term_type_key ],
					'relation' => 'OR'
				);
			}
			
			if ( $term_type_key == 'teacher' ) {
				$return_data[ 'search' ]  = 1;
				$return_data[ 'teacher' ] = sanitize_text_field( $_POST[ 'term_object' ][ 'teacher' ] );
				$author_query             = $term_set[ $term_type_key ];
			}
			
			if ( $term_type_key == 'favorite' ) {
				$return_data[ 'search' ] = 1;
				$favorites_query         = get_user_meta( $user_id, 'favorited_classes', TRUE );
			}
		}
		
	}
	
	if ( ! empty( $term_queries ) ) {
		
		$term_queries[] = array(
			'relation' => 'AND'
		);
		
	}
	
	if ( empty( $keyword_query ) && empty( $term_queries ) && empty( $author_query ) && empty( $favorites_query ) ) {
		$return_data[ 'search' ] = 0;
	}
	
	if ( esc_attr( $_POST[ 'post_type' ] ) == 'class' || esc_attr( $_POST[ 'post_type' ] ) == 'post' ) {
		
		$archive_query_args = array(
			'post_type'      => esc_attr( $_POST[ 'post_type' ] ),
			'posts_per_page' => 8,
			'offset'         => esc_attr( $_POST[ 'post_count' ] ),
			's'              => $keyword_query,
			'tax_query'      => $term_queries,
			'author__in'     => $author_query,
			'post__in'       => $favorites_query
		);
		
		$query = new WP_Query( $archive_query_args );
		
		$user_favorites = get_user_meta( $user_id, 'favorited_classes', TRUE );
		
		while ( $query->have_posts() ) : $query->the_post();
			
			global $post;
			if ( esc_attr( $_POST[ 'post_type' ] ) == 'post' ) {
				$post_views = get_post_meta( $post->ID, 'post_views', TRUE );
			}
			if ( in_array( $post->ID, $user_favorites ) ) {
				
				$visible_class = 'is-visible';
				
			} else {
				$visible_class = '';
			}
			
			$html_output = '
		<li class="class-tiles__item is-hidden">

		    <!-- TILE COMPONENT -->
		    <a class="content-tile content-tile--eggshell" href="' . get_the_permalink() . '">
		        <div class="content-tile__media">
					<div class="content-tile__media__image">
				
						<img src="' . wp_get_attachment_url( get_post_thumbnail_id() ) . '" />
						<span class="content-tile__media__icon ' . $visible_class . '" role="presentation">' . py_get_icon( 'heart_fill' ) . '</span>
				
						<!-- ADD \'is-visible\' class to show \'tick\' -->
						<span class="content-tile__featured-flag" role="presentation">
                            <span>' . py_get_icon( 'check' ) . '</span>
                        </span>
					</div>
		        </div>

		        <div class="content-tile__inner">
		            <div class="content-tile__content">
		                <!-- ADD \'is-visible\' class  to \'content-tile__heading__icon\' to show \'heart\' -->
		                <h3 class="content-tile__heading">' . get_the_title() . chumly_get_ratings_output( FALSE ) . '</h3>
		                <div class="content-tile__summary">' . get_the_excerpt() . '</div>
		            </div>

		            <div class="content-tile__meta">';
			
			if ( esc_attr( $_POST[ 'post_type' ] ) == 'post' ) {
				
				$html_output .= '
							<!-- CONTENT META COMPONENT -->
							<div class="content-meta">
								<ul class="content-meta__row">
									<li class="content-meta__item">
			
										<!-- USER AVATAR COMPONENT -->
										<div class="user-avatar user-avatar--lap-up-left">
											<div class="user-avatar__image"><img src="' . chumly_get_avatar( $post->post_author ) . '" /></div>
			
											<div class="user-avatar__content">' . get_the_author() . '</div>
										</div>
										<!-- USER AVATAR COMPONENT -->
			
									</li>
									<li class="content-meta__item">Read: ' . ( $post_views > 0 ? $post_views : '0' ) . ' times' . '</li>
								</ul>
			
							</div>
							<!-- CONTENT META COMPONENT -->';
				
			} elseif ( esc_attr( $_POST[ 'post_type' ] == 'class' ) ) {
				
				$html_output .= '
							<!-- CONTENT META COMPONENT -->
							<div class="content-meta">
								<ul class="content-meta__row">
									<li class="content-meta__item">
	
										<!-- USER AVATAR COMPONENT -->
										<div class="user-avatar user-avatar--centered">
								
											<div class="user-avatar__image"><img src="' . chumly_avatar( get_the_author_meta( 'ID' ), 'profile', NULL, FALSE ) . '"/></div>
	
											<div class="user-avatar__content">' . get_the_author() . '</div>
											
										</div>
										<!-- USER AVATAR COMPONENT -->
	
									</li>
	
									<li class="content-meta__item">' . get_post_meta( $post->ID, 'video_runtime', TRUE ) . ' minutes, ' . lcfirst( get_post_meta( $post->ID, 'class_level', TRUE ) ) . ' intensity</li>
								</ul>
	
							</div>
							<!-- CONTENT META COMPONENT -->';
				
			}
			
			$html_output .= '
		            </div>
		        </div>
		    </a>
		    <!-- TILE COMPONENT -->

		</li>';
			
			$post_markup[] = $html_output;
		
		endwhile;
		
		
		$return_data[ 'post' ] = $_POST;
		//$return_data['posts'] = $query->posts;
		$return_data[ 'query' ]       = $query->query;
		$return_data[ 'found_posts' ] = intval( $query->found_posts );
		$return_data[ 'total_pages' ] = $query->max_num_pages;
		
		// Update our post count record for pagination offset.
		$return_data[ 'post_count' ] = $_POST[ 'post_count' ] + $query->post_count;
		$return_data[ 'posts' ]      = $post_markup;
		
	}
	
	echo json_encode( $return_data );
	
	die();
}

add_action( 'wp_ajax_conduct_term_search', 'conduct_term_search' );
add_action( 'wp_ajax_nopriv_conduct_term_search', 'conduct_term_search' );


function add_favorite_class() {
	
	$user_id = sanitize_text_field( intval( $_REQUEST[ 'user_id' ] ) );
	$post_id = sanitize_text_field( intval( $_REQUEST[ 'post_id' ] ) );
	
	$class_ids = get_user_meta( $user_id, 'favorited_classes', TRUE );
	
	if ( ! is_array( $class_ids ) ) {
		$class_ids = array();
	}
	
	$class_ids[] = $post_id;
	
	update_user_meta( $user_id, 'favorited_classes', $class_ids );
	
	die();
}

add_action( 'wp_ajax_add_favorite_class', 'add_favorite_class' );
add_action( 'wp_ajax_nopriv_add_favorite_class', 'add_favorite_class' );


function remove_favorite_class() {
	
	$user_id = sanitize_text_field( intval( $_REQUEST[ 'user_id' ] ) );
	$post_id = sanitize_text_field( intval( $_REQUEST[ 'post_id' ] ) );
	
	$class_ids = get_user_meta( $user_id, 'favorited_classes', TRUE );
	
	$id_key = array_search( $post_id, $class_ids );
	unset( $class_ids[ $id_key ] );
	
	$class_ids = array_values( $class_ids );
	
	update_user_meta( $user_id, 'favorited_classes', $class_ids );
	
	die();
}

add_action( 'wp_ajax_remove_favorite_class', 'remove_favorite_class' );
add_action( 'wp_ajax_nopriv_remove_favorite_class', 'remove_favorite_class' );


function save_rating() {
	$user_id      = sanitize_text_field( intval( $_REQUEST[ 'user_id' ] ) );
	$post_id      = sanitize_text_field( intval( $_REQUEST[ 'post_id' ] ) );
	$user_rating  = sanitize_text_field( intval( $_REQUEST[ 'user_rating' ] ) );
	$user_ratings = get_user_meta( $user_id, 'user_ratings', TRUE );
	$post_ratings = get_post_meta( $post_id, 'post_ratings', TRUE );
	
	$user_ratings[ $post_id ] = $user_rating;
	$post_ratings[]           = $user_rating;
	
	update_user_meta( $user_id, 'user_ratings', $user_ratings );
	update_post_meta( $post_id, 'post_ratings', $post_ratings );
	
	die();
}

add_action( 'wp_ajax_save_rating', 'save_rating' );
add_action( 'wp_ajax_nopriv_save_rating', 'save_rating' );