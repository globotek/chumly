<?php //Template Name: Home ?>
<?php get_header(); ?>

<div class="landing-page">

	<!-- HERO -->
	<section class="landing-page__hero landing-page__hero--with-video">
		<div class="landing-page__hero__content">
			<div class="wrapper">
				<h1 class="landing-page__hero__heading">At home or on the road, stream online yoga classes from our studio.</h1>
				<div class="landing-page__hero__cta">
					<div class="landing-page__hero__primary-action">
						<a href="<?php echo home_url() . '/sign-up'; ?>" class="button button--large button--fill">Start Your 7-Day Free Online Trial</a>
					</div>
					<a href="#main-content" class="landing-page__hero__secondary-action js-jump">Or learn more</a>
				</div>
			</div>
		</div>

		<div class="landing-page__hero__image landing-page__hero__image--with-video" role="presentation" style="background-image: url(<?php echo get_template_directory_uri() . '/images/backgrounds/landing-hero.jpg'; ?>)"></div>
		<script type="text/javascript" src="//static.cdn-ec.viddler.com/js/arpeggio/v3/build/main-built.js"></script>
		
		<div class="landing-page__hero__video">
			<div class="viddler-auto-embed" data-embed-id="D1mPR5G4jD" data-video-id="f8db9e88" data-width="100%" data-hide-comment-embed-if-no-comments="1" data-initial-volume="0" data-autoplay="true" data-no-controls="true" data-loop="true"></div>
<!--			<video class="landing-page__hero__video__item" role="presentation" autoplay loop>
				<source src="<?php /*echo get_template_directory_uri() . '/images/backgrounds/home-video.mp4'; */?>" type="video/mp4">
			</video>
-->		</div>

		<a class="landing-page__hero__icon js-jump" href="#main-content">
			<span class="is-hidden--text">Click to progress down the page</span>
			<span data-icon="arrow_down_o" role="presentation"></span>
		</a>
	</section>
	<!-- HERO -->

	<!-- FEATURES -->
	<main class="landing-page__features" id="main-content">
		<div class="wrapper">
			<div class="landing-page__features__inner">

				<?php $child_pages = get_pages('hierarchical=0&sort_column=menu_order&parent=' . get_the_ID()); ?>

				<?php foreach($child_pages as $feature){ ?>
					
					<article class="landing-page__feature">
						<h3 class="landing-page__feature__heading"><?php echo $feature->post_title; ?></h3>
						<span role="presentation" class="landing-page__feature__icon" data-icon="<?php echo get_post_meta($feature->ID, 'feature_icon', true); ?>"></span>
						<h4 class="landing-page__feature__sub-heading"><?php echo get_post_meta($feature->ID, 'sub_heading', true); ?></h4>
						<p class="landing-page__feature__summary"><?php echo $feature->post_content; ?></p>
					</article>

				<?php } ?>

			</div>

			<div class="landing-page__features__button">
				<a href="<?php echo home_url() . '/sign-up'; ?>" class="button button--large button--fill">Subscribe today</a>
			</div>

		</div>
	</main>
	<!-- FEATURES -->

	<!-- LANDING TILES -->

	<section class="landing-page__tiles">

		<!-- TILES -->

		<div class="wrapper">

			<!-- HEADLINE -->

			<div class="headline">
				<div class="headline__heading">Give it a try!</div>
				<p class="headline__summary headline__summary--rev">The right class for your needs from 30 minutes to over 90 minutes</p>
			</div>

			<!-- HEADLINE -->

			<div class="content-tiles">

				<ol class="content-tiles__list content-tiles__list--centered breathe">
					<?php
					$query = new WP_Query(array(
						'post_type'   	 => 'chumly_lesson',
						'posts_per_page' => 3, 
						'tax_query' 	 => array(
							array(
								'taxonomy' => 'class_filter', 
								'field'    => 'ID', 
								'terms'     => 54
							)
						)
					));

					while($query->have_posts()) : $query->the_post(); ?>

						<?php get_template_part('content', 'class'); ?>

					<?php endwhile; ?>

				</ol>
			</div>
		</div>

		<!-- TILES -->

		<div class="landing-page__tiles__button">
			<a href="<?php echo home_url() . '/videos'; ?>" class="button button--large">Browse all classes</a>
		</div>
	</section>

	<!-- LANDING TILES -->


	<!-- TESTIMONIAL -->

	<section class="landing-page__testimonial">
		<div class="wrapper">

			<!-- HEADLINE -->

			<div class="headline">
				<h2 class="headline__heading">Power Yoga Reviews</h2>
				<p class="headline__summary">People from Santa Monica to Australia enjoy Power Yoga from the comforts of their home</p>
			</div>

			<!-- HEADLINE -->

			<?php
			$query = new WP_Query(array(
				'post_type' 	 => 'testimonial',
				'posts_per_page' => 1,
				'orderby' 	 	 => 'rand'
			));

			while($query->have_posts()) : $query->the_post();
			?>

				<div class="testimonial">
					<aside class="testimonial__icon" data-icon="quote_o" role="presentation"></aside>
					<div class="testimonial__content"><?php the_content(); ?></div>
					<div class="testimonial__avatar">

						<!-- USER AVATAR COMPONENT -->
						<div class="user-avatar user-avatar--multi">
							<div class="user-avatar__image">
								<?php the_post_thumbnail(57); ?>
							</div>
							<div class="user-avatar__content">
								<?php the_title(); ?> <span class="user-avatar__content__meta"><?php the_excerpt(); ?></span>
							</div>
						</div>
						<!-- USER AVATAR COMPONENT -->

					</div>
				</div>

			<?php endwhile; ?>

		</div>
	</section>

	<!-- TESTIMONIAL -->
	
	<!-- LANDING TILES -->
	
	<section class="landing-page__tiles">
		
		<!-- TILES -->
		
		<div class="wrapper">
			
			<!-- HEADLINE -->
			
			<div class="headline">
				<div class="headline__heading">Power Yoga Tips</div>
			</div>
			
			<!-- HEADLINE -->
			
			<div class="content-tiles">
				
				<ol class="content-tiles__list breathe">
					<?php
					$query = new WP_Query(array(
						'post_type'      => 'post',
						'posts_per_page' => 3,
						'orderby'        => 'rand'
					));
					
					while($query->have_posts()) : $query->the_post(); ?>
						
						<?php get_template_part('content', 'blog'); ?>
					
					<?php endwhile; ?>
				
				</ol>
			</div>
		</div>
		
		<!-- TILES -->
		
		<div class="landing-page__tiles__button">
			<a href="<?php echo home_url() . '/blog'; ?>" class="button button--large">Browse the blog</a>
		</div>
	</section>
	
	<!-- LANDING TILES -->
	
	<!-- HOME CONTENT -->
	
	<!-- CONTENT REVEAL -->
	<section class="landing-page__testimonial">
		<div class="wrapper content__body home-content">
			
			<?php the_post(); the_content(); ?>
			
		</div>
	</section>
	<!-- CONTENT REVEAL -->

</div>

<?php get_footer(); ?>
