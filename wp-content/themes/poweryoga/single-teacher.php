<?php get_header(); ?>

<?php the_post(); ?>

<?php $user_data = get_userdata(get_the_author_id()); ?>

<!-- HERO -->
<section class="hero hero--with-avatar">
	<div class="wrapper">
		<header class="hero__content">
			<h1 class="hero__heading"><?php the_title(); ?></h1>
			<h2 class="hero__summary"><?php echo get_post_meta($post->ID, 'teacher_byline', true); ?></h2>
		</header>
	</div>
	<aside class="hero__image" role="presentation" style="background-image: url(<?php echo get_template_directory_uri() . '/images/backgrounds/hero.jpg'; ?>)"></aside>
	<div class="hero__avatar" role="presentation" style="background-image: url(<?php the_post_thumbnail_url(); ?>)"></div>
</section>
<!-- HERO -->

<!-- PAGE CONTENT -->
<main class="generic-page">
	<div class="generic-page__primary">
		<div class="wrapper">

			<!-- CONTENT -->
			<article class="content">
				<div class="content__body">
					<?php the_content(); ?>
				</div>
			</article>
			<!-- CONTENT -->

			<!-- HEADLINE -->
			<div class="headline headline--breathe">
				<h2 class="headline__heading"><?php echo $user_data->first_name . '\'s latest classes'; ?></h2>
			</div>
			<!-- HEADLINE -->
			
			<div class="content-tiles">
				<ol class="content-tiles__list class-tiles__list--centered">

					<?php
                    $related = new WP_Query(array(
                        'post_type'      => 'chumly_lesson',
                        'posts_per_page' => 3,
                        'author'         => $user_data->ID
                    ));

                    if($related->have_posts()) : while($related->have_posts()) : $related->the_post(); ?>

                        <?php get_template_part('content', 'class'); ?>

    				<?php endwhile; else : ?>
						
						<p>No Classes</p>
						
					<?php endif; ?>
								
				</ol>
			</div>
			
			<?php 
			$healcode_id = get_field('healcode_id', sprintf('user_%s', $post->post_author)); 

			if(!empty($healcode_id)){ ?>

				<div class="content-tiles">
					<script src="https://widgets.healcode.com/javascripts/healcode.js" type="text/javascript"></script>
					<healcode-widget data-type="schedules" data-widget-partner="mb" data-widget-id="<?php echo $healcode_id; ?>" data-widget-version="0.1"></healcode-widget>
				</div>

			<?php } ?>

		</div>
	</div>
</main>
<!-- PAGE CONTENT -->

<?php get_footer(); ?>
