<?php
/**
 * Created by PhpStorm.
 * User: wmjc1
 * Date: 30/01/2019
 * Time: 16:51
 */

$program = get_queried_object();
$program_modules = chumly_unserialize( get_term_meta( $program->term_id, 'chumly_elearning_linked_children', TRUE ) );
$include_modules = wp_list_pluck( $program_modules, 'ID' );
$modules = get_terms( 'chumly_module', array( 'include' => $courses_filter, 'hide_empty' => FALSE ) ); ?>

<div class="videos">
	
	<div class="videos__archive is-visible">
		
		<div class="videos__headline headline">
			
			<h3 class="headline__heading"><?php echo $subject->name; ?></h3>
			
			<a href="/video-library" role="button" class="button-list__meta-button">
				<span class="button-list__meta-button__icon" role="presentation" data-icon="arrow_left_o"></span>
				<span class="button-list__meta-button__content">Back to Library</span>
			</a>
		
		</div>
		
		<div class="class-tiles">
			<ol class="class-tiles__list">
				
				<?php foreach( $modules as $module ) { ?>
									
					<?php include(get_template_directory() . '/content-class_category.php'); ?>
					
				<?php } ?>
			
			</ol>
		</div>
	
	</div>

</div>

