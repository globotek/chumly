<?php get_header(); ?>

<?php the_post(); ?>

<main class="video">
	<div class="wrapper">
		
		<div class="video__tile">
			
			<!-- CONTENT TILE -->
			<div class="content-tile content-tile--eggshell content-tile--single" href="/video.html">
				<div class="content-tile__media content-tile__media--video">
					
					<?php
					if (
						has_term( '', 'class_category', $post->ID ) ||
						has_term( 'homepage-videos', 'class_filter', $post->ID ) ||
						has_term( 'sales-landing-videos', 'class_filter', $post->ID ) ||
						wcs_user_has_subscription( get_current_user_id(), '', 'active' ) == TRUE ||
						wcs_user_has_subscription( get_current_user_id(), '', 'pending-cancel' ) == TRUE ||
						current_user_can( 'administrator' )
					) { ?>
						
						<script type="text/javascript" src="//static.cdn-ec.viddler.com/js/arpeggio/v3/build/main-built.js"></script>
						<div class="viddler-auto-embed" data-video-id="<?php echo get_post_meta( $post->ID, 'video_id', TRUE ); ?>" data-width="100%"></div>
					
					<?php } else { ?>
						
						<h2 style="text-align:center; margin-top:140px;">To view Power Yoga online classes, please subscribe.</h2>
						<a href="<?php echo home_url() . '/sign-up'; ?>" style="display:inline-block; width:100%; text-align:center; margin-top:20px; margin-bottom:90px;">Start Your 7 Day Free Trial.</a>
					
					<?php } ?>
				
				</div>
				
				<div class="content-tile__inner">
					<div class="content-tile__content content-tile__content--with-action">
						<h3 class="content-tile__heading content-tile__heading--rev"><?php the_title(); ?></h3>
						<div class="content-tile__summary"><?php the_content(); ?></div>
						
						<?php
						$favorited_classes = get_user_meta( get_current_user_id(), 'favorited_classes', TRUE );
						
						if ( is_user_logged_in() ) {
							
							if ( is_array( $favorited_classes ) ) {
								
								in_array( $post->ID, $favorited_classes ) ? $favorite = TRUE : $favorite = FALSE;
								
							} else {
								
								$favorite = FALSE;
								
							} ?>
							
							
							<!-- FAV BUTTON -->
							<a href="#" role="button" data-status="<?php _e( $favorite == FALSE ? 'add' : 'remove' ); ?>" data-post="<?php echo $post->ID; ?>" class="content-tile__action-button ajax-trigger">
								<div class="content-tile__action-button__text"><?php _e( $favorite == FALSE ? 'Add To Favorites' : 'Remove From Favorites' ); ?></div>
								<span class="content-tile__action-button__icon" role="presentation" data-icon="<?php _e( $favorite == FALSE ? 'heart' : 'heart_fill' ); ?>"></span>
							</a>
							<!-- FAV BUTTON -->
						
						<?php } ?>
					
					</div>
					
					<div class="content-tile__meta content-tile__meta--wide">
						
						<!-- CONTENT META COMPONENT -->
						<div class="content-meta content-meta--wide">
							<ul class="content-meta__row">
								<li class="content-meta__item">
									
									<!-- USER AVATAR COMPONENT -->
									<div class="user-avatar">
										<div class="user-avatar__image">
											<?php chumly_avatar( $post->post_author ); ?>
										</div>
										<div class="user-avatar__content">
											<?php the_author(); ?>
										</div>
									</div>
									<!-- USER AVATAR COMPONENT -->
								
								</li>
								<li class="content-meta__item">
									<?php echo 'Intensity: ' . get_post_meta( $post->ID, 'class_level', TRUE ); ?>
								</li>
							</ul>
							
							<?php
							$user_ratings = get_user_meta( get_current_user_id(), 'user_ratings', TRUE );
							$post_ratings = get_post_meta( $post->ID, 'post_ratings', TRUE );
							
							if ( ! empty( $post_ratings ) ) {
								
								$average_rating  = number_format( array_sum( $post_ratings ) / count( $post_ratings ), 1 );
								$rounded_average = round( $average_rating );
								
							}
							?>
							
							<ul class="content-meta__row">
								<li class="content-meta__item">
									<?php echo get_post_meta( $post->ID, 'video_runtime', TRUE ) . ' Mins'; ?>
								</li>
								<li class="content-meta__item">
									
									<!-- STAR RATING COMPONENT -->
									<?php chumly_output_ratings( TRUE, TRUE ); ?>
									<!-- STAR RATING COMPONENT -->
								
								</li>
							</ul>
						</div>
						<!-- CONTENT META COMPONENT -->
					
					</div>
				</div>
			</div>
			<!-- CONTENT TILE -->
		
		</div>
		
		
		<?php if (
			wcs_user_has_subscription( get_current_user_id(), '', 'active' ) == TRUE ||
			wcs_user_has_subscription( get_current_user_id(), '', 'pending-cancel' ) == TRUE ||
			current_user_can( 'administrator' )
		) { ?>
			
			<div class="headline headline--breathe-small">
				<h3 class="headline__heading">Comments</h3>
			</div>
			
			<div class="chumly content">
				
				<?php wp_reset_query(); ?>
				
				<?php chumly_post_form( 'chumly_status_post', array(
					'target_id'            => get_the_ID(),
					'post_format'          => 'comment',
					'media_classification' => 'photos'
				) ); ?>
				
				<?php chumly_post_feed( array( 'post_id' => get_the_ID() ) ); ?>
				
				<?php wp_reset_query(); ?>
			
			</div>
			
			<div class="headline headline--breathe-small">
				<h3 class="headline__heading">Related classes</h3>
			</div>
			
			<?php
			$post_terms = get_the_terms( get_the_ID(), 'class_filter' );
			
			foreach ( $post_terms as $term ) {
				if ( $term->parent == '21' ) {
					$related_term = get_term_by( 'id', $term->term_id, 'class_filter' );
				}
			}
			?>
			
			<div class="content-tiles">
				
				<ol class="content-tiles__list content-tiles__list--centered">
					
					<?php
					$query = new WP_Query( array(
						'post_type'      => 'chumly_lesson',
						'posts_per_page' => 3,
						'tax_query'      => array(
							array(
								'taxonomy' => 'class_filter',
								'field'    => 'id',
								'terms'    => $related_term->term_id
							)
						),
						'post__not_in'   => array( $post->ID )
					) );
					
					while ( $query->have_posts() ) : $query->the_post(); ?>
						
						<?php get_template_part( 'content', 'class' ); ?>
					
					<?php endwhile; ?>
				
				</ol>
			</div>
			
			<div class="video__button">
				<a href="<?php echo home_url() . '/video-library'; ?>" class="button button--large">View all classes</a>
			</div>
		
		<?php } else { ?>
			
			<?php $class_category = get_the_terms( $post->ID, 'class_category' ); ?>
			
			<?php if ( $class_category ) { ?>
				
				<div class="video__button">
					<br><br>
					<a href="<?php echo get_term_link( $class_category[ 0 ]->term_id, 'class_category' ); ?>" class="button button--large">Continue</a>
				</div>
			
			<?php } else { ?>
				
				<?php $class_filter = get_the_terms( $post->ID, 'class_filter' ); ?>
				
				<?php
				if ( has_term( 'sales-landing-videos', 'class_filter' ) ) {
					
					$continue_link = home_url() . '/yoga-videos';
					
				} else {
					
					$continue_link = home_url();
					
				} ?>
				
				<div class="video__button">
					<br><br>
					<a href="<?php echo home_url( '/sign-up' ); ?>" rel="nofollow" class="button button--large">Sign Up Now</a>
				</div>
			
			<?php } ?>
		
		<?php } ?>
	</div>
</main>

<?php get_footer(); ?>
