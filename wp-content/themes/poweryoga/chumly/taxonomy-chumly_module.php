<?php
/**
 * Created by PhpStorm.
 * User: wmjc1
 * Date: 30/01/2019
 * Time: 16:51
 */

$course = get_queried_object(); ?>
<?php $linked_children = chumly_unserialize( get_term_meta( get_queried_object_id(), 'chumly_elearning_linked_children', TRUE ) ); ?>
<?php $include_lessons = wp_list_pluck( $linked_children, 'ID' ); ?>

<div class="videos">
	
	<div class="videos__archive is-visible">
		
		<div class="videos__headline headline">
			
			<h3 class="headline__heading"><?php echo $course->name; ?></h3>
			
			<a href="/video-library" role="button" class="button-list__meta-button">
				<span class="button-list__meta-button__icon" role="presentation" data-icon="arrow_left_o"></span>
				<span class="button-list__meta-button__content">Back to Library</span>
			</a>
		
		</div>
		
		<div class="class-tiles">
			<ol class="class-tiles__list">
				
				<?php
				$query = new WP_Query( array(
					'post_type' => 'chumly_lesson',
					'post__in'  => $include_lessons
				) );
				
				while ( $query->have_posts() ) : $query->the_post(); ?>
					
					<?php get_template_part( 'content', 'class' ); ?>
				
				<?php endwhile; ?>
			
			</ol>
		</div>
	
	</div>

</div>

