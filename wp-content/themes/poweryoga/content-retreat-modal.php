<?php global $product; ?>

<div class="modal__window modal__window--wide">
	<div class="modal__window__decor-image" role="presentation">
		<?php
		$attachment_ids = $product->get_gallery_attachment_ids();
		
		foreach($attachment_ids as $attachment_id) {
			
			echo '<img src="' . wp_get_attachment_url($attachment_id) . '">';
			
		}
		?>
	</div>
	<div class="modal__window__content">
		<div class="modal__window__content__inner modal__window__content__inner--flush">
			
			<!-- FEATURE CONTENT COMPONENT -->
			<div class="feature-content feature-content--breathe-bottom">
				<h2 class="feature-content__heading"><?php echo get_the_title(); ?></h2>
				<div class="feature-content__inner">
					<p><strong><?php echo $product->get_price_html(); ?></strong></p>
					<p><?php echo get_the_content(); ?></p>
				</div>
				
				<hr style="margin:20px 0;"/>
				
				<div class="feature-content__form">
					
					<?php if($product->get_stock_quantity() > 0) { ?>
						
						<?php do_action('woocommerce_before_add_to_cart_form'); ?>
						
						<form class="form cart" method="post" enctype="multipart/form-data">
							
							<?php do_action('woocommerce_before_add_to_cart_button'); ?>
							
							<div class="form__split form__split--third-two-third">
								<div>
									<label class="form__label">Quantity</label>
									<div class="form__input form__input--select" data-module="hidden-field">
										<span class="js-hidden-field__value">Select quantity</span>
										
										<select class="hidden-field js-hidden-field__input room_quantity"
												name="quantity">
											<option>Select quantity</option>
											
											<?php for($i = 1; $i <= $product->get_stock_quantity(); $i++) {
												
												echo '<option>' . $i . '</option>';
												
											} ?>
										
										</select>
										
										<!--<span role="presentation" class="form__input__icon" data-icon="arrow_down_o"></span>-->
									</div>
								</div>
							</div>
							
							<input type="hidden" name="add-to-cart" value="<?php echo esc_attr($product->id); ?>"/>
							
							<div class="form__group form__group--center form__group--breathe">
								<button type="submit"
										class="single_add_to_cart_button button button--fill checkout_submit">Book Now
								</button>
								<!--<a href="<?php //echo WC()->cart->get_checkout_url(); ?>?add-to-cart=<?php //echo $product->id; ?>&quantity=1" class="button button--fill checkout_submit">Book Now</a>-->
							</div>
						</form>
						
						<?php do_action('woocommerce_after_add_to_cart_form'); ?>
					
					<?php } else { ?>
						
						<strong>Sold Out!</strong>
					
					<?php } ?>
				
				</div>
			</div>
			<!-- FEATURE CONTENT COMPONENT -->
		
		</div>
	</div>

	<a href="#" class="modal__window__close js-toggle__trigger" data-target="parent">
		<span class="is-hidden--text">Close this window</span>
		<span role="presentation" data-icon="cross_o"></span>
	</a>
	
</div>

<div class="modal__overlay"></div>
