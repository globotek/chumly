<?php // Template Name: Teacher Training Archive ?>
<?php get_header(); ?>
<?php $page = (get_query_var('paged')) ? absint(get_query_var('paged')) : 1; ?>
<?php $current_url = get_the_permalink(get_the_ID()); ?>

<!-- HERO -->
<section class="hero">
	<div class="wrapper">
		<header class="hero__content">
			<h1 class="hero__heading"><?php the_title(); ?></h1>
			<div class="landing-page__hero__cta">
				<div class="landing-page__hero__primary-action">
					<a href="<?php echo get_post_meta( get_the_ID(), 'hero_link', TRUE ); ?>" target="_blank" class="button button--large button--fill"><?php echo get_post_meta(get_the_ID(), 'hero_subheading', true); ?></a>
				</div>
			</div>
		</header>
	</div>
	<aside class="hero__image" role="presentation" style="background-image: url(<?php echo wp_get_attachment_url(get_post_thumbnail_id()); ?>)"></aside>
</section>
<!-- HERO -->


<!-- PAGE CONTENT -->
<main class="generic-page">
	<div class="generic-page__primary generic-page__primary--narrow">
		<div class="wrapper">
			
			<div class="content-tiles">
				<ol class="content-tiles__list">

					<?php
					$query = new WP_Query(array(
						'post_type' 	 => 'teacher_training',
						'posts_per_page' => 6,
						'paged'		 	 => $page
					));

					while($query->have_posts()) : $query->the_post(); ?>

						<?php get_template_part('content', 'teacher_training'); ?>

					<?php endwhile; ?>

				</ol>
			</div>


			<div class="videos__button">
				<?php
				if($query->max_num_pages > 1){
					$page = (get_query_var('paged')) ? absint(get_query_var('paged')) : 1;
	
					$next_page = $page + 1;
					$next_url = $current_url . 'page/' . $next_page;
	
					$previous_page = $page - 1;
					$previous_url = $current_url . 'page/' . $previous_page;
	
					if($page != 1){
	
						echo '<a href="' . $previous_url . '" class="button button--large">Previous</a>';
	
					}
	
					echo '<p class="button button--large">' . $page . ' of ' . $query->max_num_pages . '</p>';
	
					if($page < $query->max_num_pages){
	
						echo '<a href="' . $next_url . '" class="button button--large">Next</a>';
	
					}
				}
				?>
			</div>

		</div>
	</div>
</main>
<!-- PAGE CONTENT -->

<?php get_footer(); ?>

