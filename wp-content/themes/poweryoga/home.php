<?php
/**
 * Template Name: Blog
 * Created by PhpStorm.
 * User: matthew
 * Date: 2/12/16
 * Time: 11:53 AM
 */


get_header(); ?>

<?php $page = ( get_query_var( 'paged' ) ) ? absint( get_query_var( 'paged' ) ) : 1; ?>
<?php $current_url = get_the_permalink( get_the_ID() ); ?>
	
<!-- HERO -->
<section class="hero">
	<div class="wrapper">
		<header class="hero__content">
			<h1 class="hero__heading"><?php echo get_the_title(); ?></h1>
			<h2 class="hero__summary">
				<?php echo get_post_meta( get_the_ID(), 'hero_subheading', TRUE ); ?>
			</h2>
		</header>
	</div>
	<aside class="hero__image" role="presentation" style="background-image: url(<?php echo wp_get_attachment_url( get_post_thumbnail_id() ); ?>)"></aside>
</section>
<!-- HERO -->

<main class="generic-page" data-module="paginate">
	
	<?php //get_template_part('search', 'blog'); ?>
	<input id="post_count_field" type="hidden" name="post_count" value="9"/>
	<input id="search_post_type" type="hidden" name="post_type" value="post"/>
	
	<div class="generic-page__tertiary">
		<div class="wrapper">
			
			<?php yoast_breadcrumb( '<p class="content__header">', '</p>' ); ?>
			
			<div class="videos">
								
				<?php
				$term_ids = '';
				if ( isset( $_GET['term_id'] ) ) {
					$term_ids = array_filter( array_map( 'intval', $_GET['term_id'] ) );
				}
				
				$term_query = '';
				if ( ! empty( $_GET['term_id'] ) ) {
					$term_query = array(
						array(
							'taxonomy' => 'category',
							'field'    => 'term_id',
							'terms'    => $term_ids,
							'operator' => 'AND',
						)
					);
				}
				
				
				$author_query = '';
				if ( ! empty( $_GET['teacher_id'] ) ) {
					$author_query = $_GET['teacher_id'];
				}
				
				$favorites_query = '';
				if ( $_GET['favorite'] == 'yes' ) {
					$favorites_query = get_user_meta( get_current_user_id(), 'favorited_classes', TRUE );
				}
				?>
				
				<div class="content-tiles">
					<ol class="content-tiles__list">
						
						<?php
						$archive_query_args = array(
							'post_type'      => 'post',
							'posts_per_page' => 9,
							'paged'          => $page,
							'author'         => $author_query,
							'tax_query'      => $term_query,
							'post__in'       => $favorites_query
						);
						
						$query = new WP_Query( $archive_query_args );
						
						while( $query->have_posts() ) : $query->the_post(); ?>
							
							<?php get_template_part( 'content', 'blog' ); ?>
						
						<?php endwhile; ?>
					
					</ol>
				</div>
				
				<div id="ajax_response"></div>
				<div class="videos__button">
					<?php if ( $query->found_posts > 9 ) { ?>
						<a id="load_more_posts" data-action="load" href="#" class="button button--large">View
							More</a>
					<?php } else { ?>
						<a id="load_more_posts" data-action="scroll" href="#" class="button button--large">Back To
							Top</a>
					<?php } ?>
				</div>
			
			</div>
		</div>
	</div>
	
	<!-- CONTENT REVEAL -->
	<section class="landing-page__testimonial">
		<div class="wrapper content__body home-content">
			
			<?php the_post(); the_content(); ?>
		
		</div>
	</section>
	<!-- CONTENT REVEAL -->
	
</main>
	

<?php get_footer(); ?>