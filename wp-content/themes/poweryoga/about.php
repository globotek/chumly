<?php //Template Name: About Us ?>
<?php get_header(); ?>

<?php the_post(); ?>

<!-- HERO -->
<section class="hero">
	<div class="wrapper">
		<header class="hero__content">
			<h1 class="hero__heading"><?php the_title(); ?></h1>
			<h2 class="hero__summary"><?php echo get_post_meta(get_the_ID(), 'hero_subheading', true); ?></h2>
		</header>
	</div>
	<aside class="hero__image" role="presentation" style="background-image: url(<?php echo wp_get_attachment_url(get_post_thumbnail_id()); ?>"></aside>
</section>
<!-- HERO -->

<!-- PAGE CONTENT -->
<main class="generic-page">
	<div class="generic-page__primary">
		<div class="wrapper">
			
			<!-- SOCIAL ICONS -->
			<aside>
				<ol class="inline-icon-links">
					<li class="inline-icon-links__item">
						<a href="https://www.facebook.com/Bryan-Kests-Power-Yoga-112005492146905/" class="inline-icon-links__inner">
							<span class="inline-icon-links__icon" data-icon="facebook" role="presentation"></span>
							<span class="inline-icon-links__content">Power yoga on facebook</span>
						</a>
					</li>
					<li class="inline-icon-links__item">
						<a href="https://instagram.com/bryan_kest/" class="inline-icon-links__inner">
							<span class="inline-icon-links__icon" data-icon="instagram" role="presentation"></span>
							<span class="inline-icon-links__content">Power yoga on instagram</span>
						</a>
					</li>
				</ol>
			</aside>
			<!-- SOCIAL ICONS -->
			
			<!-- DECOR HEADING -->
			<header class="decor-heading decor-heading--breathe-top decor-heading--breathe-bottom">
				<span class="decor-heading__icon" role="presentation" data-icon="pictogram"></span>
				<h2 class="decor-heading__heading">A fitness routine based on aesthetics feeds your ego, not your spirit.</h2>
			</header>
			<!-- DECOR HEADING -->
			
			<!-- CONTENT -->
			
			<article class="content">
				<div class="content__body">
					<?php the_content(); ?>
				</div>
			</article>
			
			<!-- CONTENT -->
			
		</div>
	</div>
	<div class="generic-page__secondary">
		<div class="wrapper">
			
			<!-- DECOR HEADING -->
			<header class="decor-heading decor-heading--breathe-bottom">
				<span class="decor-heading__icon" role="presentation" data-icon="question_o"></span>
				<h2 class="decor-heading__heading">Frequently asked questions.</h2>
			</header>
			<!-- DECOR HEADING -->
			
			<!-- ACCORDION LIST -->
			<ol class="accordion-list" data-module="toggle">
			
				<?php
				$q_and_a = get_field('faq');
				$i = 0;
				
				foreach($q_and_a as $single){
				$i++; ?>
				
				<li class="accordion-list__item">
					<div class="accordion">
						<a href="#1" class="accordion__trigger js-toggle__trigger" data-target="<?php echo $i; ?>" role="button">
							<div class="accordion__trigger__text">
								<?php echo $single['question']; ?>
							</div>
							<span class="accordion__trigger__icon" role="presentation" data-icon="plus_o"></span>
							<span class="accordion__trigger__active-icon" role="presentation" data-icon="minus"></span>
						</a>
						<div class="accordion__content content content--wide js-toggle__target" id="<?php echo $i; ?>">
							<div class="content__body">
								<p><?php echo $single['answer']; ?></p>
							</div>
						</div>
					</div>
				</li>
				
				<?php } ?>
								
			</ol>
			<!-- ACCORDION LIST -->
			
		</div>
	</div>
</main>
<!-- PAGE CONTENT -->

<?php get_footer(); ?>