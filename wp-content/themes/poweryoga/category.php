<?php
/**
 * Created by PhpStorm.
 * User: matthew
 * Date: 2/12/16
 * Time: 11:53 AM
 */

get_header(); ?>
	
<main class="generic-page">
	
	<div class="generic-page__secondary">
		<div class="wrapper">
			
			<?php yoast_breadcrumb('<p class="content__header">','</p>'); ?>
			
			<div class="videos">
				
				<div class="videos__headline headline">
					<h1 class="headline__heading"><?php single_cat_title(); ?></h1>
				</div>
								
				<div class="content-tiles">
					<ol class="content-tiles__list">
						
						<?php
						$archive_query_args = array(
							'post_type' 	 => 'post',
							'posts_per_page' => 999,
							'paged'		 	 => $page,
							'cat'			 => get_term_by('name', single_cat_title('', false), 'category')->term_id
						);
						
						$query = new WP_Query($archive_query_args);
						
						while($query->have_posts()) : $query->the_post(); ?>
							
							<?php get_template_part('content', 'blog'); ?>
						
						<?php endwhile; ?>
					
					</ol>
				</div>
				
				
<!--				<div class="videos__button">
					<?php /*if($query->found_posts > 9) { */?>
						<a id="load_more_posts" data-action="load" href="#" class="button button--large">View More</a>
					<?php /*} else { */?>
						<a id="load_more_posts" data-action="scroll" href="#" class="button button--large">Back To Top</a>
					<?php /*} */?>
				</div>
-->
			</div>
		</div>
	</div>
</main>

<?php get_footer(); ?>