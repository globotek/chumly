<?php
/**
 * Created by PhpStorm.
 * User: matthew
 * Date: 18/4/19
 * Time: 4:45 PM
 */

wp_reset_query(); ?>

<h3 class="title breathe--bottom">Join the discussion</h3>

<?php chumly_post_form( 'chumly_status_post', array(
	'target_id'            => get_the_ID(),
	'post_format'          => 'comment',
	'media_classification' => 'photos'
) ); ?>

<?php chumly_post_feed( array( 'post_id' => get_the_ID() ) ); ?>

<?php wp_reset_query(); ?>


