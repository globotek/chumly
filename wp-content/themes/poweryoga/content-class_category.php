<li class="class-tiles__item">
	
	<!-- TILE COMPONENT -->
	<a class="content-tile content-tile--eggshell" href="<?php echo get_term_link( $module->term_id, 'chumly_module' ); ?>">
		<div class="content-tile__media">
			<div class="content-tile__media__image">
				
				<img src="<?php echo wp_get_attachment_url( get_term_meta( $module->term_id, 'term_thumbnail', TRUE ) ); ?>" />
				<!-- ADD 'is-visible' class  to 'content-tile__heading__icon' to show 'heart' -->
				<span class="content-tile__media__icon <?php //_e(in_array($post->ID, $user_favorites) ? 'is-visible' : ''); ?>" role="presentation" data-icon="heart_fill"></span>
			
			</div>
			
			<!-- ADD 'is-visible' class to show 'tick' -->
			<span class="content-tile__featured-flag" role="presentation">
                <span data-icon="check"></span>
            </span>
		
		</div>
		
		<div class="content-tile__inner">
			<div class="content-tile__content">
				
				<h3 class="content-tile__heading">
					<?php echo $module->name; ?>
					<?php //chumly_output_ratings( FALSE ); ?>
				</h3>
				<div class="content-tile__summary">
					<?php echo term_description( $module->term_id, 'chumly_module' ); ?>
				</div>
			</div>
			
			
			<div class="content-tile__meta">
				
				<!-- CONTENT META COMPONENT -->
				<div class="content-meta">
					<ul class="content-meta__row">
						<li class="content-meta__item">
							
							<!-- USER AVATAR COMPONENT -->
							<div class="user-avatar user-avatar--centered">
								
								<div class="user-avatar__image">
									<?php echo $price; ?>
								</div>
								
								<div class="user-avatar__content">
									<p>Included with your membership</p>
								</div>
							
							</div>
							<!-- USER AVATAR COMPONENT -->
						
						</li>
						<li class="content-meta__item">
							<p><?php echo $module->count . ' classes'; ?></p>
						</li>
					</ul>
				
				</div>
				<!-- CONTENT META COMPONENT -->
			
			</div>
		</div>
	</a>
	<!-- TILE COMPONENT -->

</li>
