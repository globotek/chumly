<?php get_header(); ?>

<?php the_post(); ?>

<!-- HERO -->
<section class="hero">
	<div class="wrapper">
		<header class="hero__content">
			<h1 class="hero__heading"><?php the_title(); ?></h1>
			<h2 class="hero__summary"><?php echo get_field('hero_subheading'); ?></h2>
		</header>
	</div>
	<aside class="hero__image" role="presentation" style="background-image: url(<?php echo wp_get_attachment_url(get_post_thumbnail_id()); ?>)"></aside>
</section>
<!-- HERO -->

<!-- RETREAT -->
<main class="retreat">
	<div class="retreat__wrapper">
		
		<?php wc_print_notices(); ?>
		
		<?php the_content(); ?>
		
		<hr>

		<div class="retreat__check-details">
			<div class="retreat__check-detail">
				<span class="retreat__check-detail__icon" role="presentation" data-icon="marker_o"></span>
				<span class="retreat__check-detail__text"><strong>Check in</strong> <?php echo get_field('check_in_date'); ?></span>
			</div>
			<div class="retreat__check-detail">
				<span class="retreat__check-detail__icon" role="presentation" data-icon="proceed_o"></span>
				<span class="retreat__check-detail__text"><strong>Check out</strong> <?php echo get_field('check_out_date'); ?></span>
			</div>
		</div>

		<hr />

		<div class="retreat__details">
			<div class="retreat__detail">
				<h3 class="retreat__detail__heading">Included in the pricing</h3>
				<div class="retreat__detail__content content content--wide content--small">
					<div class="content__body">
						<ul>

							<?php
							$items = get_field('price_includes');

							if(!empty($items)){

								foreach($items as $item){
	
									echo '<li>' . $item['individual_item'] . '</li>';
								
								}
								
							} ?>

						</ul>
					</div>
				</div>
			</div>
			<div class="retreat__detail">
				<h3 class="retreat__detail__heading">Not included in the pricing</h3>
				<div class="retreat__detail__content content content--wide  content--small">
					<div class="content__body">
						<ul>

							<?php
							$items = get_field('price_excludes');

							if(!empty($items)){

								foreach($items as $item){
	
									echo '<li>' . $item['individual_item'] . '</li>';
								
								}
								
							} ?>

						</ul>
					</div>
				</div>
				<div class="retreat__detail__smallprint">
					<div class="smallprint">
						<div class="smallprint__icon" role="presentation" data-icon="info"></div>

						<div class="smallprint__text">
							<?php echo get_field('terms'); ?>
						</div>
					</div>
				</div>
			</div>
		</div>

		<div class="retreat__meta"><strong>Contact</strong> <?php echo '<a href="' . get_field('contact') . '" target="_blank">' . get_field('contact') . '</a>'; ?></div>
		
		<?php $correlating_retreat = get_field('retreat_post'); ?>

		<!-- GROUPS -->
		<!-- PRODUCT CARD LIST -->
		<div class="headline headline--breathe">
			<h2 class="headline__heading">Shared Room Pricing</h2>
		</div>

		<ol class="product-cards" data-module="toggle">

			<?php
			$shared_rooms = new WP_Query(array(
				'post_type' => 'product',
				'post__in'  => $correlating_retreat,
				'orderby'  => 'menu_order',
				'order' 	=> 'ASC',
				'tax_query' => array(
					array(
						'taxonomy' => 'product_cat',
						'field'    => 'slug',
						'terms'    => 'shared-rooms'
					)
				)
			));

			$ib = 0;

			if($shared_rooms->have_posts()) : while($shared_rooms->have_posts()) : $shared_rooms->the_post();
			$ib++;
			?>

				<li class="product-cards__item">

					<!-- PRODUCT CARD ITEM -->

					<a href="#" class="product-card js-toggle__trigger" role="button" data-target="more-info-<?php echo $ib; ?>-<?php echo $product->id; ?>">
						<div class="product-card__media" role="presentation"><?php the_post_thumbnail(); ?></div>
						<div class="product-card__content">
							<div class="product-card__inner">
								<h3 class="product-card__heading"><?php the_title(); ?></h3>
								<strong class="product-card__sub-heading"><?php echo $product->get_price_html(); ?></strong>
								<p class="product-card__summary">
									<?php the_excerpt(); ?>
								</p>
								<p class="product-card__highlight">Book here!</p>
							</div>
						</div>
					</a>

					<!-- PRODUCT CARD ITEM -->

					<aside class="modal js-toggle__target" id="more-info-<?php echo $ib; ?>-<?php echo $product->id; ?>">
						<?php get_template_part('content', 'retreat-modal'); ?>
					</aside>

				</li>

			<?php endwhile; else: ?>

				<li class="product-cards__item">

					<!-- PRODUCT CARD ITEM -->

					<h3 class="product-card__heading">No Rooms Available</h3>

					<!-- PRODUCT CARD ITEM -->

				</li>
			
			<?php endif; ?>

		</ol>
		<!-- PRODUCT CARD LIST -->
		
		<!-- PRODUCT CARD LIST -->
		<div class="headline headline--breathe">
			<h2 class="headline__heading">Private Room Pricing</h2>
		</div>
		
		<ol class="product-cards" data-module="toggle">

			<?php
			$private_rooms = new WP_Query(array(
				'post_type' => 'product',
				'post__in'  => $correlating_retreat,
				'orderby'  => 'menu_order',
				'order' 	=> 'ASC',
				'tax_query' => array(
					array(
						'taxonomy' => 'product_cat',
						'field'    => 'slug',
						'terms'    => 'private-rooms'
					)
				)
			));

			$ia = 0;
			
			if($private_rooms->have_posts()) : while($private_rooms->have_posts()) : $private_rooms->the_post();
			$ia++;
			?>

				<li class="product-cards__item">

					<!-- PRODUCT CARD ITEM -->

					<a href="#" class="product-card js-toggle__trigger" role="button" data-target="more-info-<?php echo $ia; ?>-<?php echo $product->id; ?>">
						<div class="product-card__media" role="presentation"><?php the_post_thumbnail(); ?></div>
						<div class="product-card__content">
							<div class="product-card__inner">
								<h3 class="product-card__heading"><?php the_title(); ?></h3>
								<strong class="product-card__sub-heading"><?php echo $product->get_price_html(); ?></strong>
								<p class="product-card__summary">
									<?php the_excerpt(); ?>
								</p>
								<p class="product-card__highlight">Book Here!</p>
							</div>
						</div>
					</a>

					<!-- PRODUCT CARD ITEM -->

					<aside class="modal js-toggle__target" id="more-info-<?php echo $ia; ?>-<?php echo $product->id; ?>">
						<?php get_template_part('content', 'retreat-modal'); ?>
					</aside>

				</li>
	
			<?php endwhile; else: ?>

				<li class="product-cards__item">

					<!-- PRODUCT CARD ITEM -->

					<h3 class="product-card__heading">No Rooms Available</h3>

					<!-- PRODUCT CARD ITEM -->

				</li>
			
			<?php endif; ?>

		</ol>
		<!-- PRODUCT CARD LIST -->
		<!-- GROUPS -->

	</div>
</main>
<!-- RETREAT -->

<?php get_footer(); ?>
