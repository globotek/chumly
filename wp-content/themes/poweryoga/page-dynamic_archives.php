<?php
/**
 * Created by PhpStorm.
 * User: matthew
 * Date: 16/8/18
 * Time: 12:31 PM
 */

get_header(); ?>

<?php $page = ( get_query_var( 'paged' ) ) ? absint( get_query_var( 'paged' ) ) : 1; ?>
<?php $current_url = get_the_permalink( get_the_ID() ); ?>

<main class="generic-page" data-module="search-classes">
	
	<?php get_template_part( 'search', 'class' ); ?>
	
	<div class="generic-page__secondary">
		<div class="wrapper">
			
			<!--			<div id="ajax_output"></div>-->
			
			<div class="videos">
				
				<div class="videos__archive is-visible">
					
					<div class="videos__headline headline">
						
						<h3 class="headline__heading"><?php echo $dynamic_data[ 'page_title' ]; ?></h3>
						
						<a href="/video-library" role="button" class="button-list__meta-button">
							<span class="button-list__meta-button__icon" role="presentation" data-icon="arrow_left_o"></span>
							<span class="button-list__meta-button__content">Back to Library</span>
						</a>
					
					</div>
					
					<div class="class-tiles">
						<ol class="class-tiles__list">
							
							<?php
							$query = new WP_Query( $dynamic_data[ 'query_args' ] );
							
							while( $query->have_posts() ) : $query->the_post(); ?>
								
								<?php get_template_part( 'content', 'class' ); ?>
							
							<?php endwhile; ?>
						
						</ol>
					</div>
				
				</div>
				
				<div class="videos__search-results">
					
					<div class="videos__headline headline">
						
						<h3 class="headline__heading">Your Search Results</h3>
					
					</div>
					
					<div class="class-tiles">
						
						<ol class="class-tiles__list"></ol>
					
					</div>
				
				</div>
				
				<?php if( $query->found_posts > 8 ) { ?>
					
					<div class="video__button">
						
						<a id="load_more_posts" data-action="load" href="#" rel="nofollow" class="button button--large">View More</a>
					
					</div>
				
				<?php } ?>
			
			</div>
		
		
		</div>
	
	</div>
</main>


<?php get_footer(); ?>
