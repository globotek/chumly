<?php
/**
 * Created by PhpStorm.
 * User: matthew
 * Date: 6/12/16
 * Time: 12:15 PM
 */
?>


<div class="social-sharing content__header__sharing">
	<ul class="social-sharing__list">
		
		<li class="social-sharing__list__item">
			
			<script>
				function center_dialog(url, title, w, h) {
					// Fixes dual-screen position                         Most browsers      Firefox
					var dualScreenLeft = window.screenLeft != undefined ? window.screenLeft : screen.left;
					var dualScreenTop  = window.screenTop != undefined ? window.screenTop : screen.top;
					
					var width  = window.innerWidth ? window.innerWidth : document.documentElement.clientWidth ? document.documentElement.clientWidth : screen.width;
					var height = window.innerHeight ? window.innerHeight : document.documentElement.clientHeight ? document.documentElement.clientHeight : screen.height;
					
					var left      = ((width / 2) - (w / 2)) + dualScreenLeft;
					var top       = ((height / 2) - (h / 2)) + dualScreenTop;
					var newWindow = window.open(url, title, 'scrollbars=yes, width=' + w + ', height=' + h + ', top=' + top + ', left=' + left);
					
					// Puts focus on the newWindow
					if(window.focus) {
						newWindow.focus();
					}
				}
			</script>
			
			<a data-icon="icon-social-facebook"
			   href="javascript:center_dialog('https://www.facebook.com/sharer/sharer.php?u=<?php echo get_the_permalink(); ?>', 'facebook_share', 600, 550);"
			   class="social-networks__link">
				<img src="<?php echo get_template_directory_uri() . '/images/icons/facebook_icon.svg'; ?>" alt="Share on Facebook" />
			</a>
		
		</li>
		
		
		<li class="social-sharing__list__item">
			<a data-icon="icon-social-twitter"
			   href="javascript:center_dialog('https://twitter.com/share?url=<?php echo get_the_permalink(); ?>', 'twitter_share', 600, 550);"
			   class="social-networks__link">
				<img src="<?php echo get_template_directory_uri() . '/images/icons/twitter_icon.svg'; ?>" alt="Share on Twitter" />
			</a>
		</li>
	
	</ul>
</div>



