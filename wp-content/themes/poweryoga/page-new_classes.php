<?php
	/**
	 * Created by PhpStorm.
	 * User: matthew
	 * Date: 16/8/18
	 * Time: 12:54 PM
	 */
	
	$today = date('m/d/Y');
	$thirty_days_ago = date('m/d/Y', strtotime('-30 days', strtotime($today)));
	
	$dynamic_data = array(
		'page_title' => 'New Classes',
		'query_args' => array(
			'post_type' => 'chumly_lesson',
			'posts_per_page' => 16
		)
	);
	
	dynamic_archive($dynamic_data);