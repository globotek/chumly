	<!-- GLOBAL CALL TO ACTION -->

	<section class="call-to-action">
		<div class="wrapper">
			<div class="call-to-action__content">
				<h2 class="call-to-action__heading">Give it a try</h2>

				<p class="call-to-action__summary">Sign up for a free trial and be on the mat in under a minute.</p>

				<div class="call-to-action__button">
					<a href="<?php echo home_url() . '/sign-up'; ?>" class="button button--large button--fill">Start Your 7-Day Free Online Trial</a>
				</div>
			</div>
		</div>

		<aside class="call-to-action__image" role="presentation" style="background-image: url(<?php echo get_template_directory_uri() . '/images/backgrounds/call-to-action.jpg'; ?>)"></aside>
	</section>

	<!-- GLOBAL CALL TO ACTION -->


	<footer class="site-foot" role="contentinfo">
		<div class="wrapper">
			<div class="site-foot__inner">
				<div class="site-foot__brand" role="presentation" data-icon="logo_alt"></div>
				<div class="site-foot__info">
					<span class="site-foot__info__copyright">Copyright &copy; <?php echo date('Y'); ?> Bryan Kest's Power Yoga™</span>
					<div class="site-foot__info__social">

						<?php
						/** See header.php for full documentation */

						/** Get all nav menus */
						$menu_locations = get_nav_menu_locations();

						/** Select the menu we want for current location. */
						$menu_id = $menu_locations['footer-social'];

						/** Get all the menu items for the chosen menu. */
						$menu_items = wp_get_nav_menu_items($menu_id);

						/** Make sure there are menu items to prevent error. */
						if(!empty($menu_items)){

							/** Now we have separated parent items from child items, we can construct the markup. */
							echo '<ol class="site-foot__info__social__list">';

								foreach($menu_items as $link){

									echo '<li>';

										echo '<a href="' . $link->url . '" target="' . $link->target . '">';
											echo '<span class="is-hidden--text">' . $link->title . '</span>';
											echo '<span data-icon="' . $link->attr_title . '" role="presentation"></span>';
										echo '</a>';

									echo '</li>';

								}

							echo '</ol>';

						}
						/** End menu markup creation. */
						?>

					</div>
				</div>

				<div class="site-foot__actions">
					<nav>

						<?php
						/** See header.php for full documentation */

						/** Get all nav menus */
						$menu_locations = get_nav_menu_locations();

						/** Select the menu we want for current location. */
						$menu_id = $menu_locations['footer'];

						/** Get all the menu items for the chosen menu. */
						$menu_items = wp_get_nav_menu_items($menu_id);

						/** Make sure there are menu items to prevent error. */
						if(!empty($menu_items)){

							/** Now we have separated parent items from child items, we can construct the markup. */
							echo '<ol class="site-foot__nav">';

								foreach($menu_items as $link){

									echo '<li>';

										echo '<a href="' . $link->url . '">' . $link->title . '</a>';

									echo '</li>';

								}

							echo '</ol>';

						}
						/** End menu markup creation. */
						?>

					</nav>

					<form class="site-foot__form">
						<?php echo do_shortcode('[mc4wp_form]'); ?>
					</form>

				</div>
			</div>
		</div>
	</footer>

	<?php wp_footer(); ?>

</body>
</html>
