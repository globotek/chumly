<?php
/**
 * Template Name: Page with Commenting
 */
?>

<?php get_header(); ?>

<?php the_post(); ?>

<?php if ( get_post_meta( get_the_ID(), 'hero_subheading', TRUE ) ): ?>
	
	<!-- HERO -->
	<section class="hero">
		<div class="wrapper">
			<header class="hero__content">
				<h1 class="hero__heading"><?php echo get_the_title(); ?></h1>
				<h2 class="hero__summary">
					<?php if ( empty( get_post_meta( get_the_ID(), 'hero_subheading', TRUE ) ) ) {
						echo get_the_excerpt();
					} else {
						echo get_post_meta( get_the_ID(), 'hero_subheading', TRUE );
					} ?>
				</h2>
			</header>
		</div>
		<aside class="hero__image" role="presentation" style="background-image: url(<?php echo wp_get_attachment_url( get_post_thumbnail_id() ); ?>)"></aside>
	</section>
	<!-- HERO -->

<?php endif; ?>
	
	<!-- PAGE CONTENT -->
	<main class="generic-page">
		
		<div class="generic-page__primary">
			
			<div class="content">
				
				<div class="content__body">
					
					<?php the_content(); ?>
				
				</div>
			
			</div>
			
			<div class="breathe--top">
				
				<div class="content chumly">
					
					<?php comments_template(); ?>
				
				</div>
			
			</div>
		
		</div>
				
	
	</main>
	<!-- PAGE CONTENT -->

<?php get_footer(); ?>
