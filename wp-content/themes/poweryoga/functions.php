<?php
function show_template() {
	global $template;
	
	echo '<strong>' . $template . '<br>Page ID: ' . get_the_ID() . '</strong>';
	
	echo '<script id="__bs_script__">//<![CDATA[
    document.write("<script async src=\'http://HOST:3000/browser-sync/browser-sync-client.js?v=2.18.13\'><\/script>".replace("HOST", location.hostname));
//]]></script>
';

}

//add_action( 'wp_footer', 'show_template' );
//add_action('wp_head', 'show_template');


function theme_rewrite_rules() {
	
	add_rewrite_tag( '%py_template%', '([^&]+)' );
	
	add_rewrite_rule( 'video-library/new-classes', 'index.php?py_template=new-classes', 'top' );
	add_rewrite_rule( 'video-library/my-favorites', 'index.php?py_template=my-favorites', 'top' );
	add_rewrite_rule( 'video-library/most-popular', 'index.php?py_template=most-popular', 'top' );
	
}

add_action( 'init', 'theme_rewrite_rules' );

function chumly_lesson_rewrite($args, $post_type){
	
	if ($post_type == 'chumly_lesson'){
		$args['rewrite']['slug'] = 'class';
	}
	
	return $args;
}

add_filter('register_post_type_args', 'chumly_lesson_rewrite', 10, 2);


function theme_templates( $template ) {
	
	switch( get_query_var( 'py_template' ) ) {
		
		case 'new-classes':
			
			$template = get_stylesheet_directory() . '/page-new_classes.php';
			
			break;
		
		case 'my-favorites':
			
			$template = get_stylesheet_directory() . '/page-my_favorites.php';
			
			break;
		
		case 'most-popular':
			
			$template = get_stylesheet_directory() . '/page-most_popular.php';
			
			break;
		
	}
	
	return $template;
	
}

add_action( 'template_include', 'theme_templates' );


function dynamic_archive( $dynamic_data ) {
	
	include_once( 'page-dynamic_archives.php' );
	
}


add_filter( 'auto_update_plugin', '__return_false' );


function theme_styles() {
	wp_enqueue_style( 'style', get_template_directory_uri() . '/style.css' );
	wp_enqueue_style( 'web-font', 'https://hello.myfonts.net/count/2efc7f' );
	wp_enqueue_style( 'global', get_template_directory_uri() . '/css/global.css' );
	wp_enqueue_style( 'events', get_template_directory_uri() . '/css/events.css' );
}

add_action( 'wp_enqueue_scripts', 'theme_styles' );


function theme_scripts() {
	wp_enqueue_script( 'app', get_template_directory_uri() . '/scripts/app.js', 'lib', FALSE, TRUE );
	wp_enqueue_script( 'functions', get_template_directory_uri() . '/scripts/functions.js', 'lib', FALSE, TRUE );
	wp_register_script( 'ajax-setup', get_stylesheet_directory_uri() . '/scripts/ajax-functions.js', 'jquery', FALSE, TRUE );
	wp_localize_script( 'ajax-setup', 'ajax_variables', array( 'ajax_url' => admin_url( 'admin-ajax.php' ), 'user_id' => get_current_user_id(), 'home_url' => home_url() ) );
	wp_enqueue_script( 'ajax-setup' );
}

add_action( 'wp_enqueue_scripts', 'theme_scripts' );


function theme_files() {
	include_once( get_template_directory() . '/includes/ajax-functions.php' );
	include_once( get_template_directory() . '/includes/blog-pagination.php' );
}

add_action( 'after_setup_theme', 'theme_files' );


add_theme_support( 'menus' );
add_theme_support( 'post-thumbnails' );
add_theme_support( 'automatic-feed-links' );
add_theme_support( 'woocommerce' );

add_image_size( 'avatar', 50, 50, TRUE );

register_nav_menus( array(
	'main'          => 'Main Menu',
	'account'       => 'Account Menu',
	'footer'        => 'Footer Menu',
	'footer-social' => 'Footer Social Links'
) );

/**
 * internet_allow_email_login filter to the authenticate filter hook, to fetch a username based on entered email
 *
 * @param  obj    $user
 * @param  string $username [description]
 * @param  string $password [description]
 *
 * @return boolean
 */
function py_email_login( $user, $username, $password ) {
	
	if( is_email( $username ) ) {
		
		$user = get_user_by( 'email', $username );
		if( $user ) {
			$username = $user->user_login;
		}
		
	}
	
	return wp_authenticate_username_password( NULL, $username, $password );
	
}

add_filter( 'authenticate', 'py_email_login', 20, 3 );


function checkout_return_to_cart_button() {
	echo '<a href="' . WC()->cart->get_cart_url() . '" class="input alt button return_to_cart">Return to Cart</a>';
}

add_action( 'woocommerce_after_checkout_form', 'checkout_return_to_cart_button' );


function form_field_css( $field ) {
	if( $field[ 'type' ] != 'checkbox' ) {
		
		$field[ 'input_class' ] = array( 'form__input' );
		
	}
	
	return $field;
}

add_filter( 'woocommerce_form_field_args', 'form_field_css' );


function auto_complete_order( $order_id ) {
	if( !$order_id ) {
		return;
	}
	
	$order = wc_get_order( $order_id );
	$order->update_status( 'completed' );
}

add_action( 'woocommerce_thankyou', 'auto_complete_order' );


function change_view_cart_text( $translated_text, $text, $domain ) {
	if( class_exists( 'WooCommerce' ) && is_woocommerce() && $translated_text == 'View Cart' ) {
		$translated_text = 'Edit Order';
		
		return $translated_text;
		
	} else {
		
		return $translated_text;
		
	}
}

add_filter( 'gettext', 'change_view_cart_text', 20, 3 );


function auto_add_coupon() {
	$cart_contents = WC()->cart->cart_contents;
	
	foreach( $cart_contents as $cart_item ) {
		if( $cart_item[ 'product_id' ] == 157 ) {
			WC()->cart->add_discount( sanitize_text_field( 'freemonth' ) );
			wc_print_notice( 'You will receive your first month free of charge.', 'success' );
		}
	}
}

//add_action('woocommerce_before_checkout_form', 'auto_add_coupon');


function toggle_display( $atts, $content ) {
	
	$output = '<a href="#" id="home-content__trigger" class="button button--large js-toggle__trigger" role="button">Read More</a>';
	
	$output .= '<div id="home-content__target" class="content__body" style="display:none">';
	
	$closure = '</div>';
	
	return $output . $content . $closure;
}

add_shortcode( 'toggle', 'toggle_display' );


function activate_widgets() {
	register_sidebar( array(
		'name' => 'Blog Post Sidebar',
		'id'   => 'blog_single',
	) );
	
	register_sidebar( array(
		'name' => 'Blog Post Mobile Sidebar',
		'id'   => 'blog_mobile_single',
	) );
}

add_action( 'widgets_init', 'activate_widgets' );


function py_get_icon( $icon ) {
	$string = '';
	$string .= '<svg class="icon" aria-hidden="true">';
	$string .= '<use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="' . get_template_directory_uri() . '/images/icons/svg-symbols.svg#' . $icon . '"></use>';
	$string .= '</svg>';
	
	return $string;
	
}


function py_chumly_main_sidebar_menu( $menu ) {
	
	unset( $menu[ 10 ][ 'nav_items' ][ 1 ] );
	unset( $menu[ 16 ] );
	unset( $menu[ 20 ][ 'nav_items' ][ 1 ] );
	
	return $menu;
	
}

add_filter( 'chumly_main_sidebar_menu', 'py_chumly_main_sidebar_menu' );