<?php get_header(); ?>

<?php $page = ( get_query_var( 'paged' ) ) ? absint( get_query_var( 'paged' ) ) : 1; ?>
<?php $current_url = get_the_permalink( get_the_ID() ); ?>
	
	<main class="generic-page" data-module="search-classes">
		
		<?php get_template_part( 'search', 'class' ); ?>
		
		<div class="generic-page__secondary">
			
			<div class="wrapper">
				
				<div id="ajax_output"></div>
				
				<div class="videos">
					
					<div class="videos__library is-visible">
						
						<div class="videos__inline">
							
							<div class=" videos__inline--quarter">
								
								<div class="videos__headline headline">
									
									<h3 class="headline__heading">Class of the Day</h3>
								
								</div>
								
								<div class="class-tiles">
									<ol class="class-tiles__list class-tiles__list--whole">
										
										<?php
										$today = date( 'Ymd' );
										
										$query = new WP_Query( array(
											'post_type'      => 'chumly_lesson',
											'meta_key'       => 'class_of_the_day',
											'meta_value'     => $today,
											'posts_per_page' => 1,
										) );
										
										if ( ! $query->found_posts ) {
											
											$query = new WP_Query( array(
												'post_type'      => 'chumly_lesson',
												'posts_per_page' => 1,
												'orderby'        => 'rand'
											) );
											
										}
										
										while ( $query->have_posts() ) : $query->the_post();
											
											get_template_part( 'content', 'class' );
										
										endwhile; ?>
									
									</ol>
								</div>
							
							</div>
							
							<div class="videos__inline--three-quarters">
								
								<div class="videos__headline headline">
									
									<a href="/video-library/new-classes">
										<h3 class="headline__heading">New Classes</h3>
									</a>
									
									<a href="/video-library/new-classes" role="button" class="button-list__meta-button">
                                        <span class="button-list__meta-button__icon" role="presentation"
                                              data-icon="proceed_o"></span>
										<span class="button-list__meta-button__content">View All</span>
									</a>
								
								
								</div>
								
								<div class="class-tiles">
									<ol class="class-tiles__list class-tiles__list--thirds">
										
										<?php
										$today           = date( 'm/d/Y' );
										$thirty_days_ago = date( 'm/d/Y', strtotime( '-30 days', strtotime( $today ) ) );
										
										$archive_query_args = array(
											'post_type'      => 'chumly_lesson',
											'posts_per_page' => 3,
											'post_parent'    => 0
										);
										
										$query = new WP_Query( $archive_query_args );
										
										while ( $query->have_posts() ) : $query->the_post(); ?>
											
											<?php get_template_part( 'content', 'class' ); ?>
										
										<?php endwhile; ?>
									
									</ol>
								</div>
							
							</div>
						
						</div>
						
						<div class="videos__headline headline">
							
							<a href="/video-library/my-favorites">
								<h3 class="headline__heading">My Favorites</h3>
							</a>
							
							<a href="/video-library/my-favorites" role="button" class="button-list__meta-button">
                                <span class="button-list__meta-button__icon" role="presentation"
                                      data-icon="proceed_o"></span>
								<span class="button-list__meta-button__content">View All</span>
							</a>
						
						
						</div>
						
						<div class="class-tiles">
							<ol class="class-tiles__list">
								
								<?php
								$favorites = get_user_meta( get_current_user_id(), 'favorited_classes', TRUE );
								
								$archive_query_args = array(
									'post_type'      => 'chumly_lesson',
									'posts_per_page' => 4,
									'post_parent'    => 0,
									'post__in'       => $favorites
								);
								
								$query = new WP_Query( $archive_query_args );
								
								while ( $query->have_posts() ) : $query->the_post(); ?>
									
									<?php get_template_part( 'content', 'class' ); ?>
								
								<?php endwhile; ?>
							
							</ol>
						</div>
						
						
						<div class="videos__headline headline">
							
							<a href="/video-library/most-popular">
								<h3 class="headline__heading">Most Popular</h3>
							</a>
							
							<a href="/video-library/most-popular" role="button" class="button-list__meta-button">
                                <span class="button-list__meta-button__icon" role="presentation"
                                      data-icon="proceed_o"></span>
								<span class="button-list__meta-button__content">View All</span>
							</a>
						
						</div>
						
						
						<div class="class-tiles">
							<ol class="class-tiles__list">
								
								<?php
								$archive_query_args = array(
									'post_type'      => 'chumly_lesson',
									'posts_per_page' => 4,
									'post_parent'    => 0
								);
								
								$query = new WP_Query( $archive_query_args );
								
								while ( $query->have_posts() ) : $query->the_post(); ?>
									
									<?php get_template_part( 'content', 'class' ); ?>
								
								<?php endwhile; ?>
							
							</ol>
						</div>
						
						<?php $programs = get_terms( 'chumly_program', array( 'hide_empty' => FALSE ) ); ?>
						
						<?php if ( ! empty( $programs ) ) { ?>
							
							<?php foreach ( $programs as $program ) { ?>
								
								<div class="videos__headline headline">
									
									<a href="<?php echo get_term_link( $program->term_id ); ?>">
										<h3 class="headline__heading"><?php echo $program->name; ?></h3>
									</a>
									
									<a href="<?php echo get_term_link( $program->term_id ); ?>" role="button" class="button-list__meta-button">
                                <span class="button-list__meta-button__icon" role="presentation"
                                      data-icon="proceed_o"></span>
										<span class="button-list__meta-button__content">View All</span>
									</a>
								
								</div>
								
								
								<div class="class-tiles">
									<ol class="class-tiles__list">
										
										<?php
										$program_modules = chumly_unserialize( get_term_meta( $program->term_id, 'chumly_elearning_linked_children', TRUE ) );
										
										$modules_filter = wp_list_pluck( $program_modules, 'ID' );
										
										$modules = get_terms( 'chumly_module', array( 'include' => $modules_filter, 'hide_empty' => FALSE ) );
										
										foreach ( $modules as $module ) { ?>
											
											<?php include( 'content-class_category.php' ); ?>
										
										<?php } ?>
									
									</ol>
								</div>
							
							<?php } ?>
						
						<?php } ?>
						
						
						<div class="videos__headline headline">
							
							<a href="<?php echo get_the_permalink( 58 ); ?>">
								<h3 class="headline__heading">Teachers</h3>
							</a>
							
							<a href="<?php echo get_the_permalink( 58 ); ?>" role="button"
							   class="button-list__meta-button">
                                <span class="button-list__meta-button__icon" role="presentation"
                                      data-icon="proceed_o"></span>
								<span class="button-list__meta-button__content">View All</span>
							</a>
						
						</div>
						
						<div class="class-tiles">
							<ol class="class-tiles__list class-tiles__list--centered">
								
								<?php
								$archive_query_args = array(
									'post_type'   => 'teacher',
									'post_parent' => 0,
									'tax_query'   => array(
										array(
											'taxonomy' => 'teacher_category',
											'field'    => 'slug',
											'terms'    => 'video-producer'
										)
									)
								);
								
								$query = new WP_Query( $archive_query_args );
								
								while ( $query->have_posts() ) : $query->the_post(); ?>
									
									<div class="chumly">
										
										<a href="<?php the_permalink(); ?>">
											
											<div class="avatar avatar--large avatar--round">
												
												<?php //chumly_avatar( $post->post_author, 'profile' ); ?>
												<?php the_post_thumbnail( 'large', array( 'class' => 'avatar__image' ) ); ?>
											
											</div>
											
											<p class="avatar__content"><?php echo chumly_username( $post->post_author ); ?></p>
										
										</a>
									
									</div>
								<?php endwhile; ?>
							
							</ol>
						</div>
					
					</div>
					
					<div class="videos__search-results">
						
						<div class="videos__headline headline">
							
							<h3 class="headline__heading"><?php the_search_query(); ?> Your Search Results</h3>
						
						</div>
						
						<div class="class-tiles">
							
							<ol class="class-tiles__list"></ol>
						
						</div>
						
						<div class="video__button">
							
							<a id="load_more_posts" data-action="load" href="#" rel="nofollow" class="button button--large">Load More</a>
						
						</div>
					
					</div>
				
				</div>
			
			
			</div>
		
		</div>
	
	</main>

<?php get_footer(); ?>