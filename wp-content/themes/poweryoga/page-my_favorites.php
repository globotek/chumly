<?php
/**
 * Created by PhpStorm.
 * User: matthew
 * Date: 16/8/18
 * Time: 1:04 PM
 */

$favorites = get_user_meta(get_current_user_id(), 'favorited_classes', true);

$dynamic_data = array(
	'page_title' => 'My Favorites',
	'query_args' => array(
		'post_type' => 'chumly_lesson',
		'posts_per_page' => 16,
		'post__in' => $favorites
	)
);

dynamic_archive($dynamic_data);