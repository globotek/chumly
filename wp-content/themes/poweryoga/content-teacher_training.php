<li class="product-cards__item">

	<!-- PRODUCT CARD ITEM -->

	<div class="product-card">
		<div class="product-card__media" role="presentation"  style="background-image: url(<?php echo wp_get_attachment_url(get_post_thumbnail_id()); ?>)"></div>
		<div class="product-card__content">
			<div class="product-card__inner">
				<h3 class="product-card__heading"><?php the_title(); ?></h3>

				<div class="product-card__summary">
					<?php the_content(); ?>
				</div>
			</div>
		</div>
	</div>
	
	<!-- PRODUCT CARD ITEM -->

</li>
