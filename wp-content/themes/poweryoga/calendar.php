<?php //Template Name: Calendar ?>
<?php get_header(); ?>
<?php $page_id = 72; ?>
<?php $hero_image_url = wp_get_attachment_url(get_post_thumbnail_id($page_id)); ?>

<?php the_post(); ?>
<!-- HERO -->
<section class="hero">
	<div class="wrapper">
		<header class="hero__content">
			<h1 class="hero__heading"><?php echo get_the_title($page_id); ?></h1>
			<h2 class="hero__summary"><?php echo get_post_meta($page_id, 'hero_subheading', true); ?></h2>
		</header>
	</div>
	<aside class="hero__image" role="presentation" style="background-image: url(<?php echo $hero_image_url; ?>)"></aside>
</section>
<!-- HERO -->


<!-- PAGE CONTENT -->
<main class="generic-page">
	<div class="generic-page__primary">
		<div class="wrapper">
			<?php the_content(); ?>
        </div>
    </div>
</main>

<?php get_footer(); ?>
