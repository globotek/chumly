<!DOCTYPE html>
<!--[if IE 9]>
<html dir="ltr" lang="en-US" class="ie9 lt-ie10"><![endif]-->
<!--[if (gte IE 9)|!(IE)]><!-->
<html dir="ltr" lang="en-US"><!--<![endif]-->
<head>
	<meta charset="UTF-8">
	<meta http-equiv="X-UA-Compatible" content="IE=Edge,chrome=1">
	
	<title><?php wp_title(); ?></title>
	
	<meta name="viewport" content="width=device-width, initial-scale=1"/>
	
	<link rel="stylesheet" href="//hello.myfonts.net/count/2efc7f" media="all"/>
	<link rel="shortcut icon" href="/images/favicon.ico" type="image/x-icon"/>
	<link rel="apple-touch-icon" href="/images/apple-touch-icon.png"/>
	<link rel="apple-touch-icon" sizes="57x57" href="/images/apple-touch-icon-57x57.png"/>
	<link rel="apple-touch-icon" sizes="72x72" href="/images/apple-touch-icon-72x72.png"/>
	<link rel="apple-touch-icon" sizes="76x76" href="/images/apple-touch-icon-76x76.png"/>
	<link rel="apple-touch-icon" sizes="114x114" href="/images/apple-touch-icon-114x114.png"/>
	<link rel="apple-touch-icon" sizes="120x120" href="/images/apple-touch-icon-120x120.png"/>
	<link rel="apple-touch-icon" sizes="144x144" href="/images/apple-touch-icon-144x144.png"/>
	<link rel="apple-touch-icon" sizes="152x152" href="/images/apple-touch-icon-152x152.png"/>
	
	<?php wp_head(); ?>
	
	<!-- Global site tag (gtag.js) - Google Analytics -->
	<script async src="https://www.googletagmanager.com/gtag/js?id=UA-96947295-4"></script>
	<script>
		window.dataLayer = window.dataLayer || [];
		function gtag(){dataLayer.push(arguments);}
		gtag('js', new Date());
		
		gtag('config', 'UA-96947295-4');
	</script>


</head>


<body <?php body_class(); ?>>

<header class="site-head" role="banner" data-module="toggle">
	<div class="site-head__mask js-toggle__trigger" data-target="user-menu"></div>
	<div class="wrapper">
		<div class="site-head__inner">
			<div class="site-head__brand">
				<a href="/" title="Back to homepage" data-icon="logo"></a>
			</div>
			<nav class="site-head__nav">
				
				<a class="site-head__hamburger js-toggle__trigger" href="#main-menu" data-target="main-menu">
					<span class="hamburger"><span class="hamburger__inner"></span></span>
					<span class="is-hidden--text">Open menu</span>
				</a>
				
				<?php
				/**
				 * In this section, we're going to take the menus created in the admin area
				 * and feed each item into the markup. We need to process the menus as to split
				 * them into parent and child menu items so they can be arranged accordingly.
				 *
				 *
				 * First, we get all available menus based on what's stipulated in functions.php
				 * so that we're not reliant on what the menu has been called by the user in the
				 * admin area.
				 */
				$menu_locations = get_nav_menu_locations();
				
				/** Select the menu we want for current location, in this case, header so main. */
				$menu_id = $menu_locations[ 'main' ];
				
				/** Get all the menu items for the chosen menu. */
				$menu_items = wp_get_nav_menu_items( $menu_id );
				
				/** Make sure there are menu items to prevent error. */
				if( !empty( $menu_items ) ) {
					
					/**
					 * We need to split the menu items into their respective parent and child denotions.
					 * To do this, we loop over all the menu item and take the ID and parent ID of the
					 * menu item. Parent's will always have parent ID of 0 so we use that to determine if
					 * it's a parent menu item and drop it into the parent array. If the parent ID is more
					 * than 0, it's a child item so it gets dropped in the child array. We use the child's
					 * parent ID as a key so we can group all child items together in one array which we can
					 * query easily using the parent ID, as the children are to be attached to the parent.
					 */
					foreach( $menu_items as $link ) {
						$link_id = $link->ID;
						$parent_id = $link->menu_item_parent;
						
						if( $parent_id == 0 ) {
							$parent_array[] = $link_id;
						}
						
						if( $parent_id > 0 ) {
							$child_array[ $parent_id ][] = $link_id;
						}
					}
					
					/** Now we have separated parent items from child items, we can construct the markup. */
					echo '<ol class="site-head__nav__list js-toggle__target" id="main-menu">';
					
					/** Loop over all menu items again, as to build the lists.*/
					foreach( $menu_items as $link ) {
						$link_id = $link->ID;
						$parent_id = $link->menu_item_parent;
						
						/**
						 * We check if the menu item ID exists as a key in the child array. If it does, it has
						 * child items associated with it.
						 */
						if( !empty( $child_array ) && array_key_exists( $link_id, $child_array ) ) {
							
							/**
							 * As we've separated items with submenus from those that don't, we can now output
							 * the necessary submenu markup independently from those with no submenu.
							 */
							echo '<li>';
							
							echo '<a href="' . $link->url . '" class="js-toggle__trigger" data-target="' . $link->ID . '">';
							echo $link->title;
							//echo '<i class="site-head__nav__icon js-site-head__icon" data-icon="add" data-orig="add" data-active="minus"></i>';
							echo '</a>';
							
							//echo '<ol class="site-head__nav__list site-head__nav__list--child js-site-head__subnav">';
							echo '<ol class="site-head__user-menu js-toggle__target" id="' . $link->ID . '" style="width: auto; right: auto;">';
							/**
							 * We have the IDs of child items stored but no data so we need to loop over
							 * all the menu items again, in order to get the data for each menu item ID.
							 * Due to previously using the parent ID as the key for each set of child items
							 * we can easily pick the array of child item IDs we want to look at, thus linking
							 * them to the correct parent ID in the markup.
							 */
							foreach( $child_array[ $link_id ] as $sub_link_id ) {
								
								foreach( $menu_items as $sub_link ) {
									
									/**
									 * As we iterate, we check to see if the child ID matches the menu item ID
									 * and if it does, we output a list item and feed in appropriate data.
									 */
									if( $sub_link->ID == $sub_link_id ) {
										
										echo '<li style="white-space: nowrap;">';
										echo '<a href="' . $sub_link->url . '">' . $sub_link->title . '</a>';
										echo '</li>';
										
									}
									
								}
								
							}
							
							echo '</ol>';
							
							echo '</li>';
							
						} elseif( $link->menu_item_parent == 0 ) {
							
							echo '<li>';
							
							echo '<a href="' . $link->url . '">' . $link->title . '</a>';
							
							echo '</li>';
							
						}
						
					}
					
					echo '<li>';
					
					if( is_user_logged_in() ) {
						
						/** Select the menu we want for current location, in this case, header so main. */
						$menu_id = $menu_locations[ 'account' ];
						
						/** Get all the menu items for the chosen menu. */
						$menu_items = wp_get_nav_menu_items( $menu_id );
						
						echo '<a href="#" class="button button--upper button--small button--icon js-toggle__trigger" data-target="user-menu">Hello, ' . chumly_username(get_current_user_id(), 'first') . '<span class="button__icon" data-icon="arrow_down" role="presentation"></span></a>';
						
						
						if( !empty( $menu_items ) ) {
							
							echo '<ol class="site-head__user-menu js-toggle__target" id="user-menu">';
							
							foreach( $menu_items as $menu_item ) {
								
								echo '<li>';
								
								echo '<a href="' . $menu_item->url . '">';
								
								echo '<span data-icon="' . $menu_item->attr_title . '" role="presentation"></span>';
								echo $menu_item->title;
								
								echo '</a>';
								
								echo '</li>';
								
							}
							
							echo '</ol>';
							
						}
						
						
					} else {
						
						echo '<a href="#" class="button button--upper button--small js-toggle__trigger" data-target="login-window">login</a>';
						
					}
					
					echo '</li>';
					
					echo '</ol>';
					
				}
				/** End menu markup creation. */
				?>
			
			</nav>
			
			<?php if( is_user_logged_in() ) { ?>
				
				<div class="chumly" data-module="chumly-toggle">
					
					<nav class="user-menu">
						
						<ul class="user-menu__inner">
							<?php
							
							$user_menu = new Chumly_User_Menu();
							
							echo $user_menu->notifications();
							//echo $user_menu->messages();
							echo $user_menu->friends();
							echo $user_menu->settings();
							
							?>
						</ul>
					
					</nav>
				
				</div>
			
			<?php } ?>
		
		</div>
	</div>
	
	
	<!-- MODAL WINDOW -->
	
	<aside class="modal js-toggle__target" id="login-window">
		<div class="modal__window">
			<h2 class="modal__window__header">
				<span role="presentation" data-icon="user"></span>Member Login
			</h2>
			<div class="modal__window__content">
				
				<!-- NOTIFICATION COMPONENT -->
				<div class="prompt" style="display:none">
					<div class="prompt__icon" role="presentation" data-icon="cross_o"></div>
					<div class="prompt__content">
						Your login details were incorrect. Please try again
					</div>
				</div>
				<!-- NOTIFICATION COMPONENT -->
				
				<!--<h3 class="modal__window__heading">Namaste</h3>-->
				<p class="modal__window__summary">Welcome back to your yoga practice!</p>
				<div class="modal__window__content__inner">
					<form class="form">
						<div class="form__group">
							<label class="form__label" for="login_user">Username</label>
							<input type="text" name="login_user" id="login_user" class="form__input" placeholder="Username"/>
						</div>
						<div class="form__group">
							<label class="form__label" for="login_password">Password</label>
							<input type="password" name="login_password" id="login_password" class="form__input" placeholder="Password"/>
						</div>
						<div class="form__split form__split--flush">
							<div class="form__split__item">
								<div class="form__check">
									<div class="form__check__input">
										<input type="checkbox" name="login_remember" id="login_remember"/>
										<label class="form__check__decor" for="login_remember" data-icon="check"></label>
									</div>
									<label class="form__check__label" for="login_remember">Remember me</label>
								</div>
							</div>
							<div class="form__split__item">
								<a href="<?php echo chumly_password_reset_url(); ?>" class="form__helper-link">Forgot username or password?</a>
							</div>
						</div>
						<div class="form__group form__group--center form__group--breathe">
							<button type="submit" id="login_submit" class="button button--fill">Login</button>
						</div>
					</form>
				</div>
			</div>
			<a href="#" class="modal__window__close js-toggle__trigger" data-target="login-window">
				<span class="is-hidden--text">Close this window</span>
				<span role="presentation" data-icon="cross_o"></span>
			</a>
		</div>
		<div class="modal__overlay js-toggle__trigger" data-target="login-window"></div>
	</aside>
	
	<!-- MODAL WINDOW -->

</header>