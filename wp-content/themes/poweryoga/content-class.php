<?php $user_favorites = get_user_meta( get_current_user_id(), 'favorited_classes', TRUE ); ?>
<?php set_query_var( 'chumly_page', FALSE ); ?>

<?php if ( $user_favorites ) {
	
	if ( in_array( $post->ID, $user_favorites ) ) {
		
		$is_favorite = 'is-visible';
		
	}
	
} ?>

<li class="class-tiles__item">
	<!-- TILE COMPONENT -->
	<a class="content-tile content-tile--eggshell" href="<?php the_permalink(); ?>">
		<div class="content-tile__media">
			<div class="content-tile__media__image">
				
				<img src="<?php echo wp_get_attachment_url( get_post_thumbnail_id() ); ?>"/>
				<!-- ADD 'is-visible' class  to 'content-tile__heading__icon' to show 'heart' -->
				<span class="content-tile__media__icon <?php echo $is_favorite; ?>" role="presentation" data-icon="heart_fill"></span>
			
			</div>
			
			<!-- ADD 'is-visible' class to show 'tick' -->
			<span class="content-tile__featured-flag" role="presentation">
                <span data-icon="check"></span>
            </span>
		
		</div>
		
		<div class="content-tile__inner">
			<div class="content-tile__content">
				
				<h3 class="content-tile__heading">
					<?php the_title(); ?>
					<?php chumly_output_ratings( FALSE ); ?>
				</h3>
				<div class="content-tile__summary"><?php the_excerpt(); ?></div>
			</div>
			
			
			<div class="content-tile__meta">
				
				<!-- CONTENT META COMPONENT -->
				<div class="content-meta">
					<ul class="content-meta__row">
						<li class="content-meta__item">
							
							<!-- USER AVATAR COMPONENT -->
							<div class="user-avatar user-avatar--centered">
								
								<div class="user-avatar__image">
									<?php chumly_avatar( get_the_author_meta( 'ID' ), 'profile', '' ); ?>
								</div>
								
								<div class="user-avatar__content">
									<p><?php the_author(); ?></p>
								</div>
							
							</div>
							<!-- USER AVATAR COMPONENT -->
						
						</li>
						<li class="content-meta__item">
							<p><?php echo get_post_meta( $post->ID, 'video_runtime', TRUE ) . ' minutes, ' . lcfirst( get_post_meta( $post->ID, 'class_level', TRUE ) ) . ' intensity'; ?></p>
						</li>
					</ul>
				
				</div>
				<!-- CONTENT META COMPONENT -->
			
			</div>
		</div>
	</a>
	<!-- TILE COMPONENT -->

</li>
