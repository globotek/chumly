<?php
/**
 * Empty cart page
 *
 * @author  WooThemes
 * @package WooCommerce/Templates
 * @version 2.0.0
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}

wc_print_notices();

?>

<p class="cart-empty" style="text-align:center"><?php _e( 'Your cart is currently empty.', 'woocommerce' ) ?></p>

<?php do_action( 'woocommerce_cart_is_empty' ); ?>

<p class="return-to-shop" style="margin-top:20px; text-align:center;"><a class="button wc-backward" href="<?php echo esc_url(home_url()); ?>"><?php _e( 'Home', 'woocommerce' ) ?></a></p>
