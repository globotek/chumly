<?php //Template Name: Pricing ?>
<?php get_header(); ?>

<!-- HERO -->
<section class="hero">
	<div class="wrapper">
		<header class="hero__content">
			<h1 class="hero__heading"><?php echo get_the_title(); ?></h1>
			<h2 class="hero__summary"><?php echo get_post_meta(get_the_ID(), 'hero_subheading', true); ?></h2>
		</header>
	</div>
	<aside class="hero__image" role="presentation" style="background-image: url(<?php echo wp_get_attachment_url(get_post_thumbnail_id()); ?>)"></aside>
</section>
<!-- HERO -->

<!-- PRICING LAYOUT -->
<main class="pricing">
	<div class="pricing__wrapper">
		<form>
			<ol class="pricing__list">
				
				<?php
				$pricing_table_post = get_posts(array('post_type' => 'easy-pricing-table'));
				$pricing_table_data = get_post_meta($pricing_table_post[0]->ID, '1_dh_ptp_settings', true);
				$pricing_table = $pricing_table_data['column'];
				$i = 0;
				foreach($pricing_table as $pricing_column){
					
					$i++;
					$pricing_features = explode("\n", $pricing_column['planfeatures']);
					?>
					
					<li class="pricing__item">
						<input type="radio" name="price" id="pricing_<?php echo $i; ?>" class="pricing__radio" />
						<label class="pricing__inner" for="pricing_<?php echo $i; ?>">
							<h3 class="pricing__heading"><?php echo $pricing_column['planname']; ?></h3>
							<div class="pricing__icon" data-icon="decor_yoga_<?php echo $i; ?>" role="presentation"></div>
							<h4 class="pricing__price"><?php echo $pricing_column['planprice']; ?></h4>
							<span class="pricing__meta"><?php echo $pricing_column['buttontext']; ?></span>
<!--							<input type="hidden" class="checkout_url" value="--><?php //echo $pricing_column['buttonurl']; ?><!--" />-->
							
							<ol class="pricing__points">
								
								<?php foreach($pricing_features as $feature){ ?>
								
									<?php if(!empty($feature)){ ?>
								
										<li class="pricing__point">
											<span class="pricing__point__icon" role="presentation" data-icon="check"></span>
											<span class="pricing__point__text"><?php echo $feature; ?></span>
										</li>
									
									<?php } ?>
							
								<?php } ?>
								
							</ol>
							
							<div class="pricing__button">
                                <?php global $woocommerce; ?>
                                <?php $checkout_url = $woocommerce->cart->get_checkout_url() . $pricing_column['buttonurl']; ?>
								<a href="<?php echo $checkout_url; ?>" type="submit" class="button button--large button--fill">Choose this plan</a>
							</div>

						</label>
					</li>
				
				<?php } ?>
				
			</ol>
		</form>
	</div>
</main>
<!-- PRICING LAYOUT -->

<?php get_footer(); ?>