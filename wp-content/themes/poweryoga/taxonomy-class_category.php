<?php get_header(); ?>

<?php $query_object = get_queried_object(); ?>

<div class="landing-page">
	<!-- LANDING HERO CTA -->
	<section class="landing-page__hero landing-page__hero--without-video">
		<div class="landing-page__hero__content">
			<div class="wrapper">
				<h1 class="landing-page__hero__heading"><?php single_cat_title(); ?></h1>
				
				<!--<h2 class="landing-page__hero__heading">From the Best Yoga Teachers</h2>-->
				
				<div class="landing-page__hero__center">
					<ul class="landing-page__hero__list">
						<li><h3 class="landing-page__hero__heading">Unlimited Access From Any Device</h3></li>
						
						<li><h3 class="landing-page__hero__heading">Choose Your Favorite Yoga Teacher</h3></li>
						
						<li><h3 class="landing-page__hero__heading">Select Different Yoga Levels</h3></li>
						
						<li><h3 class="landing-page__hero__heading">Choose from 5 to 120 Minutes</h3></li>
					</ul>
				</div>
				
				<div class="landing-page__hero__subscribe-cta__primary-action">
					<a href="<?php echo home_url() . '/sign-up'; ?>" class="button button--large button--fill">7-Day
						Free Online Trial</a>
				</div>
			</div>
			
			<div class="landing-page__hero__image" style="background-image: url(<?php echo wp_get_attachment_url(get_term_meta($query_object->term_id, 'term_image', true)); ?>)"></div>
			
		</div>
	</section>
	<!-- LANDING HERO CTA -->
</div>

<div class="generic-page__secondary">
	
	<div class="wrapper">
		
		<p class="content__header" style="margin: -30px 0 -60px 0;">
		<span xmlns:v="http://rdf.data-vocabulary.org/#">
			<span typeof="v:Breadcrumb">
				<a href="<?php echo home_url(); ?>" rel="v:url" property="v:title">Home</a> »
				<span rel="v:child" typeof="v:Breadcrumb">
					<a href="<?php echo home_url() . '/yoga-videos/'; ?>" rel="v:url" property="v:title">Classes</a> »
					<span class="breadcrumb_last"><?php single_cat_title(); ?></span>
				</span>
			</span>
		</span>
		</p>
		<!-- LANDING TILES -->
		<div class="content-tiles">
			
			<ol class="content-tiles__list">
				<?php
				$query = new WP_Query(array(
					'post_type'      => 'class',
					'posts_per_page' => 3,
					'tax_query'      => array(
						array(
							'taxonomy' => 'class_category',
							'field'    => 'ID',
							'terms'    => $query_object->term_id
						)
					)
				));
				
				while($query->have_posts()) : $query->the_post(); ?>
					
					<?php get_template_part('content', 'class'); ?>
				
				<?php endwhile; ?>
			
			</ol>
		</div>
		<!-- LANDING TILES -->
		
	</div>
</div>
	

<div class="landing-page">
	<!-- FEATURES -->
	<main class="landing-page__features" id="main-content">
		<div class="wrapper">
			<div class="landing-page__features__inner">
				
				<?php $information_ctas = get_field('information_ctas', 'class_category_' . $query_object->term_id); ?>
				
				<?php foreach($information_ctas as $feature){ ?>
					
					<article class="landing-page__feature">
						<h3 class="landing-page__feature__heading"><?php echo $feature['title']; ?></h3>
						<span role="presentation" class="landing-page__feature__icon" data-icon="<?php echo $feature['svg_id']; ?>"></span>
						<h4 class="landing-page__feature__sub-heading"><?php echo $feature['subtitle']; ?></h4>
						<p class="landing-page__feature__summary"><?php echo $feature['intro']; ?></p>
					</article>
				
				<?php } ?>
			
			</div>
			
			<div class="landing-page__features__button">
				<a href="<?php echo home_url() . '/sign-up'; ?>" class="button button--large button--fill">Subscribe today</a>
			</div>
		
		</div>
	</main>
	<!-- FEATURES -->
	
	<!-- YOGA CATEGORIES -->
	<section class="landing-page__tiles">
		
		<!-- TILES -->
		<div class="wrapper">
			
			<!-- HEADLINE -->
			<div class="headline">
				<div class="headline__heading">Yoga Categories</div>
			</div>
			<!-- HEADLINE -->

			<div class="content-tiles">
				
				<ol class="content-tiles__list">
					
					<?php $categories = get_terms('class_category', array('hide_empty' => false)); ?>
					
					<?php foreach($categories as $category){ ?>
						
						<?php include('content-class_categories.php'); ?>
					
					<?php } ?>
				
				</ol>
			</div>
		</div>
		<!-- TILES -->
		
	</section>
	<!-- YOGA CATEGORIES -->
	
	<!-- CONTENT REVEAL -->
	<section class="landing-page__testimonial">
		<div class="wrapper content__body home-content">
			<?php echo the_field('term_content', 'class_category_' . $query_object->term_id); ?>
		</div>
	</section>
	<!-- CONTENT REVEAL -->
	
</div>


<?php get_footer(); ?>
