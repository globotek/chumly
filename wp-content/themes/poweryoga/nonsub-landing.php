<?php // Template Name: Non-sub Video Landing ?>
<?php get_header(); ?>

<?php if(is_user_logged_in()){ ?>
	
	<?php wp_safe_redirect(get_permalink( get_page_by_title( 'Videos' ) )); ?>

<?php } ?>

<div class="landing-page">
	<!-- LANDING HERO CTA -->
	<section class="landing-page__hero landing-page__hero--without-video">
		<div class="landing-page__hero__content">
			<div class="wrapper">
				<h1 class="landing-page__hero__heading">Online Yoga Videos<br>From the Best Yoga Teachers</h1>
				
				<div class="landing-page__hero__center">
					<ul class="landing-page__hero__list">
						<li><h3 class="landing-page__hero__heading">Unlimited Access From Any Device</h3></li>
						
						<li><h3 class="landing-page__hero__heading">Choose Your Favorite Yoga Teacher</h3></li>
						
						<li><h3 class="landing-page__hero__heading">Select Different Yoga Levels</h3></li>
						
						<li><h3 class="landing-page__hero__heading">Choose from 5 to 120 Minutes</h3></li>
					</ul>
				</div>
				
				<div class="landing-page__hero__subscribe-cta__primary-action">
					<a href="<?php echo home_url() . '/sign-up'; ?>" class="button button--large button--fill">7-Day
						Free Online Trial</a>
				</div>
			</div>
			
			<div class="landing-page__hero__image" style="background-image: url(<?php the_post_thumbnail_url('full') ?>)"></div>
		
		</div>
	</section>
	<!-- LANDING HERO CTA -->
	
	<!-- LANDING TILES -->
	<section class="landing-page__tiles">
		
		<!-- TILES -->
		
		<div class="wrapper">
			
			<!-- HEADLINE -->
			
			<div class="headline">
				<h2 class="headline__heading">Free Yoga Videos</h2>
			</div>
			
			<!-- HEADLINE -->
			
			<div class="content-tiles">
				
				<ol class="content-tiles__list">
					<?php
					$query = new WP_Query(array(
						'post_type'   	 => 'class',
						'posts_per_page' => 3,
						'tax_query' 	 => array(
							array(
								'taxonomy'  => 'class_filter',
								'field'     => 'slug',
								'terms'     => 'sales-landing-videos'
							)
						)
					));
					
					while($query->have_posts()) : $query->the_post(); ?>
						
						<?php get_template_part('content', 'class'); ?>
					
					<?php endwhile; wp_reset_query(); ?>
				
				</ol>
			</div>
		</div>
		<!-- TILES -->
	
	</section>
	<!-- LANDING TILES -->
	
	
	<!-- FEATURES -->
	<main class="landing-page__features" id="main-content">
		<div class="wrapper">
			<div class="landing-page__features__inner">
				
				
				<?php $child_pages = get_pages('hierarchical=0&sort_column=menu_order&parent=' . get_the_ID()); ?>
			
				<?php foreach($child_pages as $feature){ ?>
					
					<article class="landing-page__feature">
						<h3 class="landing-page__feature__heading"><?php echo $feature->post_title; ?></h3>
						<span role="presentation" class="landing-page__feature__icon" data-icon="<?php echo get_post_meta($feature->ID, 'feature_icon', true); ?>"></span>
						<h4 class="landing-page__feature__sub-heading"><?php echo get_post_meta($feature->ID, 'sub_heading', true); ?></h4>
						<p class="landing-page__feature__summary"><?php echo $feature->post_content; ?></p>
					</article>
				
				<?php } ?>
			
			</div>
			
			<div class="landing-page__features__button">
				<a href="<?php echo home_url() . '/sign-up'; ?>" class="button button--large button--fill">Subscribe today</a>
			</div>
		
		</div>
	</main>
	<!-- FEATURES -->
	
	
	<!-- YOGA CATEGORIES -->
	<section class="landing-page__tiles">
		
		<!-- TILES -->
		<div class="wrapper">
			
			<!-- HEADLINE -->
			<div class="headline">
				<div class="headline__heading">Yoga Categories</div>
			</div>
			<!-- HEADLINE -->
			
			<div class="content-tiles">
				
				<ol class="content-tiles__list">
					
					<?php $categories = get_terms('class_category', array('hide_empty' => false)); ?>
					
					<?php foreach($categories as $category){ ?>
						
						<?php include('content-class_categories.php'); ?>
					
					<?php } ?>
				
				</ol>
			</div>
		</div>
		<!-- TILES -->
	
	</section>
	<!-- YOGA CATEGORIES -->
	
	<!-- CONTENT REVEAL -->
	<section class="landing-page__testimonial">
		<div class="wrapper content__body home-content">
			
			<?php the_post(); the_content(); ?>
		
		</div>
	</section>
	<!-- CONTENT REVEAL -->

</div>

<?php get_footer(); ?>
