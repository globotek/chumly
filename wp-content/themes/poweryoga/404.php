<?php get_header(); ?>

<?php the_post(); ?>

<!-- HERO -->
<section class="hero">
	<div class="wrapper">
		<header class="hero__content">
			<h1 class="hero__heading">Page Not Found</h1>
			<h2 class="hero__summary">404 error</h2>
		</header>
	</div>
	<aside class="hero__image" role="presentation" style="background-image: url(<?php echo get_template_directory_uri() . '/images/backgrounds/hero.jpg'; ?>)"></aside>
</section>
<!-- HERO -->


<!-- PAGE CONTENT -->
<main class="generic-page">
	<div class="generic-page__primary">
		<div class="wrapper">

        	<p>Unfortunately, it seems we don't have a page for what you're looking for.</p> 

        </div>
	</div>
</main>
<!-- PAGE CONTENT -->

<?php get_footer(); ?>
