<?php
$class_filters = get_terms( 'class_filter', array(
	'parent'  => 0,
	'exclude' => array( 21, 54, 127, 135 )
) );

$teacher_filters = get_users( array(
	'role' => 'author'
) );

$favorite_filters = array();
?>

<div class="generic-page__primary generic-page__primary--narrow">
	<div class="wrapper">
<!--		<div id="ajax_response"></div>-->
		<!-- FILTER MENU -->
		
		<div class="filter-menu">
			
			<h2 class="filter-menu__heading">Find classes by</h2>
			
			<input type="hidden" id="search_post_type" name="post_type" value="chumly_lesson"/>
			
			
			
			<div class="filter-menu__group">
				<input id="keyword-search" class="filter-menu__searchbox" type="search" name="text-search" placeholder="Search by title, description or teacher"/>
				<ol class="filter-menu__list">
					
					<?php foreach( $class_filters as $main_filter ) { ?>
						
						<li class="filter-menu__item">
							<div class="filter-menu__item__inner" data-module="hidden-field">
								<span class="filter-menu__item__text js-hidden-field__value"><?php echo $main_filter->name; ?></span>
								<span class="filter-menu__item__icon" role="presentation" data-icon="arrow_down_o"></span>
								
								<select class="hidden-field js-hidden-field__input" data-type="<?php echo strtolower( $main_filter->name ); ?>">
									
									<option value="null" data-title="<?php echo $main_filter->name; ?>"><?php echo 'All ' . $main_filter->name; ?></option>
									
									<?php
									$child_filters = get_terms( 'class_filter', array(
										'child_of' => $main_filter->term_id
									) );
									
									foreach( $child_filters as $single_filter ) { ?>
										
										<option value="<?php echo $single_filter->term_id; ?>" data-title="<?php echo $single_filter->name; ?>" data-type="<?php echo $single_filter->parent; ?>" <?php _e( isset( $_GET[ 'term_id' ] ) && in_array( $single_filter->term_id, $_GET[ 'term_id' ] ) ? 'selected' : '' ); ?>><?php echo $single_filter->name; ?></option>
									
									<?php } ?>
								
								</select>
							
							</div>
						</li>
					
					<?php } ?>
					
					<li class="filter-menu__item">
						<div class="filter-menu__item__inner" data-module="hidden-field">
							<span class="filter-menu__item__text js-hidden-field__value">Teacher</span>
							<span class="filter-menu__item__icon" role="presentation" data-icon="arrow_down_o"></span>
							
							<select class="hidden-field js-hidden-field__input" name="teacher_id" data-type="teacher">
								
								<option value="null" data-title="Teacher">All Teachers</option>
								
								<?php foreach( $teacher_filters as $teacher ) {
									
									if( count_user_posts( $teacher->ID, 'chumly_lesson' ) > 0 ) { ?>
										
										<option value="<?php echo $teacher->ID; ?>" data-title="<?php echo $teacher->data->display_name; ?>" <?php selected( $_GET[ 'teacher_id' ], $teacher->ID ); ?>><?php echo $teacher->data->display_name; ?></option>
									
									<?php } ?>
								
								<?php } ?>
							
							</select>
						</div>
					</li>
					
					
					<?php if( is_user_logged_in() ) { ?>
						
<!--						<li class="filter-menu__item">-->
<!--							<label class="filter-menu__item__inner" >-->
<!--								-->
<!--									<input type="checkbox" name="favorite" data-type="favorite" value="yes" />-->
<!--									Search Favorites-->
<!--								-->
<!--							</label>-->
<!--						</li>-->
					
					<?php } ?>
					
				</ol>
			
			</div>
			
			<div class="button-list">
				<a href="#" role="button" id="clear_filters" class="button-list__meta-button">
					<span class="button-list__meta-button__icon" role="presentation" data-icon="bars"></span>
					<span class="button-list__meta-button__content">Clear filters</span>
					<input id="post_count_field" type="hidden" name="post_count" value="8"/>
				</a>
			</div>
		
		</div>
		
		<!-- FILTER MENU -->
		
		<!-- BUTTON LIST (when filters are selected)-->
		<div class="button-list">
			<ol class="button-list__list"></ol>
		</div>
		<!-- BUTTON LIST -->
	
	</div>
</div>