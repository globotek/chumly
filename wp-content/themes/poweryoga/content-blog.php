<li class="content-tiles__item">

    <!-- TILE COMPONENT -->
    <a class="content-tile content-tile--eggshell" href="<?php the_permalink(); ?>">

        <div class="content-tile__media">
			
			<?php the_post_thumbnail( 'large', array( 'class' => 'content-tile__media__image' ) ); ?>

        </div>

        <div class="content-tile__inner">
            <div class="content-tile__content">
                <!-- ADD 'is-visible' class  to 'content-tile__heading__icon' to show 'heart' -->
                <h3 class="content-tile__heading"><?php the_title(); ?> <span class="content-tile__heading__icon"
                                                                              role="presentation"
                                                                              data-icon="heart_fill"></span></h3>
				
				<?php if( !is_single() ) { ?>

                    <div class="content-tile__summary"><?php the_excerpt(); ?></div>
				
				<?php } ?>

            </div>
			
			<?php if( !is_single() ) { ?>

                <div class="content-tile__meta">

                    <!-- CONTENT META COMPONENT -->
                    <div class="content-meta">
                        <ul class="content-meta__row">
                            <li class="content-meta__item">

                                <!-- USER AVATAR COMPONENT -->
                                <div class="user-avatar user-avatar--lap-up-left">
                                    <div class="user-avatar__image">
										<?php chumly_avatar( $post->post_author ); ?>
                                    </div>

                                    <div class="user-avatar__content">
										<?php the_author(); ?>
                                    </div>
                                </div>
                                <!-- USER AVATAR COMPONENT -->

                            </li>
                            <li class="content-meta__item">
								<?php $post_views = get_post_meta( $post->ID, 'post_views', TRUE ); ?>
								<?php echo 'Read: ' . ( $post_views > 0 ? $post_views : '0' ) . ' times'; ?>
                            </li>
                        </ul>

                    </div>
                    <!-- CONTENT META COMPONENT -->

                </div>
			
			<?php } ?>

        </div>
    </a>
    <!-- TILE COMPONENT -->

</li>
