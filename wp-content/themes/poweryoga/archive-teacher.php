<?php // Template Name: Teacher Archive ?>

<?php get_header(); ?>
<?php the_post(); ?>

<!-- HERO -->
<section class="hero">
	<div class="wrapper">
		<header class="hero__content">
			<h1 class="hero__heading"><?php the_title(); ?></h1>
			<h2 class="hero__summary"><?php echo get_the_content(); ?></h2>
		</header>
	</div>
	<aside class="hero__image" role="presentation" style="background-image: url(<?php echo wp_get_attachment_url(get_post_thumbnail_id()); ?>)"></aside>
</section>
<!-- HERO -->


<!-- PAGE CONTENT -->
<main class="generic-page">
	<div class="generic-page__primary">
		<div class="wrapper">

			<!-- LIST OF TEACHERS -->

			<div class="avatar-group">
				<ol class="avatar-group__list">

					<?php
                    $query = new WP_Query(array(
                        'post_type' 	 => 'teacher',
                        'posts_per_page' => -1, 
						'order' 		 => 'ASC',
						'meta_key' 		 => 'teacher_surname',
						'orderby' 		 => 'meta_value'
                    ));

                    while($query->have_posts()) : $query->the_post(); ?>

						<li class="avatar-group__item">
							<a href="<?php the_permalink(); ?>" class="avatar-group__inner">
								<div class="avatar-group__image" role="presentation"><?php the_post_thumbnail(); ?></div>
								<h3 class="avatar-group__heading"><?php the_title(); ?></h3>
							</a>
						</li>

					<?php endwhile; ?>

				</ol>
			</div>

			<!-- LIST OF TEACHERS -->

		</div>
	</div>
</main>
<!-- PAGE CONTENT -->

<?php get_footer(); ?>
