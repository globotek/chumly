<?php
/**
 * Created by PhpStorm.
 * User: matthew
 * Date: 21/10/18
 * Time: 4:29 PM
 */
?>

<?php do_action( 'chumly_before_content' ); ?>

<div class="navigator navigator--skinny-sidebar woocommerce">

    <div class="navigator__sidebar">
		
		<?php chumly_sidebar( 'main' ); ?>

    </div>

    <div class="navigator__content woocommerce-MyAccount-content">
        
        <h2 class="breathe--bottom">My Subscriptions</h2>
		
		<?php WC_Subscriptions::get_my_subscriptions_template(); ?>
    
    </div>

</div>

<?php do_action( 'chumly_after_content' ); ?>


