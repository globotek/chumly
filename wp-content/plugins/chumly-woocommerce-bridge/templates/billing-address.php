<?php
/**
 * Created by PhpStorm.
 * User: matthew
 * Date: 20/10/18
 * Time: 4:39 PM
 */ ?>

<?php do_action( 'chumly_before_content' ); ?>

<div class="navigator navigator--skinny-sidebar woocommerce">
	
	<div class="navigator__sidebar">
		
		<?php chumly_sidebar( 'main' ); ?>
	
	</div>
	
	<div class="navigator__content woocommerce-MyAccount-content">
		
		<div class="breathe--bottom">
			
			<?php woocommerce_account_edit_address( 'billing' ); ?>
		
		</div>
		
	</div>

</div>

<?php do_action( 'chumly_after_content' ); ?>
