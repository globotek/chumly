<?php
/**
 * Created by PhpStorm.
 * User: matthew
 * Date: 20/10/18
 * Time: 11:56 AM
 */
?>

<?php do_action( 'chumly_before_content' ); ?>

<div class="navigator navigator--skinny-sidebar woocommerce">
	
	<div class="navigator__sidebar">
		
		<?php chumly_sidebar( 'main' ); ?>
	
	</div>
	
	<div class="navigator__content">
		
		<?php wc_print_notices(); ?>
		
		<h2 class="breathe--bottom">My Addresses</h2>
		
		<?php wc_get_template('myaccount/my-address.php'); ?>
	
	</div>

</div>

<?php do_action( 'chumly_after_content' ); ?>

