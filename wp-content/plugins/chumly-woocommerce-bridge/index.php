<?php
/**
 * Plugin Name: Chumly WooCommerce Bridge
 * Author: GloboTek
 * Author URI: https://globotek.net
 * Version: 1.0.0
 * Description: Integrates WooCommerce with Chumly to create a seamless experience between the two.
 * Tags: chumly
 */
/**
 * Created by PhpStorm.
 * User: matthew
 * Date: 19/10/18
 * Time: 9:57 AM
 */

function chumly_woocommerce_install() {
	
	include_once( 'installation.php' );
	
	$chumly_woocommerce_installed = get_option( 'chumly_installed' );
	$chumly_woocommerce_version   = get_option( 'chumly_version' );
	
	if ( $chumly_woocommerce_installed != 1 ) {
		require_once( 'chumly-install.php' );
	}
	
	if ( $chumly_woocommerce_version < 0.1 ) {
		require_once( 'chumly-update.php' );
	}
	
}

register_activation_hook( __FILE__, 'chumly_woocommerce_install' );


function chumly_woocommerce_bridge() {
	
	if ( ! is_plugin_active( 'chumly/chumly.php' ) || ! is_plugin_active( 'woocommerce/woocommerce.php' )) {
		
		function chumly_woocommerce_activation_error() {
			
			$class   = 'notice notice-error';
			$message = __( 'Chumly WooCommerce Bridge requires Chumly Core & WooCommerce plugin. Plugin deactivated.', 'chumly' );
			
			printf( '<div class="%1$s"><p>%2$s</p></div>', esc_attr( $class ), esc_html( $message ) );
			
			deactivate_plugins( plugin_basename( __FILE__ ) );
			
		}
		
		add_action( 'admin_notices', 'chumly_woocommerce_activation_error' );
		
	} else {
		
		function chumly_woocommerce_sidebar_menu( $menus ) {
			
			$shopping_menu = array();
			
			$shopping_menu[ 15 ] = array(
				'section_title' => 'Shopping',
				'nav_items'     => array(
					array(
						'icon'  => 'paper',
						'title' => 'My Orders',
						'url'   => wc_get_page_permalink( 'myaccount' ) . 'orders'
					),
					array(
						'icon'  => 'box',
						'title' => 'My Addresses',
						'url'   => wc_get_page_permalink( 'myaccount' ) . 'edit-address'
					)
				)
			
			);
			
			if ( class_exists( 'WC_Subscriptions' ) ) {
				
				$shopping_menu[ 15 ][ 'nav_items' ][] = array(
					'icon'  => 'reload',
					'title' => 'My Subscriptions',
					'url'   => wc_get_page_permalink( 'myaccount' ) . 'subscriptions'
				);
				
			}
			
			ksort( $shopping_menu );
			
			$menus[ 'shopping' ] = $shopping_menu;
			
			return $menus;
			
		}
		
		add_filter( 'chumly_dashboard_navigation', 'chumly_woocommerce_sidebar_menu', 15 );
		
		
		function chumly_woocommerce_template_directory( $chumly_template_directories ) {
			
			//global $chumly_template_directories;
			
			$chumly_template_directories[] = plugin_dir_path( __FILE__ ) . 'templates';
			
			return $chumly_template_directories;
			
		}
		
		add_filter( 'chumly_template_directories_setup', 'chumly_woocommerce_template_directory' );
		
		
		function chumly_woocommerce_rewrites() {
			
			global $wp_query;
			
			$account_pagename = get_post( get_option( 'woocommerce_myaccount_page_id' ) )->post_name;
			$account_url      = ltrim( parse_url( wc_get_page_permalink( 'myaccount' ) )[ 'path' ], '/' );
			$address_url      = ltrim( parse_url( wc_get_page_permalink( 'myaccount' ) . 'edit-address' )[ 'path' ], '/' );
			
			add_rewrite_rule( $address_url . '/billing', 'index.php?chumly_template=edit_billing&pagename=' . $account_pagename . '&edit-address=billing', 'top' );
			add_rewrite_rule( $address_url . '/shipping', 'index.php?chumly_template=edit_shipping&pagename=' . $account_pagename . '&edit-address=shipping', 'top' );
			add_rewrite_rule( $address_url, 'index.php?chumly_template=addresses&pagename=' . $account_pagename, 'top' );
			
			if ( class_exists( 'WC_Subscriptions' ) ) {
				add_rewrite_rule( $account_url . 'view-subscription/(\d*)', 'index.php?chumly_template=view-subscription&pagename=' . $account_pagename . '&view-subscription=$matches[1]', 'top' );
				add_rewrite_rule( $account_url . 'subscriptions', 'index.php?chumly_template=subscriptions&pagename=' . $account_pagename, 'top' );
			}
			
			add_rewrite_rule( $account_url . 'view-order/(\d*)', 'index.php?chumly_template=view-order&pagename=' . $account_pagename . '&view-order=$matches[1]', 'top' );
			add_rewrite_rule( $account_url . 'orders', 'index.php?chumly_template=orders&pagename=' . $account_pagename, 'top' );
			
			add_rewrite_rule( trim( parse_url( wc_get_page_permalink( 'myaccount' ) )[ 'path' ], '/' ), 'index.php?chumly_template=orders&pagename=' . $account_pagename, 'top' );
			
		}
		
		add_action( 'init', 'chumly_woocommerce_rewrites' );
		
		
		function chumly_woocommerce_load_plugin_templates( $template ) {
			
			switch ( get_query_var( 'chumly_template' ) ) {
				
				case 'view-order':
					
					$template = chumly_get_page_template();
					chumly_inject_template( 'view-order' );
					
					break;
				
				case 'orders':
					
					$template = chumly_get_page_template();
					chumly_inject_template( 'orders' );
					
					break;
				
				case 'addresses':
					
					$template = chumly_get_page_template();
					chumly_inject_template( 'edit-address' );
					
					break;
				
				case 'edit_billing':
					
					$template = chumly_get_page_template();
					chumly_inject_template( 'billing-address' );
					
					break;
				
				case 'edit_shipping':
					
					$template = chumly_get_page_template();
					chumly_inject_template( 'shipping-address' );
					
					break;
				
				case 'subscriptions':
					
					$template = chumly_get_page_template();
					chumly_inject_template( 'subscriptions' );
					
					break;
				
				case 'view-subscription':
					
					$template = chumly_get_page_template();
					chumly_inject_template( 'view-subscription' );
					
					break;
			}
			
			return $template;
			
		}
		
		add_action( 'template_include', 'chumly_woocommerce_load_plugin_templates' );
		
		
		function chumly_woocommerce_update_new_user( $user_id ) {
			
			update_user_meta( $user_id, '_chumly_user_role', 'default' );
			
			chumly_create_media_bucket( $user_id );
			
			$user = get_userdata( $user_id );
			
			$profile_data = array();
			
			$profile_data = chumly_update_field( $user_id, 'required_2', $user->user_email, $profile_data );
			$profile_data = chumly_update_field( $user_id, 'required_3', $_POST[ 'billing_first_name' ], $profile_data );
			$profile_data = chumly_update_field( $user_id, 'required_4', $_POST[ 'billing_last_name' ], $profile_data );
			
			update_user_meta( $user_id, 'profile_fields', chumly_serialize( $profile_data ) );
			
			wp_update_user( array(
				'ID'           => $user_id,
				'display_name' => ucfirst( $_POST[ 'billing_first_name' ] ) . ' ' . ucfirst( $_POST[ 'billing_last_name' ] )
			) );
			
		}
		
		add_action( 'woocommerce_created_customer', 'chumly_woocommerce_update_new_user' );
		
	}
	
}

add_action( 'plugins_loaded', 'chumly_woocommerce_bridge', 15 );
