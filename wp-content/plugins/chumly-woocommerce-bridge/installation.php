<?php
/**
 * Created by PhpStorm.
 * User: matthew
 * Date: 20/10/18
 * Time: 3:41 PM
 */

/*global $wpdb;

$wpdb->query( 'INSERT INTO ' . $inputs_table . '
	(ID, input_id, input_order, input_name, input_label, input_type, input_required, input_instructions, input_data, input_location, input_group, input_permanent, input_active, input_placement, user_type)
	VALUES
	(NULL, "woocommerce_1", 1, "billing_first_name", "First Name", "text", 1, NULL, "a:0:{}", "woocommerce", "default", 1, 1, "grid__item--palm-one-whole grid__item--lap--one-whole grid__item--desk--one-half", "user"),
	(NULL, "woocommerce_2", 1, "billing_last_name", "Last Name", "text", 1, NULL, "a:0:{}", "woocommerce", "default", 1, 1, "grid__item--palm-one-whole grid__item--lap--one-whole grid__item--desk--one-half", "user"),
	(NULL, "woocommerce_3", 1, "billing_company", "Company name (optional)", "text", 0, NULL, "a:0:{}", "woocommerce", "default", 1, 1, "grid__item--palm-one-whole grid__item--lap--one-whole grid__item--desk--one-whole", "user"),
	(NULL, "woocommerce_4", 1, "billing_address_1", "Street address", "text", 1, NULL, "a:0:{}", "woocommerce", "default", 1, 1, "grid__item--palm-one-whole grid__item--lap--one-whole grid__item--desk--one-whole", "user"),
	(NULL, "woocommerce_5", 1, "billing_address_2", "", "text", 0, NULL, "a:0:{}", "woocommerce", "default", 1, 1, "grid__item--palm-one-whole grid__item--lap--one-whole grid__item--desk--one-whole", "user"),
	(NULL, "woocommerce_6", 1, "billing_city", "Town/City", "text", 1, NULL, "a:0:{}", "woocommerce", "default", 1, 1, "grid__item--palm-one-whole grid__item--lap--one-whole grid__item--desk--one-whole", "user"),
	(NULL, "woocommerce_7", 1, "billing_state", "State/County (optional)", "text", 0, NULL, "a:0:{}", "woocommerce", "default", 1, 1, "grid__item--palm-one-whole grid__item--lap--one-whole grid__item--desk--one-whole", "user")
');*/

update_option( 'chumly_woocommerce_installed', TRUE );
update_option( 'chumly_woocommerce_version', 1.0 );