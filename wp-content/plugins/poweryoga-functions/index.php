<?php
/**
 * Plugin Name: Power Yoga Functionality
 * Author: GloboTek
 * Version: 1.2
 * Description: This plugin is built to provide the Power Yoga site with the theme independent code required for the likes of custom post types & taxonomies.
 * Author URI: http://globotek.net
**/

function require_function_files(){
    foreach(glob(dirname(__FILE__) . '/includes/*.php') as $file){
		include_once($file);
	}
}

add_action('plugins_loaded', 'require_function_files');
