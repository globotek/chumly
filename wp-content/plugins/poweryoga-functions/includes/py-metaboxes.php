<?php
/** Generate Metaboxes **/
function create_classes_metabox() {
	add_meta_box( 'class_information', __( 'Class Information' ), 'output_classes_metabox', 'class', $context = 'advanced', $priority = 'default', $callback_args = NULL );
}

//add_action('add_meta_boxes', 'create_classes_metabox');


function output_classes_metabox( $post ) {
	wp_enqueue_style( 'meta-box', plugin_dir_url( __DIR__ ) . '/css/meta-box.css' );
	wp_nonce_field( 'save_classes_metabox', 'class_metabox_nonce' );
	
	$class_level   = get_post_meta( $post->ID, 'class_level', TRUE );
	$video_id      = get_post_meta( $post->ID, 'video_id', TRUE );
	$video_runtime = get_post_meta( $post->ID, 'video_runtime', TRUE );
	
	echo '<div class="wrap">';
	
	echo '<div id="col-container">';
	
	echo '<div id="col-right">';
	
	echo '<div class="col-wrap">';
	
	echo '<label for="class_level">Class Difficulty Level&nbsp;</label>';
	echo '<input type="text" id="class_level" name="class_level" value="' . esc_attr( $class_level ) . '" size="25"/>';
	
	echo '</div>';
	
	echo '</div>';
	
	echo '<div id="col-left">';
	
	echo '<div class="col-wrap">';
	
	echo '<label for="video_id">Viddler Video ID&nbsp;</label>';
	echo '<input type="text" id="video_id" name="video_id" value="' . esc_attr( $video_id ) . '" size="25"/>';
	
	echo '</div>';
	
	echo '<br>';
	
	echo '<div class="col-wrap">';
	
	echo '<label for="video_runtime">Video Runtime&nbsp;</label>';
	echo '<input type="text" id="video_runtime" name="video_runtime" value="' . esc_attr( $video_runtime ) . '" size="25"/>';
	
	echo '</div>';
	
	echo '</div>';
	
	echo '</div>';
	
	echo '</div>';
}

function save_classes_metabox( $post_id ) {
	if ( ! isset( $_POST['class_metabox_nonce'] ) ) {
		return;
	}
	
	if ( ! wp_verify_nonce( $_POST['class_metabox_nonce'], 'save_classes_metabox' ) ) {
		return;
	}
	
	if ( defined( 'DOING_AUTOSAVE' ) && DOING_AUTOSAVE ) {
		return;
	}
	
	if ( isset( $_POST['class_level'] ) ) {
		update_post_meta( $post_id, 'class_level', sanitize_text_field( $_POST['class_level'] ) );
	}
	
	if ( isset( $_POST['video_id'] ) ) {
		update_post_meta( $post_id, 'video_id', sanitize_text_field( $_POST['video_id'] ) );
	}
	
	if ( isset( $_POST['video_runtime'] ) ) {
		update_post_meta( $post_id, 'video_runtime', sanitize_text_field( $_POST['video_runtime'] ) );
	}
}

//add_action('save_post', 'save_class_metabox');