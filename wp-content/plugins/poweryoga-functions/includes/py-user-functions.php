<?php
function create_user_roles(){
    add_role('teacher', __('Teacher'), array(
        'read'         => true,
        'edit_posts'   => true,
        'delete_posts' => true
    ));
}

register_activation_hook(__FILE__, 'create_user_roles');
