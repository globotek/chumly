<?php
function create_custom_post_types(){
	$labels = array(
		'name' 			 => _x('Classes', 'post type general name', 'power-yoga'),
		'singular_name'  => _x('Class', 'post type singular name', 'power-yoga'),
		'edit_item'      => __('Edit Class')
	);

	$args = array(
		'labels'			 => $labels,
		'public'			 => true,
		'publicly_queryable' => true,
		'show_ui' 			 => true,
		'query_var' 		 => true,
		'rewrite' 			 => array('slug', 'with_front' => false),
		'capability_type' 	 => 'post',
		'has_archive' 		 => true,
		'hierarchical' 		 => false,
		'supports' 			 => array('title', 'editor', 'excerpt', 'thumbnail', 'author'),
		'taxonomies' 		 => array('type')
	);

	//register_post_type('class', $args);


	$labels = array(
		'name' 			 => _x('Teachers', 'post type general name', 'power-yoga'),
		'singular_name'  => _x('Teacher', 'post type singular name', 'power-yoga'),
	);

	$args = array(
		'labels'			 => $labels,
		'public'			 => true,
		'publicly_queryable' => true,
		'show_ui' 			 => true,
		'query_var' 		 => true,
		'rewrite' 			 => array('slug', 'with_front' => false),
		'capability_type' 	 => 'post',
		'has_archive' 		 => true,
		'hierarchical' 		 => false,
		'supports' 			 => array('title', 'editor', 'thumbnail', 'excerpt', 'author', 'custom-fields'),
	);

	register_post_type('teacher', $args);


	$labels = array(
		'name' 			 => _x('Teacher Training', 'post type general name', 'power-yoga'),
		'singular_name'  => _x('Teacher Training', 'post type singular name', 'power-yoga'),
	);

	$args = array(
		'labels'			 => $labels,
		'public'			 => true,
		'publicly_queryable' => true,
		'show_ui' 			 => true,
		'query_var' 		 => true,
		'rewrite' 			 => array('slug', 'with_front' => false),
		'capability_type' 	 => 'post',
		'has_archive' 		 => true,
		'hierarchical' 		 => false,
		'supports' 			 => array('title', 'editor', 'thumbnail', 'excerpt', 'author', 'custom-fields'),
	);

	register_post_type('teacher_training', $args);


	$labels = array(
		'name' 			 => _x('Retreats', 'post type general name', 'power-yoga'),
		'singular_name'  => _x('Retreat', 'post type singular name', 'power-yoga'),
	);

	$args = array(
		'labels'			 => $labels,
		'public'			 => true,
		'publicly_queryable' => true,
		'show_ui' 			 => true,
		'query_var' 		 => true,
		'rewrite' 			 => array('slug', 'with_front' => false),
		'capability_type' 	 => 'post',
		'has_archive' 		 => true,
		'hierarchical' 		 => false,
		'supports' 			 => array('title', 'editor', 'thumbnail', 'excerpt'),
		'taxonomies' 		 => array('location')
	);

	register_post_type('retreat', $args);


	$labels = array(
		'name' 			 => _x('Testimonials', 'post type general name', 'power-yoga'),
		'singular_name'  => _x('Testimonial', 'post type singular name', 'power-yoga'),
	);

	$args = array(
		'labels' 			 => $labels,
		'public'			 => true,
		'publicly_queryable' => true,
		'show_ui' 			 => true,
		'query_var' 		 => true,
		'rewrite' 			 => array('slug', 'with_front' => false),
		'capability_type' 	 => 'post',
		'has_archive' 		 => false,
		'hierarchical' 		 => false,
		'supports' 			 => array('title', 'editor', 'thumbnail', 'excerpt', 'author'),
	);

	register_post_type('testimonial', $args);
}

add_action('init', 'create_custom_post_types');


function create_custom_taxonomies(){
	$labels = array(
		'name'		 	=> _x('Categories', 'taxonomy general name'),
		'singular_name' => _x('Category', 'taxonomy singular name'),
		'edit_item' 	=> __('Edit Category'),
		'view_item' 	=> __('View Category'),
		'update_item' 	=> __('Update Category'),
		'add_new_item' 	=> __('Add New Category'),
		'new_item_name' => __('New Category Name'),
		'search_items' 	=> __('Search Categories'),
		'popular_items' => __('Most Used')
	);
	
	$args = array(
		'hierarchical'   => true,
		'labels' 		 => $labels,
		'show_ui' 		 => true,
		'show_admin_col' => true,
		'query_var' 	 => true,
		'rewrite' 		 => array('slug' => 'yoga-videos', 'with_front' => false),
	);
	
	register_taxonomy('class_category', array('class'), $args);
	
	
	$labels = array(
		'name'		 	=> _x('Categories', 'taxonomy general name'),
		'singular_name' => _x('Category', 'taxonomy singular name'),
		'edit_item' 	=> __('Edit Category'),
		'view_item' 	=> __('View Category'),
		'update_item' 	=> __('Update Category'),
		'add_new_item' 	=> __('Add New Category'),
		'new_item_name' => __('New Category Name'),
		'search_items' 	=> __('Search Categories'),
		'popular_items' => __('Most Used')
	);
	
	$args = array(
		'hierarchical'   => true,
		'labels' 		 => $labels,
		'show_ui' 		 => true,
		'show_admin_col' => true,
		'query_var' 	 => true,
		'rewrite' 		 => array('with_front' => false),
	);
	
	register_taxonomy('teacher_category', array('teacher'), $args);
	
	
	$labels = array(
		'name'		 	=> _x('Filters', 'taxonomy general name'),
		'singular_name' => _x('Filter', 'taxonomy singular name'),
		'edit_item' 	=> __('Edit Type'),
		'view_item' 	=> __('View Type'),
		'update_item' 	=> __('Update Type'),
		'add_new_item' 	=> __('Add New Type'),
		'new_item_name' => __('New Type Name'),
		'search_items' 	=> __('Search Types'),
		'popular_items' => __('Most Used')
	);
	
	$args = array(
		'hierarchical'   => true,
		'labels' 		 => $labels,
		'show_ui' 		 => true,
		'show_admin_col' => true,
		'query_var' 	 => true,
		'rewrite' 		 => array('slug'),
	);
	
	register_taxonomy('class_filter', array('class', 'chumly_lesson'), $args);


	$labels = array(
		'name'		 	=> _x('Locations', 'taxonomy general name'),
		'singular_name' => _x('Location', 'taxonomy singular name'),
		'edit_item' 	=> __('Edit Location'),
		'view_item' 	=> __('View Location'),
		'update_item' 	=> __('Update Location'),
		'add_new_item' 	=> __('Add New Location'),
		'new_item_name' => __('New Location Name'),
		'search_items' 	=> __('Search Locations'),
		'popular_items' => __('Most Used')
	);

	$args = array(
		'hierarchical'   => true,
		'labels' 		 => $labels,
		'show_ui' 		 => true,
		'show_admin_col' => true,
		'query_var' 	 => true,
		'rewrite' 		 => array('slug'),
	);
	
	//register_taxonomy('location', array('retreat'), $args);


	$labels = array(
		'name'		 	=> _x('Teachers', 'taxonomy general name'),
		'singular_name' => _x('Teacher', 'taxonomy singular name'),
		'edit_item' 	=> __('Edit Teacher'),
		'view_item' 	=> __('View Teacher'),
		'update_item' 	=> __('Update Teacher'),
		'add_new_item' 	=> __('Add New Teacher'),
		'new_item_name' => __('New Teacher Name'),
		'search_items' 	=> __('Search Teachers'),
		'popular_items' => __('Most Used')
	);

	$args = array(
		'hierarchical'   => true,
		'labels' 		 => $labels,
		'show_ui' 		 => true,
		'show_admin_col' => true,
		'query_var' 	 => true,
		'rewrite' 		 => array('slug'),
	);

	//register_taxonomy('teacher', array('teacher'), $args);
}

add_action('init', 'create_custom_taxonomies');
