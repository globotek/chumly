<?php
/*
* Plugin Name: Chumly eLearning
* Author: GloboTek
* Author URI: https://globotek.net
* Version: 0.1
*/

function chumly_elearning() {
	
	include_once( ABSPATH . 'wp-admin/includes/plugin.php' );
	
	if ( ! is_plugin_active( 'chumly/chumly.php' ) ) {
		
		function chumly_elearning_activation_error() {
			
			$class   = 'notice notice-error';
			$message = __( 'Chumly eLearning requires Chumly Core plugin. Plugin deactivated.', 'chumly' );
			
			printf( '<div class="%1$s"><p>%2$s</p></div>', esc_attr( $class ), esc_html( $message ) );
			
			deactivate_plugins( plugin_basename( __FILE__ ) );
			
		}
		
		add_action( 'admin_notices', 'chumly_elearning_activation_error' );
		
	} else {
		
		/**
		 * Setup CPT & Taxonomy.
		 */
		function chumly_elearning_post_setup() {
			$labels = array(
				'name'         => __( 'All Lessons' ),
				'single'       => __( 'Lesson' ),
				'add_new_item' => __( 'Add New Lesson', 'chumly_lesson', 'chumly' ),
				'edit_item'    => __( 'Edit Lesson' ),
				'menu_name'    => __( 'Creative Learning' )
			);
			
			$args = array(
				'labels'                    => $labels,
				'public'                    => TRUE,
				'publicly_queryable'        => TRUE,
				'show_ui'                   => TRUE,
				'show_in_menu'              => TRUE,
				'query_var'                 => TRUE,
				'rewrite'                   => array( 'slug' => 'lesson', 'with_front' => FALSE ),
				'capability_type'           => 'post',
				'menu_position'             => 24,
				'menu_icon'                 => 'dashicons-book-alt',
				'has_archive'               => TRUE,
				'hierarchical'              => TRUE,
				'supports'                  => array( 'title', 'editor', 'thumbnail', 'excerpt', 'page-attributes', 'author' ),
				'taxonomies'                => array( 'chumly_course' ),
				'register_metabox_callback' => 'chumly_elearning_add_meta_boxes'
			);
			
			register_post_type( 'chumly_lesson', $args );
			
			
			$labels = array(
				'name'          => _x( 'Search Filters', 'taxonomy general name' ),
				'singular_name' => _x( 'Search Filter', 'taxonomy singular name' ),
				'edit_item'     => __( 'Edit Filter' ),
				'view_item'     => __( 'View Filter' ),
				'update_item'   => __( 'Update Filter' ),
				'add_new_item'  => __( 'Add New Filter' ),
				'new_item_name' => __( 'New Filter Name' ),
				'search_items'  => __( 'Search Filters' ),
				'popular_items' => __( 'Most Used' )
			);
			
			$args = array(
				'hierarchical'   => TRUE,
				'labels'         => $labels,
				'show_ui'        => TRUE,
				'show_admin_col' => TRUE,
				'query_var'      => TRUE,
				'rewrite'        => array( 'slug' ),
			);
			
			register_taxonomy( 'chumly_lesson_filter', array( 'chumly_lesson' ), $args );
			
			
			$labels = array(
				'name'          => _x( 'Modules', 'taxonomy general name' ),
				'singular_name' => _x( 'Module', 'taxonomy singular name' ),
				'edit_item'     => __( 'Edit Module' ),
				'view_item'     => __( 'View Module' ),
				'update_item'   => __( 'Update Module' ),
				'add_new_item'  => __( 'Add New Module' ),
				'new_item_name' => __( 'New Module Name' ),
				'parent_item'   => __( 'Program' ),
				'search_items'  => __( 'Search Modules' ),
				'popular_items' => __( 'Most Used' ),
				'back_to_items' => __( 'Back to Modules' )
			);
			
			$args = array(
				'hierarchical'       => TRUE,
				'labels'             => $labels,
				'show_in_nav_menus'  => TRUE,
				'show_ui'            => TRUE,
				'show_in_quick_edit' => TRUE,
				'meta_box_cb'        => FALSE,
				'show_admin_col'     => TRUE,
				'query_var'          => TRUE,
				'rewrite'            => array( 'with_front' => FALSE, 'slug' => 'module', 'hierarchical' => TRUE )
			);
			
			register_taxonomy( 'chumly_module', array( 'chumly_lesson' ), $args );
			
			
			$labels = array(
				'name'          => _x( 'Programs', 'taxonomy general name' ),
				'singular_name' => _x( 'Program', 'taxonomy singular name' ),
				'edit_item'     => __( 'Edit Program' ),
				'view_item'     => __( 'View Program' ),
				'update_item'   => __( 'Update Program' ),
				'add_new_item'  => __( 'Add New Program' ),
				'new_item_name' => __( 'New Program' ),
				'search_items'  => __( 'Search Programs' ),
				'popular_items' => __( 'Most Used' ),
				'back_to_items' => __( 'Back to Programs' )
			);
			
			$args = array(
				'hierarchical'       => TRUE,
				'labels'             => $labels,
				'show_in_nav_menus'  => TRUE,
				'show_ui'            => TRUE,
				'show_in_quick_edit' => TRUE,
				'meta_box_cb'        => FALSE,
				'show_admin_col'     => TRUE,
				'query_var'          => TRUE,
				'rewrite'            => array( 'with_front' => FALSE, 'slug' => 'program', 'hierarchical' => TRUE )
			);
			
			register_taxonomy( 'chumly_program', array( 'chumly_lesson' ), $args );
			
		}
		
		add_action( 'init', 'chumly_elearning_post_setup' );
		
		
		/**
		 * Create Admin Menu.
		 */
		function chumly_elearning_menu() {
			
			global $submenu;
			
			if ( isset( $submenu[ 'edit.php?post_type=chumly_lesson' ] ) ) {
				$submenu[ 'edit.php?post_type=chumly_lesson' ][ 5 ][ 0 ] = __( 'All Lessons', 'chumly-elearning' );
			}
			
		}
		
		add_action( 'admin_menu', 'chumly_elearning_menu' );

//		/**
//		 * Chumly eLearning rewrite rules
//		 */
//		function chumly_elearning_rewrite_rules() {
//
//			add_rewrite_rule( 'videos', 'index.php?chumly_template=chumly-elearning-lessons', 'top' );
//
//			//add_rewrite_rule( 'lesson/([\s\S]*)', 'index.php?chumly_template=chumly-elearning-lesson', 'top' );
//
//		}
//
//		add_action( 'init', 'chumly_elearning_rewrite_rules' );
		
		
		function chumly_elearning_template_directory( $chumly_template_directories ) {
			
			$chumly_template_directories[] = plugin_dir_path( __FILE__ ) . 'frontend/templates';
			
			return $chumly_template_directories;
			
		}
		
		add_filter( 'chumly_template_directories_setup', 'chumly_elearning_template_directory' );
		
		
		function chumly_elearning_plugin_templates( $template ) {
			
			
			if ( is_tax( 'chumly_module' ) ) {
				
				set_query_var( 'chumly_page', TRUE );
				
				$template = chumly_get_page_template();
				chumly_inject_template( 'taxonomy-chumly_module' );
				
			}
			
			if ( is_tax( 'chumly_program' ) ) {
				
				set_query_var( 'chumly_page', TRUE );
				
				$template = chumly_get_page_template();
				chumly_inject_template( 'taxonomy-chumly_program' );
				
			}
			
			if ( get_query_var( 'chumly_template' ) == 'chumly-elearning-lessons' ) {
				
				set_query_var( 'chumly_page', TRUE );
				
				$template = chumly_locate_template( NULL, 'archive-chumly_lesson' );
				
			}
			
			if ( is_singular( 'chumly_lesson' ) ) {
				
				set_query_var( 'chumly_page', TRUE );
				
				$template = chumly_locate_template( NULL, 'single-chumly_lesson' );
				
			}
			
			return $template;
			
		}
		
		add_action( 'template_include', 'chumly_elearning_plugin_templates' );
		
		
		/**
		 * Include required files
		 */
		function chumly_elearning_includes() {
			
			foreach ( glob( dirname( __FILE__ ) . '/frontend/includes/*.php' ) as $file ) {
				include_once( $file );
			}
			
			foreach ( glob( dirname( __FILE__ ) . '/admin/metaboxes/*.php' ) as $file ) {
				include_once( $file );
			}
			
		}
		
		add_action( 'plugins_loaded', 'chumly_elearning_includes', 20 );
		
		
		/**
		 * Create Chumly dashboard navigation items.
		 */
		function chumly_elearning_newsfeed_nav( $menus ) {
			
			$elearning_menu = array();
			
			$elearning_menu[ 15 ] =
				array(
					'section_title' => 'Learn',
					'nav_items'     => array(
						array(
							'icon'  => 'play',
							'title' => 'Playlist',
							'url'   => '/playlist'
						)
					)
				);
			
			ksort( $elearning_menu );
			
			$menus[ 'elearning' ] = $elearning_menu;
			
			return $menus;
			
		}
		
		add_filter( 'chumly_dashboard_navigation', 'chumly_elearning_newsfeed_nav', 25 );
		
		
		/**
		 * Enqueue & include files.
		 */
		function chumly_elearning_admin_styles() {
			wp_enqueue_style( 'elearning-admin-css', plugin_dir_url( __FILE__ ) . 'admin/css/admin.css' );
		}
		
		add_action( 'admin_enqueue_scripts', 'chumly_elearning_admin_styles' );
		
		function chumly_elearning_scripts() {
			wp_enqueue_script( 'jquery-ui-sortable' );
			wp_enqueue_script( 'metabox-js', plugin_dir_url( __FILE__ ) . 'admin/js/meta.js', array( 'jquery-ui-sortable' ) );
			wp_enqueue_script( 'media-uploader-js', plugin_dir_url( __FILE__ ) . 'admin/js/media-uploader.js' );
			wp_enqueue_script( 'chumly-elearning', plugin_dir_url( __FILE__ ) . 'frontend/scripts/chumly-elearning.js', array( 'jquery' ) );
		}
		
		add_action( 'admin_enqueue_scripts', 'chumly_elearning_scripts' );
		
		function chumly_elearning_frontend_styles() {
			wp_enqueue_style( 'font-awesome', '//maxcdn.bootstrapcdn.com/font-awesome/4.3.0/css/font-awesome.min.css' );
			wp_enqueue_style( 'chumly-elearning', plugin_dir_url( __FILE__ ) . 'frontend/css/chumly-elearning.css' );
		}
		
		add_action( 'wp_enqueue_scripts', 'chumly_elearning_frontend_styles' );
		
		function chumly_elearning_frontend_scripts() {
			wp_enqueue_script( 'chumly-elearning', plugin_dir_url( __FILE__ ) . 'frontend/scripts/chumly-elearning.js', array( 'jquery' ) );
		}
		
		add_action( 'wp_enqueue_scripts', 'chumly_elearning_frontend_scripts' );
		
	}
	
}


add_action( 'plugins_loaded', 'chumly_elearning', 15 );