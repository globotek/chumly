<?php $subject_media = get_term_meta( $term->term_id, 'course_cover_media', TRUE ); ?>

<div class="chunk">
	
	<!-- COURSE GRID LAYOUT -->
	<div class="course-grid">
		<h2 class="course-grid__heading">
			
			<a class="button button--primary" href="/all" alt="All Courses">All Courses available</a>
			
			<?php foreach ( $courses as $course ) { ?>
				
				<?php $course_ids[] = intval( $course[ 'ID' ] ); ?>
				<a class="button button--primary" href="<?php echo get_term_link( intval( $course[ 'ID' ] ), 'chumly_course' ); ?>" alt="<?php echo $course[ 'name' ] . ' Courses'; ?>"><?php echo $course[ 'name' ]; ?></a>
			
			<?php } ?>
		
		</h2>
		
		<ol class="course-grid__list">
			
			<?php
			/** Get the child taxonomies (courses) of the current term (curriculum). */
			$terms = get_terms( array(
				'taxonomy'   => 'chumly_course',
				'include'    => $course_ids,
				'hide_empty' => FALSE,
				'orderby'    => 'include'
			) );
			
			
			//var_dump( $terms );
			/**
			 * Loop over each of the child taxonomies,
			 * getting the pieces of data we need to build the courses grid list.
			 */
			foreach ( $terms as $term ) {
				$term_id          = $term->term_id;
				$term_name        = $term->name;
				$term_description = get_term_meta( $term->term_id, 'short_description', TRUE );
				$term_link        = get_term_link( $term );
				$parent_term      = get_term( $term->parent, 'chumly_course' );
				
				$bg_color = get_term_meta( $parent_term->term_id, 'course_color', TRUE );
				
				if ( get_term_meta( $parent_term->term_id, 'course_color_transparent', TRUE ) ) {
					
					$bg_color = 'transparent';
					
				} ?>
				
				<li class="course-grid__item" data-module="course-actions" data-course_id="<?php echo $term_id; ?>">
					
					<!-- COURSE CARD COMPONENT -->
					
					<div class="course-card is-course">
						<div class="course-card__header" style="background-color:<?php echo $bg_color; ?>">
							<div class="course-card__header__inner">
								
								<h3 class="course-card__header__heading">
									<i class="fa fa-flask"></i>
									<?php echo $parent_term->name; ?>
								</h3>
								
								<span class="course-card__header__meta">
									<i class="fa fa-th-large"></i>
									Course
								</span>
							
							</div>
						</div>
						<div class="course-card__content">
							<h3 class="course-card__content__heading">
								<?php echo $term_name; ?>
							</h3>
							
							<p class="course-card__content__summary"><?php echo nl2br( $term_description ); ?></p>
							
							<!--<div class="course-card__content__meta">
								
								<nav class="pills">
									<div class="pills__icon">
										<i class="fa fa-tag"></i>
									</div>
									<div class="pills__list">
										<a href="#" class="pill">
											Default
										</a>
										<a href="#" class="pill pill--primary">
											Primary
										</a>
										<a href="#" class="pill pill--success">
											Success
										</a>
									</div>
								</nav>
									
							</div>-->
							
							<div class="course-card__content__button-group">
								
								<a class="button button--positive" href="<?php echo $term_link; ?>">Start Course</a>
								
								<?php
								$saved_courses = get_user_meta( get_current_user_id(), '_saved_courses', TRUE );
								
								if ( empty( $saved_courses ) || ! in_array( $term_id, $saved_courses ) ) { ?>
									
									<a href="#"
									   class="button button--primary chumly-action__trigger"
									   data-module_action="update_course_playlist"
									   data-update_action="save">Add to playlist</a>
								
								<?php } else { ?>
									
									<a href="#"
									   class="button button--primary chumly-action__trigger"
									   data-module_action="update_course_playlist"
									   data-update_action="delete">Remove from playlist</a>
								
								<?php } ?>
							
							</div>
						</div>
					</div>
					
					<!-- COURSE CARD COMPONENT -->
				
				</li>
			
			<?php } ?>
		
		</ol>
	</div>
	
	<!-- COURSE GRID LAYOUT -->

</div>