<div class="wrapper">
	
	<!-- HEADLINE COMPONENT -->

	<header class="headline headline--jumbo">
		<div class="headline__meta">
			<!--<div class="headline__meta__icon-group">
				<i class="fa fa-check-square-o"></i>
				<span>5 Minute watch</span>
			</div>-->
		</div>
		<h1 class="headline__heading">
			<?php the_title(); ?>
		</h1>
		<?php 
		$post_parent_id = wp_get_post_parent_id(get_the_ID());
		$terms = get_the_terms($post_parent_id, 'course'); 
		 
		foreach($terms as $term){
			if($term->parent != 0){	
				$term_link = get_term_link($term->term_id, 'course');
				$term_name = $term->name;
			}
		}
		?>
		<a href="<?php echo $term_link; ?>">Go back to <?php echo $term_name; ?> course index</a>
	</header>

	<!-- HEADLINE COMPONENT -->

	<section class="view">
		<div class="view__inner">
			<div class="view__content">

				<!-- VIDEO PLAYER WRAPPER COMPONENT -->

				<div class="video-player">
					<?php the_content(); ?>
					<img src="<?php //echo theme_root() . 'images/temp/video-player-placeholder.png'; ?>" />
				</div>

				<script>
				jQuery(document).ready(function($){
					$('video').gtekVid();
				});
				</script>

				<!-- VIDEO PLAYER WRAPPER COMPONENT -->

				<!-- TABS COMPONENT -->
				<?php $summary_content = get_post_meta(get_the_ID(), 'video_summary', true); ?>
				<?php $transcript_content = get_post_meta(get_the_ID(), 'video_transcript', true); ?>
				<?php $extras_content = get_post_meta(get_the_ID(), 'video_extras', true); ?>
				
				<div class="tabs" data-module="tabs">
					<nav class="tabs__nav">
						<ul class="tabs__list">
							<?php if(!empty($summary_content)){ ?>
								<li>
									<a href="#" class="chumly-tabs__trigger is-active" data-target="summary">
										Summary 
									</a>
								</li>
							<?php } ?>
							
							<?php if(!empty($transcript_content)){ ?>	
								<li>
									<a href="#" class="chumly-tabs__trigger" data-target="transcript">
										Transcript 
									</a>
								</li>
							<?php } ?>
							
							<?php if(!empty($extras_content)){ ?>	
								<li>
									<a href="#" class="chumly-tabs__trigger" data-target="extras">
										Extras 
									</a>
								</li>
							<?php } ?>	
						</ul>
					</nav>
					<ol class="tabs__items">
						<?php if(!empty($summary_content)){ ?>
							<li class="tabs__item">
								<article class="tabs__item__inner">
									<h2 class="tabs__item__heading chumly-tabs__trigger is-active" data-target="summary">
										Summary
									</h2>
									<div class="tabs__item__content content chumly-tabs__target is-active" id="summary">
										<div class="content__body">
											<?php echo $summary_content; ?>										
										</div>
									</div>
								</article>
							</li>
						<?php } ?>
						
						<?php if(!empty($transcript_content)){ ?>	
							<li class="tabs__item">
								<article class="tabs__item__inner">
									<h2 class="tabs__item__heading chumly-tabs__trigger" data-target="transcript">
										Transcript
									</h2>
									<div class="tabs__item__content chumly-tabs__target content" id="transcript">
										<div class="content__body">
											<?php echo $transcript_content; ?>										
										</div>
									</div>
								</article>
							</li>
						<?php } ?>
						
						<?php if(!empty($extras_content)){ ?>	
							<li class="tabs__item">
								<article class="tabs__item__inner">
									<h2 class="tabs__item__heading chumly-tabs__trigger" data-target="extras">
										Extras
									</h2>
									<div class="tabs__item__content chumly-tabs__target content" id="extras">
										<div class="content__body">
											<?php echo $extras_content; ?>										
										</div>
									</div>
								</article>
							</li>
						<?php } ?>
					</ol>
				</div>

				<!-- TABS COMPONENT -->

			</div>
			<aside class="view__sidebar">
				<?php get_template_part('partials/lesson', 'sidebar'); ?>
			</aside>
		</div>
	</section>
</div>
