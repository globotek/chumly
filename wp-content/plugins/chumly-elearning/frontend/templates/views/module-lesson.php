<?php
/**
 * Created by PhpStorm.
 * User: matthew
 * Date: 4/9/18
 * Time: 12:44 PM
 */

/** We can get the wrapper post ID now we're in the loop **/
$lesson_modules = get_children( array(
	'post_parent' => $post_id,
	'post_type'   => 'chumly_lesson'
) );

if ( empty( $lesson_modules ) ) { ?>

	<div class="grouped-list">

		<div class="grouped-list__header">
			<div class="grouped-list__header__icon">
				<div class="box">
					<a href="<?php echo get_the_permalink(); ?>">
						<i class="icon fa fa-hand-o-up"></i>
					</a>
				</div>
			</div>
			<div class="grouped-list__header__content">
				<div class="box">
					<h1 class="grouped-list__heading">
						<a href="<?php echo get_the_permalink(); ?>">
							<?php echo get_the_title(); ?>
						</a>
					</h1>
					<p class="grouped-list__summary">
						<?php echo get_the_content(); ?>
					</p>
				</div>
			</div>
		</div>

	</div>

<?php } else { ?>

	<div class="grouped-list">
	
		<div class="grouped-list__header">
			<div class="grouped-list__header__icon">
				<i class="icon fa fa-hand-o-up"></i>
			</div>
			<div class="grouped-list__header__content">
				<div class="box">
					<h1 class="grouped-list__heading">
						<?php echo get_the_title(); ?>
					</h1>
					<p class="grouped-list__summary">
						<?php echo get_the_content(); ?>
					</p>
				</div>
			</div>
		</div>
		
		
		<ol class="grouped-list__items">
			
			<!-- GROUPED LIST ITEM -->
			
			<?php
			/**
			 * We get all the child posts of the wrapper post
			 * which are in fact our lesson modules.
			 */
			$sub_query = new WP_Query( array(
				'post_type'   => 'chumly_lesson',
				'post_parent' => $post_id,
				'order'       => 'ASC',
				'orderby'     => 'menu_order'
			) );
			$count     = 0;
			
			//foreach ( $lesson_modules as $module ) {
			while ( $sub_query->have_posts() ) : $sub_query->the_post();
				$count ++;
				
				/**
				 * We need to check if the lesson module has being completed
				 * so we get our _completed_modules array and then check if the post_ID
				 * is in the array.
				 */
				$completed_modules = get_user_meta( get_current_user_id(), '_completed_modules', TRUE );
				$current_module    = get_user_meta( get_current_user_id(), '_recent_activity', TRUE );
				
				if ( ! empty( $completed_modules ) && in_array( get_the_ID(), $completed_modules ) ) {
					$module_status = '--positive';
				} elseif ( $current_module == get_the_ID() ) {
					$module_status = '--neutral';
				} else {
					$module_status = '';
				}
				
				$lesson_module = get_post_meta( get_the_ID(), '_lesson_module', TRUE );
				
				switch ( $lesson_module ) {
					case 'listen':
						$module_icon = 'fa-headphones';
						break;
					case 'watch':
						$module_icon = 'fa-play-circle-o';
						break;
					case 'find':
						$module_icon = 'fa-search';
						break;
					case 'ask':
						$module_icon = 'fa-question';
						break;
					case 'huddle':
						$module_icon = 'fa-group';
						break;
					case 'try':
						$module_icon = 'fa-flask';
						break;
					case 'make':
						$module_icon = 'fa-cube';
						break;
					case 'read':
						$module_icon = 'fa-file-text-o';
						break;
					case 'reflect':
						$module_icon = 'fa-history';
						break;
					case 'quiz':
						$module_icon = 'fa-check-circle-o';
						break;
					case 'credit':
						$module_icon = 'fa-certificate';
						break;
				}
				?>
				
				<li class="grouped-list__item grouped-list__item<?php echo $module_status; ?>">
					
					<a href="<?php the_permalink(); ?>" class="grouped-list__item__inner">
						<div class="grouped-list__item__icon">
							<?php echo $count; ?>
						</div>
						<div class="grouped-list__item__content">
							<?php the_title(); ?>
						</div>
						<div class="grouped-list__item__meta">
							<span><?php echo ucfirst( $lesson_module ); ?></span><i class="fa <?php echo $module_icon; ?>"></i>
						</div>
					</a>
				</li>
			
			<?php endwhile; ?>
		
		</ol>

	</div>

<?php } ?>
