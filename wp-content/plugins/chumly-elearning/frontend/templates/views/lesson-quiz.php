
<!-- HEADLINE COMPONENT -->
<header class="headline headline--jumbo">
	<div class="headline__meta">
		<div class="headline__meta__icon-group">
			<i class="fa fa-check-square-o"></i>
			<span>Quiz</span>
		</div>
		<!--<div class="headline__meta__categories">
			<ul class="headline__category-list">
				<li>
					<a href="#">Understand</a>
				</li>
			</ul>
		</div>-->
	</div>
	<h1 class="headline__subheadline">
		<?php the_title(); ?>
	</h1>
	<?php 
	$post_parent_id = wp_get_post_parent_id(get_the_ID());
	$terms = get_the_terms($post_parent_id, 'course'); 
		
	foreach($terms as $term){
		if($term->parent != 0){	
			$term_link = get_term_link($term->term_id, 'course');
			$term_name = $term->name;
		}
	}
	?>
	<a href="<?php echo $term_link; ?>">Go back to <?php echo $term_name; ?> course index</a>
</header>

<!-- HEADLINE COMPONENT -->
<section class="view">
	<div class="view__inner">
		<div class="view__content">

			<!-- QUESTION COMPONENT -->
			
			<?php 
			$quiz_question = get_post_meta($post->ID, '_quiz_question', true);
			$quiz_answers = get_post_meta($post->ID, '_quiz_answers', true);
			$quiz_correct = get_post_meta($post->ID, '_quiz_correct_answer', true);
			?>
			
			<div class="question">
				<h2 class="question__heading"><?php echo $quiz_question; ?></h2>
				<ol class="question__answers">
					<?php foreach($quiz_answers as $answer_key => $answer_value){ ?>
						<li class="question__answer answer__<?php echo $answer_key; ?>">
							<input type="radio" name="quiz" id="answer_<?php echo $answer_key; ?>" class="question__answer__hidden-input" answer_id="<?php echo $answer_key; ?>" />
							<label class="question__answer__text" for="answer_<?php echo $answer_key; ?>">
								<div class="question__answer__text__inner"><?php echo $answer_value; ?></div>
							</label>
						</li>
					<?php } ?>
				<ol>

				<div class="form__group form__group--right">
					<p id="check_answer" class="button button--primary">Submit</p>
				</div>
			</div>
			
			<!-- QUESTION COMPONENT -->

			
		</div>
		<aside class="view__sidebar">
			<?php get_template_part('partials/lesson', 'sidebar'); ?>
		</aside>
	</div>
</section>
