<?php $term = $data; ?>

<div class="chunk">
	
	<!-- VIEW LAYOUT STARTS -->
	<section class="view">
		<div class="view__inner">
			
			<div class="view__content" data-module="update-user-activity" data-module_action="chumly_elearning_course_activity" data-object_id="<?php echo $term->term_id; ?>">
				
				<?php $lessons = unserialize( get_term_meta( $term->term_id, 'chumly_elearning_linked_children', TRUE ) ); ?>
				
				<?php foreach ( $lessons as $lesson ) { ?>
					
					<?php $lesson_ids[] = intval( $lesson[ 'ID' ] ); ?>
				
				<?php } ?>
				
				
				<!-- GROUPED LIST COMPONENT -->
				<div class="grouped-list">
					
					<?php
					/**
					 * First, we need to get the lesson, which is a parent post.
					 * The parent contains the lesson modules which we'll then
					 * retrieve via a sub-query within this loop.
					 */
					$query  = new WP_Query( array(
						'post_type' => 'chumly_lesson',
						'post__in'  => $lesson_ids,
						'order'     => 'DESC',
						'orderby'   => 'post__in'
					) );
					$letter = 'A';
					
					while ( $query->have_posts() ) : $query->the_post();
						//var_dump(get_the_ID());
						chumly_get_template( 'views', 'module-lesson', get_the_ID() );
						//include_once( 'module-lesson.php' );
					endwhile; ?>
					<!-- GROUPED LIST COMPONENT -->
				
				</div>
			
			</div>
			
			<?php
			$video_source = get_term_meta( $terms[ 0 ]->term_id, 'intro_video', TRUE );
			
			if ( ! empty( $video_source ) ) { ?>
				<aside class="view__sidebar">
					<h2 class="view__sidebar__heading">
						Watch intro video
					</h2>
					
					<!-- VIDEO PLAYER COMPONENT -->
					
					<div class="video-player">
						<video width="100%">
							<source src="<?php echo $video_source; ?>" type="video/mp4"/>
						</video>
					</div>
					
					<!-- VIDEO PLAYER COMPONENT -->
				
				</aside>
			<?php } ?>
		
		</div>
	</section>
	<!-- VIEW LAYOUT ENDS -->
	
	</section>
</div>