<?php do_action( 'chumly_before_content' ); ?>


<?php
$user_id         = get_current_user_id();
$user_first_name = chumly_get_profile_field( 'first_name', 'name', $user_id )->value;


/**
 * Get from user_meta the post ID of the most recently viewed lesson module
 * and then get the parent post's ID. The parent post's ID gets us the
 * wrapper post object which is the lesson.
 */
$current_module_id = intval( get_user_meta( $user_id, '_last_viewed_lesson_module', TRUE ) );
$current_lesson_id = intval( get_user_meta( $user_id, '_last_viewed_lesson', TRUE ) );
$current_course_id = intval( get_user_meta( $user_id, '_last_viewed_course', TRUE ) );
$lesson_post_id    = wp_get_post_parent_id( $current_course_id ); ?>
	
	<header class="hero headline--tertiary headline--non-cap">
		<h2 class="headline__heading">
			
			<?php if ( ! empty( $current_lesson_id ) ) { ?>
				
				<strong><?php echo $user_first_name; ?></strong>, you were recently looking at
				<a href="<?php echo get_permalink( $current_lesson_id ); ?>"><?php echo get_the_title( $current_lesson_id ); ?></a>
			
			<?php } else { ?>
				
				<strong><?php echo $user_first_name; ?></strong>, your playlist is empty at the moment
			
			<?php } ?>
		
		</h2>
	</header>
	
	<section class="navigator chunk">
		
		<div class="navigator__content">
			
			<?php
			/**
			 * First, we need to get the lesson, which is a parent post.
			 * The parent contains the lesson modules which we'll then
			 * retrieve via a sub-query within this loop.
			 * We use the above acquired $parent_post_id to get just
			 * the post we're after
			 */
			$query = new WP_Query( array(
				'post_type' => 'chumly_lesson',
				'p'         => $current_lesson_id
			) );
			
			while ( $query->have_posts() ) : $query->the_post();
				
				chumly_get_template( 'views', 'module-lesson', get_the_ID() );
			
			endwhile; ?>
			
		
		</div>
		
		<!-- SIDEBAR -->
			<aside class="navigator__sidebar">
								
				<?php $current_course_term = get_terms( array(
					'taxonomy'   => 'chumly_course',
					'include'    => $current_course_id,
					'hide_empty' => FALSE
				) ); ?>
				
				<?php chumly_get_template( 'partials', 'course-card', NULL, $current_course_term[ 0 ] ); ?>
				
			</aside>
		
		<!-- SIDEBAR -->
	
	</section>

	<section class="chunk">
		
		<!-- COURSE GRID COMPONENT -->
		<div class="course-grid course-grid--med">
			<h2 class="course-grid__heading">
				Courses saved for later
			</h2>
			
			<ol class="course-grid__list">
				
				<!-- course-card COMPONENT -->
				<?php
				/** Get an array of our saved courses */
				$course_ids = get_user_meta( get_current_user_id(), '_saved_courses', TRUE );
				
				/**
				 * Check for courses.
				 */
				if ( empty( $course_ids ) ) {
					
					echo '<li>Nothing saved at the moment</li>';
					
				} else {
					
					/**
					 * Take the IDs of saved categories, and get
					 * all the objects to build the UI
					 */
					$terms = get_terms( array(
						'taxonomy'   => 'chumly_course',
						'include'    => $course_ids,
						'hide_empty' => FALSE
					) );
					
					/**
					 * Loop over terms (courses) and get all the individual
					 * bits of data we need to build the UI
					 */
					foreach ( $terms as $term ) { ?>
						
						<li class="course-grid__item">
							
							<?php chumly_get_template( 'partials', 'course-card', NULL, $term ); ?>
						
						</li>
					
					<?php }
				} ?>
				<!-- course-card COMPONENT -->
			
			</ol>
		</div>
		<!-- COURSE GRID COMPONENT -->
	
	</section>
	
	
	<section class="chunk">
	<!-- VIEW LAYOUT -->
	
	<!-- CALL TO ACTION LIST COMPONENT-->
	<div class="call-to-action-list">
		<h2 class="call-to-action-list__heading">
			Other content saved for later
		</h2>
		<?php $saved_modules = get_user_meta( get_current_user_id(), '_saved_modules', TRUE ); ?>
		
		<ol class="call-to-action-list__items">
			
			<?php if ( ! empty( $saved_modules ) ) { ?>
				
				<?php foreach ( $saved_modules as $module_id ) { ?>
					<?php $module_title = get_the_title( $module_id ); ?>
					<?php $module_type = get_post_meta( $module_id, '_lesson_module', TRUE ); ?>
					<?php $module_permalink = get_the_permalink( $module_id ); ?>
					<li>
						<div class="call-to-action">
							<div class="call-to-action__inner">
								<div class="call-to-action__decor">
									<?php
									$lesson_module = get_post_meta( $module_id, '_lesson_module', TRUE );

									switch ( $lesson_module ) {
										case 'listen':
											$module_icon = 'fa-headphones';
											break;
										case 'watch':
											$module_icon = 'fa-play-circle-o';
											break;
										case 'find':
											$module_icon = 'fa-search';
											break;
										case 'ask':
											$module_icon = 'fa-question';
											break;
										case 'huddle':
											$module_icon = 'fa-group';
											break;
										case 'try':
											$module_icon = 'fa-flask';
											break;
										case 'make':
											$module_icon = 'fa-cube';
											break;
										case 'read':
											$module_icon = 'fa-file-text-o';
											break;
										case 'reflect':
											$module_icon = 'fa-history';
											break;
										case 'quiz':
											$module_icon = 'fa-check-circle-o';
											break;
										case 'credit':
											$module_icon = 'fa-certificate';
											break;
									}
									?>
									<i class="fa <?php echo $module_icon; ?>"></i>
								</div>
								<div class="call-to-action__text">
									<h3 class="call-to-action__heading"><?php echo $module_title; ?></h3>
									<p class="call-to-action__sub-heading"><?php echo ucfirst( $module_type ); ?></p>
								</div>
								<a class="call-to-action__link" href="<?php echo $module_permalink; ?>"><i class="fa fa-arrow-circle-o-right"></i></a>
								<button class="call-to-action__link delete_module is-muted" module_id="<?php echo $module_id; ?>">
									<i class="fa fa-times-circle"></i></button>
							</div>
						</div>
					</li>
				<?php } ?>
			
			<?php } else { ?>
				
				<li>Nothing saved at the moment</li>
			
			<?php } ?>
		
		</ol>
	</div>
	<!-- CALL TO ACTION LIST COMPONENT-->
	
	</section>

	<div id="ajax-response"></div>

<?php do_action( 'chumly_after_content' ); ?>