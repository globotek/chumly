<?php
/**
 * Created by PhpStorm.
 * User: matthew
 * Date: 19/7/18
 * Time: 2:23 PM
 */ ?>

<?php get_header(); ?>
<?php global $post; ?>
<?php do_action( 'chumly_before_content' ); ?>

<?php
$post_id        = get_the_ID();
$parent_post_id = wp_get_post_parent_id( get_the_ID() );

if ( ! $parent_post_id ) {
	$parent_post_id = $post_id;
}

?>
<div class="wrapper">
<main class="generic-page" data-module="update-user-activity" data-module_action="chumly_elearning_lesson_activity" data-parent_object_id="<?php echo $parent_post_id; ?>" data-object_id="<?php echo $post_id; ?>">
	
	<div class="generic-page__primary">
		
		
			
			<?php if ( have_posts() ) {
				while ( have_posts() ): the_post(); ?>
					
					<?php
					$lesson_type = get_post_meta( get_the_ID(), '_lesson_module', TRUE );
					
					$template_part_file = '';
					
					switch ( $lesson_type ) {
						case 'article':
						default:
							$template_part_file = 'article';
							break;
						case 'watch':
							$template_part_file = 'watch';
							break;
						case 'quiz':
							$template_part_file = 'quiz';
							break;
					}
					
					
					?>
					
					<section class="chunk">

						<header class="headline">
										
							<h1 class="headline__heading">
								<?php the_title(); ?>
							</h1>
							
							<?php
							$post_parent_id = wp_get_post_parent_id( get_the_ID() );
							$terms          = get_the_terms( $post_parent_id, 'chumly_course' );
							
							foreach ( $terms as $term ) {
								if ( $term->parent != 0 ) {
									$term_link = get_term_link( $term->term_id, 'chumly_course' );
									$term_name = $term->name;
								}
							}
							?>
							<?php echo do_shortcode( '[chumly_ratings]' ); ?>
							<!--										<a href="--><?php //echo $term_link; ?><!--">Go back to --><?php //echo $term_name; ?><!-- course index</a>-->
						
						</header>
						
						<div class="navigator">
							
							<main class="navigator__content">
								
								<div class="box">
									
									<?php chumly_get_template( 'views', 'lesson-' . $template_part_file ); ?>
								
								</div>
								
								<!--								<div class="box breathe--top">-->
								<!--									-->
								<!--									--><?php //echo do_shortcode( '[chumly_ratings]' ); ?>
								<!--								-->
								<!--								</div>-->
								
								<div class="chunk">

									<div class="box">
									
										<h1 class="headline__subheadline">Your Notes</h1>
										
										<article class="breathe--top">
											
											<?php
											$user_notes = get_user_meta( get_current_user_id(), '_user_notes', TRUE );
											$post_id    = get_the_ID();
											
											if ( ! empty( $user_notes ) ) {
												$user_notes = array_reverse( $user_notes );
												
												foreach ( $user_notes as $user_note ) {
													if ( $user_note[ 'note_post' ] == $post_id ) {
														$user_data = get_userdata( $user_note[ 'note_author' ] );
														?>
														
														<!-- COMMENT COMPONENT -->
														
														<li class="comment">
															<div class="comment__inner">
																<article class="comment__body" style="margin-top:0px">
																	<header class="comment__header">
																		<p class="headline__heading">
																			<?php echo ucfirst( $user_note[ 'note_privacy' ] ); ?> note on
																			<time datetime="<?php echo date( 'd-m-Y', $user_note[ 'note_timestamp' ] ); ?>"><?php echo date( 'd F Y \a\t g:ia', $user_note[ 'note_timestamp' ] ); ?></time>
																		</p>
																	</header>
																	
																	<p><?php echo $user_note[ 'note_content' ]; ?></p>
																	
																	<!--<a href="#" class="comment__reply">Reply</a>-->
																</article>
															</div>
														</li>
														
														<!-- COMMENT COMPONENT -->
														<?php
													}
												}
											} else { ?>
												
												<p>Your notes will appear here</p>
											
											<?php } ?>
										</article>

									</div>

								</div>

								<hr>

								<div class="chunk--mini">

									<h3 class="user-profile__sub-heading breathe--top">Post a Message</h3>
									
									<div class="chunk">

										<?php wp_reset_query(); ?>
										<?php chumly_post_form( 'chumly_comment', array(
											'target_id'   => get_the_ID(),
											'post_format' => 'comment'
										) ); ?>
										<?php chumly_post_feed( array( 'post_id' => get_the_ID() ) ); ?>
									
									</div>

								</div>
							
							</main>
							
							<aside class="navigator__sidebar">
								
								<?php chumly_get_template( 'partials', 'lesson-sidebar' ); ?>
							
							</aside>
						
						</div>
					
					</section>
				
				
				<?php endwhile;
			} ?>
		</div>
	
</main>
</div>
<?php do_action( 'chumly_after_content' ); ?>

<?php get_footer(); ?>
