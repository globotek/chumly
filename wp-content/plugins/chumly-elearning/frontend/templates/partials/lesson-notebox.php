<?php
/**
 * Created by PhpStorm.
 * User: matthew
 * Date: 6/7/18
 * Time: 11:26 AM
 */ ?>

<div class="notebox">
	<div class="notebox__header">
		<div class="box box--radius_top">
			<h2 class="notebox__heading">
				<i class="fa fa-pencil-square"></i>
				Make a note on this content
			</h2>
		</div>
	</div>
	<div class="notebox__content">
		<div class="box box--radius_bottom">
			<form class="form">
				<div class="form__group">
					<textarea id="lesson_note" class="form__group__input form__group__input--multiline" placeholder="Type away!"></textarea>
				</div>
				
				<div class="form__group">
					<select id="note_privacy" class="form__group__select" placeholder="Set Comment Privacy.">
						<option value="public">Public</option>
						<option value="private">Private</option>
					</select>
				</div>
				
				<div class="form__group form__group--right">
					<p id="save_note" class="button button--positive button button--right">Save This Note</p>
				</div>
			</form>
		</div>
	</div>
</div>

