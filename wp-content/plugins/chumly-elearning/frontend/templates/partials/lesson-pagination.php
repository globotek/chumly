<?php
/**
 * Created by PhpStorm.
 * User: matthew
 * Date: 6/7/18
 * Time: 11:17 AM
 */
function link_page( $link ) {
	global $post;
	
	$pages = get_pages( array(
		'post_type'   => 'chumly_lesson',
		'parent'      => $post->post_parent,
		'sort_column' => 'menu_order'
	) );
	
	$total_modules = count( $pages ) - 1;
	
	foreach ( $pages as $key => $sibling ) {
		if ( $post->ID == $sibling->ID ) {
			$ID = $key;
		}
	}
	
	$permalink = array( 'before' => get_permalink( $pages[ $ID - 1 ]->ID ), 'after' => get_permalink( $pages[ $ID + 1 ]->ID ) );
	$title     = array( 'before' => get_the_title( $pages[ $ID - 1 ]->ID ), 'after' => get_the_title( $pages[ $ID + 1 ]->ID ) );
	
	if ( $link == 'before' && $ID >= 1 ) {
		echo
			'<a class="button" href="' . $permalink[ $link ] . '">
				<span class="post-navigation__nav__icon">
					<i class="fa fa-arrow-circle-o-left"></i>
				</span>
				<span class="post-navigation__nav__text">' . $title[ $link ] . '</span>
			</a>';
	} elseif ( $link == 'after' && $ID < $total_modules ) {
		echo
			'<a class="button" href="' . $permalink[ $link ] . '">
				<span class="post-navigation__nav__text">' . $title[ $link ] . '</span>
				<span class="post-navigation__nav__icon">
					<i class="fa fa-arrow-circle-o-right"></i>
				</span>
			</a>';
	} elseif ( $link == 'after' && $ID >= $total_modules ) {
		$post_parent_id = wp_get_post_parent_id( get_the_ID() );
		$terms          = get_the_terms( $post_parent_id, 'chumly_course' );
		
		foreach ( $terms as $term ) {
			if ( $term->parent != 0 ) {
				$term_link = get_term_link( $term->term_id, 'chumly_course' );
				$term_name = $term->name;
			}
		}
		
		echo
			'<a class="button" href="' . $term_link . '">
				<span class="post-navigation__nav__icon">
					<i class="fa fa-home"></i>
				</span>
				<span class="post-navigation__nav__text">Back to Course</span>
			</a>';
	}
}

?>


	
	<div class="post-navigation">
		<nav class="post-navigation__nav">
			<div class="post-navigation__nav__item">
				<?php link_page( 'before' ); ?>
			</div>
			
			<div class="post-navigation__nav__item">
				<?php link_page( 'after' ); ?>
			</div>
		</nav>
		<div class="post-navigation__button-wrapper">
			<?php
			$completed_modules = get_user_meta( get_current_user_id(), '_completed_modules', TRUE );
			
			if ( !empty( $completed_modules ) && in_array( get_the_ID(), $completed_modules ) ) {
				echo '<p class="button button--inactive">Completed</p>';
			} else {
				echo '<p id="mark_complete" class="button button--primary button">Mark as Complete</p>';
			}
			?>
		</div>
	</div>
