<?php
/**
 * Created by PhpStorm.
 * User: matthew
 * Date: 20/7/18
 * Time: 1:01 PM
 */ ?>


<header class="hero">
	
	<div class="hero__wrapper">
		
		<h1 class="hero__heading">
			
			<div class="hero__heading__icon">
				<i class="fa fa-flask"></i>
			</div>
			
			<?php single_cat_title(); ?>
		
		</h1>
		
		<div class="hero__content <?php //if ( $course_media != NULL ) { echo 'hero__content--narrow'; } ?>">
			
			<p class="hero__summary">
				
				<?php echo $term->description; ?>
			
			</p>
			
			<?php if ( $course_media != NULL ) { ?>
				
				<div class="hero__decor">
					
					<?php if ( pathinfo( $course_media )[ 'extension' ] == 'mp4' ) { ?>
						
						<video controls width="100%">
							<source src="<?php echo $course_media; ?>" type="video/mp4">
						</video>
					
					<?php } else { ?>
						
						<img src="<?php echo $course_media; ?>" alt="Illustration representing Understand"/>
					
					
					<?php } ?>
				
				</div>
			
			<?php } ?>
		
		
		</div>
	
	
	</div>

</header>
	
	

