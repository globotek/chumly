<?php
/**
 * Created by PhpStorm.
 * User: matthew
 * Date: 5/7/18
 * Time: 4:55 PM
 */

$curriculum         = get_term_by( 'id', $data->parent, 'chumly_course', OBJECT );
$course_id          = $data->term_id;
$course_name        = $data->name;
$course_description = get_term_meta( $data->term_id, 'short_description', TRUE );
$course_link        = get_term_link( $data );
$parent_term        = get_term( $data->parent, 'chumly_course' );
$bg_color           = get_term_meta( $parent_term->term_id, 'course_color', TRUE );

if ( get_term_meta( $parent_term->term_id, 'course_color_transparent', TRUE ) ) {
	
	$bg_color = 'transparent';
	
}
?>


<div class="course-card is-course" data-module="course-actions" data-course_id="<?php echo $course_id; ?>">
	
	<div class="course-card__header" style="background-color:<?php echo $bg_color; ?>">
		<div class="box box--radius_top">
			<div class="course-card__header__inner">
				<h3 class="course-card__header__heading">
					<i class="fa fa-flask"></i>
					<?php echo esc_attr( $curriculum->name ); ?>
				</h3>
				<span class="course-card__header__meta">
					<i class="fa fa-th-large"></i>
					Course
				</span>
			</div>
		</div>
	</div>
	<div class="course-card__content">
		<div class="box box--radius_bottom">
			<h3 class="course-card__content__heading">
				<?php echo $course_name; ?>
			</h3>
			<p class="course-card__content__summary"><?php echo nl2br( $course_description ); ?></p>
			
			<div class="course-card__content__button-group">
				<a class="button button--primary" href="<?php echo $course_link; ?>">
					Go To Course
				</a>
			</div>

			<?php do_action( 'chumly_course_card_actions', $course_id ); ?>
		</div>
	</div>

</div>



