<?php $lesson_type = get_post_meta( get_the_ID(), '_lesson_module', TRUE ); ?>

<?php chumly_get_template( 'partials', 'lesson-pagination' ); ?>

<div class="breathe--top">
	<?php chumly_get_template( 'partials', 'lesson-notebox' ); ?>
</div>

<h3 class="navigator__sidebar__heading breathe--top">
	Goes well with
</h3>

<?php $saved_modules = get_user_meta( get_current_user_id(), '_saved_modules', TRUE ); ?>

<?php $related_lesson_one_id = get_post_meta( $post->ID, '_related_lesson_one', TRUE ); ?>
<?php $related_lesson_one_title = get_the_title( $related_lesson_one_id ); ?>
<?php $related_lesson_one_module = get_post_meta( $related_lesson_one_id, '_lesson_module', TRUE ); ?>
<?php $related_lesson_one_permalink = get_the_permalink( $related_lesson_one_id ); ?>

<?php if ( !empty( $related_lesson_one_id ) ) { ?>

<div class="box breathe--top">
	
	<div class="call-to-action">
		<div class="call-to-action__inner">
			<input type="hidden" class="hidden_post_id" post_id="<?php echo $related_lesson_one_id; ?>"/>
			<div class="call-to-action__decor">
				<?php
				$lesson_module = get_post_meta( $related_lesson_one_id, '_lesson_module', TRUE );
				
				switch ( $lesson_module ) {
					case 'listen':
						$module_icon = 'fa-headphones';
						break;
					case 'watch':
						$module_icon = 'fa-play-circle-o';
						break;
					case 'find':
						$module_icon = 'fa-search';
						break;
					case 'ask':
						$module_icon = 'fa-question';
						break;
					case 'huddle':
						$module_icon = 'fa-group';
						break;
					case 'try':
						$module_icon = 'fa-flask';
						break;
					case 'make':
						$module_icon = 'fa-cube';
						break;
					case 'read':
						$module_icon = 'fa-file-text-o';
						break;
					case 'reflect':
						$module_icon = 'fa-history';
						break;
					case 'quiz':
						$module_icon = 'fa-check-circle-o';
						break;
					case 'credit':
						$module_icon = 'fa-certificate';
						break;
				}
				?>
				<i class="fa <?php echo $module_icon; ?>"></i>
			</div>
			<div class="call-to-action__text">
				<h3 class="call-to-action__heading"><?php echo $related_lesson_one_title; ?></h3>
				<p class="call-to-action__sub-heading"><?php echo ucfirst( $related_lesson_one_module ); ?></p>
			</div>
			<?php if ( !empty( $saved_modules ) && in_array( $related_lesson_one_id, $saved_modules ) ) { ?>
				<button class="call-to-action__link"></button>
			<?php } else { ?>
				<button class="call-to-action__link save_module"><i class="fa fa-plus-circle"></i></button>
			<?php } ?>
			<a class="call-to-action__link" href="<?php echo $related_lesson_one_permalink; ?>"><i class="fa fa-arrow-circle-o-right"></i></a>
		</div>
	</div>
	
	<?php } ?>

</div>
	