<?php
/**
 * Created by PhpStorm.
 * User: matthew
 * Date: 5/7/18
 * Time: 1:25 PM
 */
function chumly_elearning_update_lesson_activity() {
	
	update_user_meta( $_POST[ 'user_id' ], '_last_viewed_lesson_module', $_POST[ 'post_id' ] );
	update_user_meta( $_POST[ 'user_id' ], '_last_viewed_lesson', $_POST[ 'parent_post_id' ] );
	
	chumly_die();
	
}

add_action( 'wp_ajax_chumly_elearning_update_lesson_activity', 'chumly_elearning_update_lesson_activity' );
add_action( 'wp_ajax_nopriv_chumly_elearning_update_lesson_activity', 'chumly_elearning_update_lesson_activity' );


function chumly_elearning_update_course_activity(){
	
	update_user_meta( $_POST[ 'user_id' ], '_last_viewed_course', $_POST[ 'course_id' ] );
	
	chumly_die();
	
}

add_action( 'wp_ajax_chumly_elearning_update_course_activity', 'chumly_elearning_update_course_activity' );
add_action( 'wp_ajax_nopriv_chumly_elearning_update_course_activity', 'chumly_elearning_update_course_activity' );


function chumly_mark_lesson_complete() {
	
	$completed_modules = get_user_meta( $_POST[ 'user_id' ], '_completed_modules', TRUE );
	
	if ( is_array( $completed_modules ) ) {
		if ( !in_array( $_POST[ 'post_id' ], $completed_modules ) ) {
			$completed_modules[] = $_POST[ 'post_id' ];
			update_user_meta( $_POST[ 'user_id' ], '_completed_modules', $completed_modules );
		}
	} else {
		$completed_modules[] = $_POST[ 'post_id' ];
		update_user_meta( $_POST[ 'user_id' ], '_completed_modules', $completed_modules );
	}
	
	chumly_die();
	
}

add_action( 'wp_ajax_chumly_mark_lesson_complete', 'chumly_mark_lesson_complete' );
add_action( 'wp_ajax_nopriv_chumly_mark_lesson_complete', 'chumly_mark_lesson_complete' );


function chumly_save_lesson_note() {
	
	$note_content   = sanitize_text_field( stripslashes( $_POST[ 'note_content' ] ) );
	$note_privacy   = sanitize_text_field( $_POST[ 'note_privacy' ] );
	$note_author    = sanitize_text_field( $_POST[ 'user_id' ] );
	$note_timestamp = time();
	
	$note_data = array(
		'note_post'      => $_POST[ 'post_id' ],
		'note_privacy'   => $note_privacy,
		'note_content'   => $note_content,
		'note_timestamp' => $note_timestamp,
		'note_author'    => $note_author
	);
	
	$user_notes = get_user_meta( $_POST[ 'user_id' ], '_user_notes', TRUE );
	
	if ( $note_content != NULL ) {
		$user_notes[] = $note_data;
		
		update_user_meta( $_POST[ 'user_id' ], '_user_notes', $user_notes );
	}
	
	chumly_die();
	
}

add_action( 'wp_ajax_chumly_save_lesson_note', 'chumly_save_lesson_note' );
add_action( 'wp_ajax_nopriv_chumly_save_lesson_note', 'chumly_save_lesson_note' );


function chumly_save_related_lesson_to_playlist() {
	
	$related_lesson_id  = sanitize_text_field( $_POST[ 'related_post_id' ] );
	$_POST[ 'user_id' ] = sanitize_text_field( $_POST[ 'user_id' ] );
	$saved_modules      = get_user_meta( $_POST[ 'user_id' ], '_saved_modules', TRUE );
	
	$saved_modules[] = $related_lesson_id;
	
	update_user_meta( $_POST[ 'user_id' ], '_saved_modules', $saved_modules );
	
	chumly_die();
	
}

add_action( 'wp_ajax_chumly_save_related_lesson_to_playlist', 'chumly_save_related_lesson_to_playlist' );
add_action( 'wp_ajax_nopriv_chumly_save_related_lesson_to_playlist', 'chumly_save_related_lesson_to_playlist' );


function chumly_delete_related_lesson_from_playlist() {
	
	$related_lesson_id  = sanitize_text_field( $_POST[ 'related_post_id' ] );
	$_POST[ 'user_id' ] = sanitize_text_field( $_POST[ 'user_id' ] );
	$saved_modules      = get_user_meta( $_POST[ 'user_id' ], '_saved_modules', TRUE );
	
	$array_key = array_search( $related_lesson_id, $saved_modules );
	unset( $saved_modules[ $array_key ] );
	$saved_modules = array_values( $saved_modules );
	
	update_user_meta( $_POST[ 'user_id' ], '_saved_modules', $saved_modules );
	
	chumly_die();
	
}

add_action( 'wp_ajax_chumly_delete_related_lesson_from_playlist', 'chumly_delete_related_lesson_from_playlist' );
add_action( 'wp_ajax_nopriv_chumly_delete_related_lesson_from_playlist', 'chumly_delete_related_lesson_from_playlist' );


function chumly_check_quiz_answer() {
	
	$_POST[ 'post_id' ] = sanitize_text_field( $_POST[ 'post_id' ] );
	
	$submitted_answer = sanitize_text_field( $_POST[ 'submitted_answer_id' ] );
	$valid_answer     = get_post_meta( $_POST[ 'post_id' ], '_quiz_correct_answer', TRUE );
	
	if ( $submitted_answer == $valid_answer ) {
		_e( '<script>
				jQuery(".answer__' . $submitted_answer . '").addClass("is-right");
				jQuery(".answer__' . $submitted_answer . ' .question__answer__text").after("<div class=\"question__answer__result is-visible\"><div class=\"question__answer__result__inner\"><span>Correct</span></div></div>");
				</script>' );
		
		$completed_modules = get_user_meta( $_POST[ 'user_id' ], '_completed_modules', TRUE );
		$lesson_points     = get_post_meta( $_POST[ 'post_id' ], '_lesson_points', TRUE );
		$user_points       = get_user_meta( $_POST[ 'user_id' ], '_total_points', TRUE );
		
		if ( is_array( $completed_modules ) ) {
			if ( !in_array( $_POST[ 'post_id' ], $completed_modules ) ) {
				$completed_modules[] = $_POST[ 'post_id' ];
				$updated_points      = $lesson_points + $user_points;
				
				update_user_meta( $_POST[ 'user_id' ], '_completed_modules', $completed_modules );
				update_user_meta( $_POST[ 'user_id' ], '_total_points', $updated_points );
			}
		} else {
			$completed_modules[] = $_POST[ 'post_id' ];
			$updated_points      = $lesson_points + $user_points;
			
			update_user_meta( $_POST[ 'user_id' ], '_completed_modules', $completed_modules );
			update_user_meta( $_POST[ 'user_id' ], '_total_points', $updated_points );
		}
		echo '<script>jQuery("#mark_complete").removeAttr("id").removeClass("button--success").addClass("button--inactive").html("Completed");</script>';
		
		echo $updated_points;
	} else {
		
		_e( '<script>
				jQuery(".answer__' . $submitted_answer . '").addClass("is-wrong");
				jQuery(".answer__' . $submitted_answer . ' .question__answer__text").after("<div class=\"question__answer__result is-visible\"><div class=\"question__answer__result__inner\"><span>Wrong! Try again...</span></div></div>");
				</script>' );
	}
	
	chumly_die();
	
}

add_action( 'wp_ajax_chumly_check_quiz_answer', 'chumly_check_quiz_answer' );
add_action( 'wp_ajax_nopriv_chumly_check_quiz_answer', 'chumly_check_quiz_answer' );