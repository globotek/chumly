<?php
/**
 * Created by PhpStorm.
 * User: matthew
 * Date: 5/7/18
 * Time: 4:22 PM
 */
function chumly_update_course_playlist() {
	
	if ( $_POST[ 'update_action' ] == 'save' ) {
		
		$course_id = $_POST[ 'course_id' ];
		
		
		$user_courses = get_user_meta( $_POST[ 'user_id' ], '_saved_courses', TRUE );
		
		if ( !$user_courses ) {
			$user_courses = array();
		}
		
		if ( $course_id != NULL ) {
			$user_courses[] = $course_id;
			
			update_user_meta( $_POST[ 'user_id' ], '_saved_courses', $user_courses );
		}
		
	} else {
		
		$course_id = sanitize_text_field( $_POST[ 'course_id' ] );
		
		$course_ids = get_user_meta( $_POST[ 'user_id' ], '_saved_courses', TRUE );
		
		$located_key = array_search( $course_id, $course_ids );
		
		unset( $course_ids[ $located_key ] );
		
		update_user_meta( $_POST[ 'user_id' ], '_saved_courses', $course_ids );
		
	}
	
	chumly_die();
	
}

add_action( 'wp_ajax_chumly_update_course_playlist', 'chumly_update_course_playlist' );
add_action( 'wp_ajax_nopriv_chumly_update_course_playlist', 'chumly_update_course_playlist' );


function chumly_course_card_remove( $course_id ) {
	
	echo '<p title="Remove this item" class="course-card__cancel" term_id="' . $course_id . '">';
	echo '<i class="fa fa-times-circle chumly-action__trigger" data-module_action="update_course_playlist"></i>';
	echo '</p>';

	
}

add_action( 'chumly_course_card_actions', 'chumly_course_card_remove' );
