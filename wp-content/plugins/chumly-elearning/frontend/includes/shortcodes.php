<?php
/**
 * Created by PhpStorm.
 * User: matthew
 * Date: 3/7/18
 * Time: 2:49 PM
 */
function chumly_elearning_subjects() {
	ob_start();
	include( plugin_dir_path( __DIR__ ) . 'templates/archive-subject.php' );
	$output = ob_get_clean();
	
	return $output;
}

add_shortcode( 'chumly_elearning_subjects', 'chumly_elearning_subjects' );


function chumly_elearning_courses() {
	ob_start();
	include( plugin_dir_path( __DIR__ ) . 'templates/archive-course.php' );
	$output = ob_get_clean();
	
	return $output;
}

add_shortcode( 'chumly_elearning_courses', 'chumly_elearning_courses' );


function chumly_elearning_playlist() {
	ob_start();

	include( plugin_dir_path( __DIR__ ) . 'templates/playlist.php' );
	$output = ob_get_clean();
	
	return $output;
}

add_shortcode( 'chumly_elearning_playlist', 'chumly_elearning_playlist' );
