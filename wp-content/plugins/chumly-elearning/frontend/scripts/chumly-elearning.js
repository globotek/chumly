(function ($) {

	$.fn.chumlyTaxonomyRowBuilder = function() {
console.log('Go!!!');
		var index = 0,
		    count = 0,
		    ID, name;

		$('.elearning-builder ul').sortable({
			handle: '.elearning-builder-column-order',
			//cancel: '.row-content',
			axis: 'y',
			create: function () {
				update_elearning_order_count();
			},
			update: function () {
				update_elearning_order_count();
			}
		});

		$('.elearning-builder ul').disableSelection();


		function update_elearning_order_count() {

			count = 0;

			var builderList = $('.elearning-builder ul').children('li.elearning-builder-row');

			builderList.each(function () {

				var countIndex = count;

				count++;

				$(this).children('.elearning-builder-column-order').html('<span class="circle">' + count + '</span>');
				$(this).children('[name="elearning-subject[course_order]"]').val(count);

				$(this).children('.elearning-builder-id').attr('name', 'chumly-elearning-linked-children[' + countIndex + '][ID]');
				$(this).children('.elearning-builder-name').attr('name', 'chumly-elearning-linked-children[' + countIndex + '][name]');

			});


		}


		$('.elearning-builder-add').on('click', function (event) {
			console.log('new Row');
			event.preventDefault();

			var selection = $('.elearning-builder-selector select option:selected').get().reverse(),
			    lastRow   = $('.elearning-builder').find('.elearning-builder-row').last(),
			    deleteLastRow;

			index = $('input[name="total-elearning-rows"]').val();

			if (index == 0) {
				deleteLastRow = true;
			}

			$.each(selection, function (key, elem) {

				var elem   = $(elem),
				    newRow = lastRow.clone();

				ID = elem.val();
				name = elem.text();

				lastRow.after(newRow);

				$('input[name="total-elearning-rows"]').val(++index);
				newRow.find('.elearning-builder-id').val(ID);
				newRow.find('.elearning-builder-name').val(name);
				newRow.find('.elearning-builder-placeholder p').text(name);

				newRow.css('display', 'flex');


			});

			if (deleteLastRow) {
				lastRow.remove();
			}

			update_elearning_order_count();

		});


		$('.elearning-builder').on('click', 'a[href="#delete"]', function (event) {

			event.preventDefault();

			var row      = $(this).parents('.elearning-builder-row'),
			    rowCount = $(this).parents('.elearning-builder').find($('.elearning-builder-row')).length;

			console.log('Row Count', rowCount);

			if (rowCount > 1) {

				row.remove();

			} else {

				row.find($('.elearning-builder-id')).val('');
				row.hide();

			}

			var deleteIndex = $('input[name="total-elearning-rows"]').val();

			$('input[name="total-elearning-rows"]').val(--deleteIndex);

			update_elearning_order_count();

		});

	}


}(jQuery));
/**
 * Created by matthew on 5/7/18.
 */
(function ($) {
	
	$.fn.chumlyCourseActions = function () {
		
		console.log('Course Actions');
		
		var elem = $(this),
			triggers = elem.find('.js-action__trigger'),
			courseID = elem.data('course_id'),
			userID = chumly_vars.user_id;
		
		var init = function () {
				
				triggers.off('click').on('click', function (event) {
					
					event.preventDefault();
					
					var trigger = $(this),
						moduleAction = trigger.data('module_action');
					
					if (moduleAction == 'update_course_playlist') {
						
						var updateAction = trigger.attr('data-update_action');
						
						if(updateAction == 'save') {
							
							
							updateCoursePlaylist(trigger, updateAction);
							
						} else {
							
							
							updateCoursePlaylist(trigger, updateAction);
							
						}
							
					}
					
				});
				
			},
			updateCoursePlaylist = function ( trigger, updateAction ) {
				console.log(updateAction);
				$.ajax({
					url: chumly_vars.ajax_url,
					type: 'POST',
					data: {
						'action': 'chumly_update_course_playlist',
						'course_id': courseID,
						'user_id': userID,
						'update_action': updateAction
					},
					success: function (data) {
						
						if(updateAction == 'save') {
							console.log('Add to playlist');
							console.log(trigger);
							trigger.html('Remove from playlist').attr('data-update_action', 'delete');
						
						} else {
						 console.log(trigger);
							if(trigger.hasClass('button')) {
								console.log('Remove from playlist');
								trigger.html('Add to playlist').attr('data-update_action', 'save');
								
							} else if(trigger.is('i')){
								
								console.log('Icon delete');
								
								elem.fadeOut('250', function(){
									$(this).remove();
								});
								
							}
								
						}
					}
				});
								
			};
		
		init();
		
	}
	
}(jQuery));
(function ($) {

    $.fn.tabs = function () {
        
        var elem = $(this),
            triggers = elem.find(".js-tabs__trigger"),
            targets = elem.find(".js-tabs__target"),
            settings = {
                activeClass: "is-active"
            };

        var init = function () {

            if (triggers.any()) {

                // Attach click to triggers
                triggers.off("click").on("click", function (evt) {

                    evt.preventDefault();

                    // Load up the trigger, it's data, any related triggers and the target that we're looking for
                    var trigger = $(this),
                        data = trigger.attr("data-target"),
                        relatedtriggers = triggers.filter("[data-target='" + data + "']"),
                        target = targets.filter("#" + data);

                    // If the target is there and this trigger is not active
                    if (target.any() && !trigger.hasClass(settings.activeClass)) {

                        // Clean up targets and add active class to targeted target
                        targets.removeClass(settings.activeClass);
                        target.addClass(settings.activeClass);

                        // Clean up triggers and add active class to targeted trigger
                        triggers.removeClass(settings.activeClass);
                        trigger.addClass(settings.activeClass);

                        // Find triggers, if any add active class to them
                        if (relatedtriggers.length > 0) {
                            relatedtriggers.each(function () {
                                $(this).addClass(settings.activeClass);
                            });
                        }

                    }
                });
            }

            $(window).on("hashchange", function() {
                processUrl();
            });
        },

        // Try and find a trigger based on hash. If trigger found run click method
        processUrl = function () {

            var hash = window.location.hash,
                trigger = triggers.filter("[data-target='" + hash.replace("#", "") + "']");

                console.log(hash);

            if (trigger.length > 0) {

                if (trigger.length > 1) {
                    trigger = trigger.eq(0);
                }

                trigger.trigger("click");
            }

        };
        
        // run methods
        init();
        processUrl();

        return this;
    };

}(jQuery));
(function($) {

	$.fn.toggle = function() {
		var elem = $(this),
			targets = $('.js-toggle__target'),
			triggers = elem.find('.js-toggle__trigger'),
			reset = $('.js-toggle__reset'),
			settings = {
				activeClass: 'is-active',
				visibleClass: 'is-visible'
			};

		var init = function() {
			
			triggers.off('click').on('click', function(evt) {

				evt.preventDefault();

				// Load trigger and target
				var trigger = $(this),
					target = targets.filter('#' + trigger.attr('data-target'));

				// Toggle menu state accordingly
				if(trigger.hasClass(settings.activeClass)) {
					toggle(trigger, target, 'off');
				}
				else {
					toggle(trigger, target, 'on');
				}

			});

			reset.off('click').on('click', function(evt) {
				evt.preventDefault();

				toggle('reset');
			});
		},

		toggle = function(trigger, target, state) {

			switch(state) {
				case 'off':
					target.removeClass(settings.visibleClass);
					trigger.removeClass(settings.activeClass);
					reset.removeClass(settings.visibleClass);
					break;
				case 'on':
					target.addClass(settings.visibleClass);
					trigger.addClass(settings.activeClass);
					reset.addClass(settings.visibleClass);
					break;
				case 'reset':
				default:
					triggers.removeClass(settings.activeClass);
					targets.removeClass(settings.visibleClass);
					reset.removeClass(settings.visibleClass);
					break;
			}
		}

		init();
		return this;
	};

}(jQuery));
/**
 * Created by matthew on 5/7/18.
 */
(function ($) {
	
	$.fn.chumlyUpdateActivity = function () {
		
		var elem           = $(this),
		    moduleAction   = elem.data('module_action'),
		    objectID       = elem.data('object_id'),
		    parentObjectID = elem.data('parent_object_id');
		
		console.log('Activity Module');
		console.log(moduleAction);
		//console.log(postID);
		//console.log(user_id);
		
		var init               = function () {
			
			    //console.log('Single Lesson');
			    switch (moduleAction) {
				
				    case 'chumly_elearning_lesson_activity':
					    updateLatestLesson();
					    break;
				
				    case 'chumly_elearning_course_activity':
					    updateLatestCourse();
					    break;
				
			    }
			
			
		    },
		    updateLatestLesson = function () {
			
			    //console.log(chumly_vars.user_id);
			
			    $.ajax({
				    url:  chumly_vars.ajax_url,
				    type: 'POST',
				    data: {
					    'action':         'chumly_elearning_update_lesson_activity',
					    'post_id':        objectID,
					    'parent_post_id': parentObjectID,
					    'user_id':        chumly_vars.user_id
				    }
			    });
			
			
		    },
		    updateLatestCourse = function () {
			
			    console.log('Update Course', objectID);
			
			    $.ajax({
				    url:  chumly_vars.ajax_url,
				    type: 'POST',
				    data: {
					    'action':    'chumly_elearning_update_course_activity',
					    'course_id': objectID,
					    'user_id':   chumly_vars.user_id
				    }
			    });
			
		    };
		
		init();
		
	};
	
	
}(jQuery));

/*------------------------------------*\
    CENTRAL APP MASTER 
    
    This file includes the module placeholders system that allows modular 
    binding of custom methods / plugins etc. 
    
    EXAMPLE
    
    <div data-module="example1,example2"></div> 
    
    The above would meet two conditions in the below switch statement.
    
\*------------------------------------*/

(function($) {
	
	var app = (function($) {
		
		// This method will run when the DOM is ready. 
		var init = function() {
			
			// Find any module placeholders 
			var modulePlaceholders = $("[data-module]"); 
			
			if(modulePlaceholders.any()) {
				
				// Loop each placeholder
				modulePlaceholders.each(function() {
					
					var elem = $(this),
						modules = elem.attr("data-module");
					
					// If any modules found	
					if(modules) {
						
						// Split on the comma 
						modules = modules.split(",");
						
						// Loop each module key
						$.each(modules, function(i, module) {
							
							// Run switch to bind each module to each key
							switch(module) {
								
								case "toggle":
									elem.toggle();
									break;
									
								case "tabs":
									elem.tabs();
									break;
								
								case "update-user-activity":
									elem.chumlyUpdateActivity();
									break;
								
								case "course-actions":
									console.log('Run Course Actions');
									elem.chumlyCourseActions();
									break;

								case "taxonomy-row-builder":
									console.log('Row Builder');
									elem.chumlyTaxonomyRowBuilder();
									break;
							}
							
						});
					}
				});
			}
		};
		
		return {
			init: init
		}
		
	}(jQuery));

	// Run the app on doc ready
	$(document).ready(function() {
		app.init();
	});
	
}(jQuery));
//# sourceMappingURL=chumly-elearning.js.map
