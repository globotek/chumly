jQuery(document).ready(function($){
	$.fn.gtekVid = function(){
		// Iterate over each video player on a page and format.
		return this.each(function(){
			var gtekVid = $(this);
			
			console.log(gtekVid);
			
			// Remove Standard Controls.
			gtekVid.removeAttr('controls').addClass('gtek-video-canvas');
			
			// Create video player HTML.
			// Video player wrapper.
			var gtekVid_wrapper = $('<div></div>').addClass('gtek-video-player');
			
			// Set initial video state attribute.
			gtekVid.attr('state', 'paused');

			// Video controls.
			/*jshint multistr: true */
			var gtekVid_controls = $('\
				<div class="gtek-video-controls">\
					<div class="gtek-video-seek-container">\
						<div class="gtek-video-seek"></div>\
					</div>\
					<p class="gtek-video-play" title="Play/Pause">\
						<span class="gtek-video-play-icon fa fa-play"></span>\
						<span class="gtek-video-pause-icon fa fa-pause"></span>\
					</p>\
					<div class="gtek-video-timer">00:00</div>\
					<div class="gtek-video-duration"> / 00:00</div>\
					<p class="gtek-video-fullscreen fa fa-expand" title="Fullscreen"></p>\
					<div class="gtek-video-audio">\
						<p class="gtek-video-mute fa fa-volume-up" title="Mute/Unmute"></p>\
						<div class="gtek-video-volume-padding">\
							<div class="gtek-video-volume"></div>\
						</div>\
					</div>\
				</div>\
				');
		
			gtekVid.wrap(gtekVid_wrapper);
			gtekVid.after(gtekVid_controls);
		
			// Add our listeners to each video's respective controls.
			var video_container = gtekVid.parent('.gtek-video-player'),
				video_canvas = $('.gtek-video-canvas'),
				video_controls = $('.gtek-video-controls', video_container),
				video_play = $('.gtek-video-play', video_container),
				video_seek = video_controls.find('.gtek-video-seek'),
				video_timer = video_controls.find('.gtek-video-timer'),
				video_duration = video_controls.find('.gtek-video-duration'),
				//video_audio = video_controls.find('.gtek-video-audio'),
				video_volume = video_controls.find('.gtek-video-volume'),
				video_mute = video_controls.find('.gtek-video-mute'),
				video_fullscreen = video_controls.find('.gtek-video-fullscreen'),
				controls_width = video_canvas.outerWidth();
			
			
			// Size and position control elements.
			//var controls_height = Math.ceil((video_canvas.height() / 100) * 14);
			video_controls.css({'width': controls_width + 'px'/*,'height': controls_height + 'px', 'min-height': '60px'*/});
			$('.gtek-video-seek-background').css('right', (controls_width - 2) + 'px');
			
			// Show & hide controls on video hover.
			gtekVid.add(video_controls).on('hover', function(){
				video_controls.show();
			});
			
			gtekVid.add(video_controls).on('mouseleave', function(){
				if(gtekVid.attr('state') === 'playing'){
					video_controls.hide();
				}
			});
			
			
			// Play, pause & video state function. Initial state set on Line 24.
			function gtekPlay(){
				if(gtekVid.attr('state') === 'paused' || gtekVid.attr('state') === 'ended'){
					gtekVid[0].play();
					gtekVid.attr('state', 'playing');
					$('.gtek-video-play-icon').hide();
					$('.gtek-video-pause-icon').show();
				} else {
					gtekVid[0].pause();
					gtekVid.attr('state', 'paused');
					$('.gtek-video-play-icon').show();
					$('.gtek-video-pause-icon').hide();
				}
			
				// Add ended state when video ends.
				gtekVid.bind('ended', function(){
					gtekVid.attr('state', 'ended');
					$('.gtek-video-play-icon').show();
					$('.gtek-video-pause-icon').hide();
				});
			}
			
			video_play.on('click', gtekPlay);
			gtekVid.on('click', gtekPlay);
			
			// Timer functionality.
			// Current timestamp.
			window.setInterval(function(){
				if(gtekVid.attr('state') === 'playing'){
					var current_seconds = Math.floor(gtekVid.get(0).currentTime);
					
					var current_hours = Math.floor(current_seconds / 3600);
					current_seconds -= current_hours * 3600;
					
					var current_minutes = Math.floor(current_seconds/60);
					current_seconds -= current_minutes * 60;
					
					var video_time = (current_minutes < 10 ? '0' + current_minutes : current_minutes) + ':' + (current_seconds < 10 ? '0' + current_seconds : current_seconds);
					//console.log(video_time);
					
					//video_controls.find('.gtek-video-seek').slider({value: current_seconds});
					var gtek_handle = video_seek.find('.ui-slider-handle');
					
					if(!gtek_handle.hasClass('ui-state-hover') && !gtek_handle.hasClass('ui-state-active') && !video_seek.is(':hover')){
						video_timer.text(video_time);
					}
				}
			}, 100);
			
			// Video Duration.
			var gtekDuration = window.setInterval(function(){
				if(gtekVid.get(0).readyState === 4){
					var duration_seconds = Math.floor(gtekVid.get(0).duration);
					
					var duration_hours = Math.floor(duration_seconds / 3600);
					duration_seconds -= duration_hours * 3600;
					
					var duration_minutes = Math.floor(duration_seconds/60);
					duration_seconds -= duration_minutes * 60;
					
					var duration_total = (duration_minutes < 10 ? '0' + duration_minutes : duration_minutes) + ':' + (duration_seconds < 10 ? '0' + duration_seconds : duration_seconds);
					
					video_duration.text('/' + duration_total);
					console.log(' / ' + duration_total);
					clearInterval(gtekDuration);
				}
			}, 100);

			
			// Video seek control.
			var gtekSeek = window.setInterval(function(){
				if(gtekVid.get(0).readyState === 4){
					clearInterval(gtekSeek);
					var gtek_duration = Math.floor(gtekVid.get(0).duration);

					video_seek.slider({
						animate: true,
						max: gtek_duration,
						min: 0,
						orientation: 'horizontal',
						range: 'min',
						step: 0.0001,
						value: 0,
						slide: function(event, ui){
							seeksliding: true;
							var current_seconds = Math.floor(ui.value);
							
							var current_hours = Math.floor(current_seconds / 3600);
							current_seconds -= current_hours * 3600;
							
							var current_minutes = Math.floor(current_seconds/60);
							current_seconds -= current_minutes * 60;
							
							var video_time = (current_minutes < 10 ? '0' + current_minutes : current_minutes) + ':' + (current_seconds < 10 ? '0' + current_seconds : current_seconds);

							video_timer.text(video_time);
				
							var left_position = $('.gtek-video-seek .ui-slider-handle').attr('style'),
								get_position_digits = left_position.replace('left: ', '').replace('%;', ''),
								right_position = (100 - get_position_digits);
							
							console.log(right_position);
			
							$('.gtek-video-seek-background').css({'right': right_position + '%'});
						},
						stop: function(event, ui){
							seeksliding = false;
							gtekVid.get(0).currentTime = ui.value;
						}
					});
				}
			}, 100);
			
			
			// Move seek bar as video plays.
			var gtekUpdateSeek = function() {
				var gtek_time = gtekVid.get(0).currentTime;
				var gtek_handle = video_seek.find('.ui-slider-handle');
				if(!gtek_handle.hasClass('ui-state-hover') && !gtek_handle.hasClass('ui-state-active') && !video_seek.is(':hover')){
					video_seek.slider('value', gtek_time);
				}
				
				var left_position = $('.gtek-video-seek .ui-slider-handle').attr('style'),
					get_position_digits = left_position.replace('left: ', '').replace('%;', ''),
					right_position = (100 - get_position_digits);
				
				console.log(right_position);

				$('.gtek-video-seek-background').css({'right': right_position + '%'});
			};

			gtekVid.bind('timeupdate', gtekUpdateSeek);
			
			
			// Video volume control.
			video_volume.slider({
				value: 1,
				orientation: 'vertical',
				range: 'max',
				max: 1,
				step: 0.05,
				animate: true,
				slide: function(e, ui){
					gtekVid.get(0).mute = false;
					volume_level = ui.value;
					gtekVid.get(0).volume = ui.value;
				}
			});

			
			// Video mute.
			video_mute.on('click', function(){
				if(gtekVid.prop('muted') === false){
					$(this).removeClass('fa-volume-up').addClass('fa-volume-off');
					gtekVid.prop('muted', true).attr('muted', true);
					video_volume.slider('value', '0');
				} else {
					$(this).removeClass('fa-volume-off').addClass('fa-volume-up');
					gtekVid.prop('muted', false).attr('muted', false);
					video_volume.slider('value', gtekVid.get(0).volume);
				}
			});
			
			$('.gtek-video-volume-padding, .gtek-video-audio .gtek-video-volume .ui-slider-handle, .gtek-video-mute').on('hover', function(){
				$('.gtek-video-volume-padding, .gtek-video-audio .gtek-video-volume .ui-slider-handle, .gtek-video-mute').show();
			});
			
			$('.gtek-video-volume-padding, .gtek-video-mute').on('mouseleave', function(){
				$('.gtek-video-volume-padding, .gtek-video-audio .gtek-video-volume .ui-slider-handle').hide();
			});
						
			
			// Video Fullscreen.
			function test(){
				console.log('fullscreen change');
				video_controls.css({'width': '100%', 'margin-bottom': '0', 'margin-left': '0'});
				video_fullscreen.removeClass('fullscreen fa-compress').addClass('fa-expand');
			}
			
			document.addEventListener('fullscreenchange', test);
			document.addEventListener('mozfullscreenchange', test);
			document.addEventListener('MSFullscreenChange', test);
			
			video_fullscreen.on('click', function(){
				if(video_fullscreen.hasClass('fullscreen')){
					
					if(gtekVid.get(0).requestFullscreen){
						document.webkitRequestFullscreen();
					} else if(gtekVid.get(0).mozRequestFullScreen){
						document.mozCancelFullScreen(); // Firefox
					} else if(gtekVid.get(0).webkitRequestFullscreen){
						document.webkitExitFullscreen(); // Chrome and Safari
					} else if(gtekVid.get(0).webkitExitFullscreen){
						gtekVid.get(0).webkitExitFullscreen(); // iOS
					}
					video_fullscreen.removeClass('fullscreen fa-compress').addClass('fa-expand');
					
					console.log('exit fullscreen');
					
				} else {
					
					if(gtekVid.get(0).requestFullscreen){
						gtekVid.get(0).requestFullscreen();
					} else if(gtekVid.get(0).mozRequestFullScreen){
						gtekVid.get(0).mozRequestFullScreen(); // Firefox
					} else if(gtekVid.get(0).webkitRequestFullscreen){
						gtekVid.get(0).webkitRequestFullscreen(); // Chrome and Safari
					} else if(gtekVid.get(0).webkitEnterFullscreen){
						gtekVid.get(0).webkitEnterFullscreen(); // iOS
					} else if(gtekVid.get(0).msRequestFullscreen){
						gtekVid.get(0).msRequestFullscreen(); // IE
					}
					video_fullscreen.addClass('fullscreen fa-compress').removeClass('fa-expand');
					video_controls.css({'width': '100%', 'margin-bottom': '-4px', 'margin-left': '0'});

					/*var viewport_width = $(window).width(),
						fullscreen_controls_width = (viewport_width / 3) * 2,
						fullscreen_controls_margin = (viewport_width - fullscreen_controls_width) / 2;
					console.log(viewport_width);
					console.log(fullscreen_controls_width);
					console.log(fullscreen_controls_margin);
					
					video_controls.css({'width': fullscreen_controls_width + 'px', 'margin-bottom': '25px', 'margin-left': fullscreen_controls_margin + 'px'});*/
				}
				
			});
			
		}); // Iterate over videos function close.
	}; // gtekVid function close.
}); // Document Ready function close.


