<?php
/**
 * Created by PhpStorm.
 * User: matthew
 * Date: 9/8/18
 * Time: 8:04 PM
 */
function chumly_output_elearning_builder_row( $item, $i ) { ?>
		
	<li class="elearning-builder-row" <?php _e($item == NULL ? 'style="display: none;"' : ''); ?>>
		
		<input type="hidden" name="chumly-elearning-linked-children[<?php echo $i; ?>][ID]" value="<?php echo $item['ID']; ?>" class="elearning-builder-id" />
		<input type="hidden" name="chumly-elearning-linked-children[<?php echo $i; ?>][name]" value="<?php echo $item['name']; ?>" class="elearning-builder-name" />
<!--		<input type="hidden" name="chumly-elearning-linked-children[--><?php //echo $i; ?><!--][index]" value="--><?php //echo $item['index']; ?><!--" class="elearning-builder-index" />-->
		
		<div class="elearning-builder-column elearning-builder-column-order"><span class="circle elearning-builder-order"><?php echo $i++; ?></span></div>
		
		<div class="elearning-builder-column elearning-builder-placeholder">
			<p><?php echo stripslashes_deep( $item[ 'name' ] ); ?></p>
		</div>
		
		<div class="elearning-builder-column">
<!--			<a href="#edit">Edit</a>-->
			<a href="#delete" style="color:#a00">Delete</a>
		</div>
		
	</li>
	
	<?php
	
}