<?php
function chumly_elearning_post_metaboxes() {
	global $post;
	add_meta_box( 'postimagediv', __( 'Featured Image' ), 'post_thumbnail_meta_box', NULL, 'side', 'low' );
	add_meta_box( 'post-format-meta', 'Lesson Module', 'create_lesson_module_meta_box', 'chumly_lesson', 'side', 'high' );
	add_meta_box( 'lesson-points-meta', 'Lesson Points', 'create_lesson_points_meta_box', 'chumly_lesson', 'side', 'core' );
	add_meta_box( 'post-selection-meta', 'Related Lessons', 'create_post_selection_meta_box', 'chumly_lesson', 'normal', 'core' );
	
	if ( isset( $_GET[ 'action' ] ) ) {
		$is_quiz = get_post_meta( $post->ID, '_lesson_module', TRUE );
		if ( $is_quiz == 'quiz' ) {
		}
	}
	
	if ( isset( $_GET[ 'action' ] ) ) {
		$is_video = get_post_meta( $post->ID, '_lesson_module', TRUE );
		if ( $is_video == 'watch' ) {
		}
	}
	
	add_meta_box( 'quiz-meta', 'Quiz', 'create_quiz_meta_box', 'chumly_lesson', 'normal', 'high' );
	add_meta_box( 'video-meta', 'Video', 'create_video_meta_box', 'chumly_lesson', 'normal', 'high' );
	
}

add_action( 'add_meta_boxes', 'chumly_elearning_post_metaboxes' );


foreach ( glob( dirname( __FILE__ ) . '/*.php' ) as $file ) {
	if ( $file != dirname( __FILE__ ) . '/metabox-controller.php' ) {
		require_once( $file );
	}
}
