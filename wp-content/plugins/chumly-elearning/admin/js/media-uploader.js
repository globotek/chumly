/**
 * Created by matthew on 5/7/18.
 */
jQuery(document).ready(function ($) {
	// Set control variable.
	var image_uploader;
	
	$('#term_cover_media').click(function (event) {
		event.preventDefault();
		
		// If the media frame already exists, reopen it.
		if (image_uploader) {
			image_uploader.open();
			return;
		}
		
		// Create the media frame.
		image_uploader = wp.media.frames.image_uploader = wp.media({
			title: $(this).data('uploader_title'),
			button: {
				text: $(this).data('uploader_button_text'),
			},
			multiple: false  // Set to true to allow multiple files to be selected
		});
		
		// When an image is selected, run a callback.
		image_uploader.on('select', function () {
			// We set multiple to false so only get one image from the uploader
			attachment = image_uploader.state().get('selection').first().toJSON();
			
			// Do something with attachment.id and/or attachment.url here
			$('#term_cover_media').val(attachment.url);
		});
		
		// Finally, open the modal
		image_uploader.open();
	});
	
/*
	// Set control variable.
	var video_uploader;
	
	$('#term_intro_video').click(function (event) {
		event.preventDefault();
		
		// If the media frame already exists, reopen it.
		if (video_uploader) {
			video_uploader.open();
			return;
		}
		
		// Create the media frame.
		video_uploader = wp.media.frames.video_uploader = wp.media({
			title: $(this).data('uploader_title'),
			button: {
				text: $(this).data('uploader_button_text'),
			},
			multiple: false  // Set to true to allow multiple files to be selected
		});
		
		// When an image is selected, run a callback.
		video_uploader.on('select', function () {
			// We set multiple to false so only get one image from the uploader
			attachment = video_uploader.state().get('selection').first().toJSON();
			
			// Do something with attachment.id and/or attachment.url here
			$('#term_intro_video').val(attachment.url);
		});
		
		// Finally, open the modal
		video_uploader.open();

	});
 */
});