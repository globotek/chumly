jQuery(document).ready(function ($) {
		
	$('.nav-tab').on('click', function () {
		
		if (!$(this).hasClass('nav-tab-active')) {
			$('.nav-tab.nav-tab-active').removeClass('nav-tab-active');
			$(this).addClass('nav-tab-active');
		}
		
		if ($(this).hasClass('multiple-choice-tab')) {
			$('.multiple-choice-wrapper').show();
			$('.fill-blank-wrapper').hide();
		}
		
		if ($(this).hasClass('fill-blank-tab')) {
			$('.fill-blank-wrapper').show();
			$('.multiple-choice-wrapper').hide();
		}
		
		if ($(this).hasClass('summary-tab')) {
			$('.summary-wrapper').show();
			$('.transcript-wrapper').hide();
			$('.extras-wrapper').hide();
		}
		
		if ($(this).hasClass('transcript-tab')) {
			$('.summary-wrapper').hide();
			$('.transcript-wrapper').show();
			$('.extras-wrapper').hide();
		}
		
		if ($(this).hasClass('extras-tab')) {
			$('.summary-wrapper').hide();
			$('.transcript-wrapper').hide();
			$('.extras-wrapper').show();
		}
		
	});
	
});
