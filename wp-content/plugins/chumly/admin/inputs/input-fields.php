<?php
global $wpdb;
$rows = $wpdb->get_results( "
	SELECT * FROM " . $wpdb->prefix . "chumly_inputs
	WHERE input_location = '" . $input_location . "'
	AND input_group = '" . esc_attr( $_GET['input_group'] ) . "'
	AND user_type = '" . esc_attr( $_GET['user_type'] ) . "'
	ORDER BY input_order ASC"
);

$i = 0; ?>
	
	<ul class="inputs-list">
		
		<?php
		$total_rows           = count( $rows );
		$input_location_index = ( chumly_get_option( $input_location . '_index' ) );
		
		echo '<input type="hidden" id="field_count" name="field_count" value="' . $total_rows . '">';
		echo '<input type="hidden" id="input_group" name="input_group" value="' . $_GET['input_group'] . '">';
		echo '<input type="hidden" id="user_type" name="user_type" value="' . $_GET['user_type'] . '">';
		echo '<input type="hidden" id="location_index" name="location_index" value="' . $input_location_index . '">';
				
		if ( $rows ) {
			
			foreach ( $rows as $row ) {
				
				$i ++;
				output_field_row( $row, $i, $total_rows, $input_location );
				
			}
			
		} ?>
		
<!--		<div class="new_row_anchor"></div>-->
	
	</ul>

<?php if ( $input_location != 'required' ) { ?>
	<li class="inputs-row">
		<input type="button" class="button-primary add-field-row" value="+ Add Row"/>
	</li>
<?php }

