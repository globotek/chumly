<?php
function chumly_field( $field, $show_label = FALSE ) {
	
	$function = 'chumly_view_' . $field->field_type . '_field';
	
	if ( function_exists( $function ) ) {
		
		$function( $field, $show_label );
		
	} else {
		
		echo '<strong>Error - </strong> <i>Define a view function for the ' . $field->field_type . ' field type</i>';
		
	}
	
}


function chumly_update_field( $user_ID, $input_id, $field_data, $profile_data = array() ) {
	
	if ( empty( $profile_data ) ) {
		$profile_data = chumly_get_profile( array( 'user_id' => $user_ID ) );
	}
	
	if ( ! is_array( $field_data ) ) {
		
		$field_data = array( 'value' => $field_data );
		
	}
	
	$input = chumly_get_input( array( 'id' => $input_id ) );
	//var_dump( $field_data );
	//var_dump($input_id);
	if ( $input ) {
		
		/** Validate and sanitize the input values using a prepare function. */
		$data         = array( 'input' => $input, 'value' => $field_data, 'user_id' => $user_ID );
		
		$value        = apply_filters( 'chumly_process_' . $input->input_type . '_field', $data );
		
		$profile_data->$input_id->field_id   = $input_id;
		$profile_data->$input_id->field_name = $input->input_name;
		$profile_data->$input_id->label      = $input->input_label;
		$profile_data->$input_id->value      = $value;
		$profile_data->$input_id->placement  = $input->input_placement;
		$profile_data->$input_id->field_type = $input->input_type;
		//var_dump( $profile_data );
		update_user_meta( $user_ID, $input_id, $value );
		
	}
	
	update_user_meta( $user_ID, 'profile_fields', chumly_serialize( $profile_data ) );
	
	return $profile_data;
	
}


function chumly_get_avatar( $user_id = NULL, $source = 'profile', $class = NULL, $echo = FALSE ) {
	
	return chumly_avatar( $user_id, $source, $class, FALSE );
	
}


function chumly_avatar( $user_id = NULL, $source = 'profile', $class = NULL, $echo = TRUE ) {
	/**
	 * $echo TRUE = Return an HTML element.
	 * $echo FALSE = Return the URL to the image.
	 */
	if ( $user_id == NULL ) {
		$user_id = chumly_explode_url()->ID;
	}
	
	if ( ! $user_id ) {
		$user_id = get_current_user_id();
	}
	
	$class = 'avatar__image ' . $class;
	
	
	do_action( 'chumly_avatar_conditionals', $source );
	
	if ( $source == 'profile' ) {
		
		$image_url = wp_get_attachment_url( get_user_meta( $user_id, '_avatar_attachment_id', TRUE ) );
		
		if ( ! $image_url ) {
			$upload_dir = wp_upload_dir();
			$image_url  = $upload_dir[ 'baseurl' ] . '/chumly/profile-avatars/default_placeholder.png';
		}
		
		if ( $echo === FALSE ) {
			
			return $image_url;
			
		} elseif ( $echo === TRUE ) {
			
			$user_data = get_userdata( $user_id );
			echo '<img class="' . $class . '" src="' . $image_url . '" alt="Avatar of ' . $user_data->first_name . ' ' . $user_data->last_name . '" title="' . $user_data->first_name . ' ' . $user_data->last_name . '"" />';
			
		}
	}
	
	
	if ( $source == 'group' ) {
		
		$group_post_id = ( new Chumly_Groups )->get_linked_post_id( $user_id );
		
		$image_url = wp_get_attachment_url( get_post_meta( $group_post_id, '_thumbnail_id', TRUE ) );
		
		if ( ! $image_url ) {
			$upload_dir = wp_upload_dir();
			$image_url  = $upload_dir[ 'baseurl' ] . '/chumly/group-avatars/default_placeholder.png';
		}
		
		
		if ( $echo === FALSE ) {
			
			return $image_url;
			
		} elseif ( $echo === TRUE ) {
			
			echo '<img class="' . $class . '" src="' . $image_url . '" alt="" />';
			
		}
		
	}
	
	
	if ( filter_var( $source, FILTER_VALIDATE_URL ) ) {
		
		return $source;
		
	}
	
}
