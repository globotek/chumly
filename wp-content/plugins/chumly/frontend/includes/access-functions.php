<?php
function chumly_check_privacy( $target_id, $methods = array() ) {
	
	$privacy = new Chumly_Privacy( $target_id, $methods );
	
	return $privacy->check_privacy();
	
}


function chumly_registration_form( $atts = array() ) {
	
	if ( isset( $_GET[ 'registered' ] ) ) {
		
		echo '<p>Thanks for registering. You may now login.</p>';
		chumly_login_form();
		
	} else {
		
		chumly_form_header( 'register', NULL );
		
		$registration = chumly_register_profile( $atts );
		
		chumly_form_footer( 'register_profile', NULL, 'Register', $registration );
		
	}
	
}


function chumly_check_login( $force_redirect = FALSE ) {
	
	if ( ! is_user_logged_in() ) {
		
		$public_pages   = unserialize( get_option( 'chumly_settings' ) )[ 'visibility' ][ 'public_pages' ];
		$server_request = explode( '?', $_SERVER[ 'REQUEST_URI' ] );
		$requested_url  = trim( home_url() . $server_request[ 0 ], '/' );
		$login_url      = trim( home_url( '/' ) . chumly_get_option( 'login_page' ), '/' );
		$reset_url      = untrailingslashit( chumly_password_reset_url() );
		$new_pass_url   = untrailingslashit( chumly_new_password_url() );
		
		if ( $requested_url != $login_url && $requested_url != $reset_url && $requested_url != $new_pass_url && ! in_array( get_the_ID(), $public_pages ) && ! is_admin() || $force_redirect ) {
			
			wp_redirect( $login_url . '?requested_url=' . $requested_url );
			
			exit;
			
		}
		
	} else {
		
		return TRUE;
		
	}
	
}


function chumly_login_form( $args = array() ) {
	
	$defaults = array(
		'redirect' => 'self'
	);
	
	$args = wp_parse_args( $args, $defaults );
	
	switch ( $args[ 'redirect' ] ) {
		case 'home' :
			$redirect = home_url();
			break;
		case 'self' :
			$redirect = ( isset( $_SERVER[ 'HTTPS' ] ) && $_SERVER[ 'HTTPS' ] !== 'off' ? 'https' : 'http' ) . '://' . $_SERVER[ 'HTTP_HOST' ] . $_SERVER[ 'REQUEST_URI' ];
			break;
		case 'profile' :
			$redirect = home_url() . '/profile';
			break;
		default :
			$redirect = $args[ 'redirect' ];
	}
	
	chumly_format_form_data();
	
	if ( isset( $_POST[ 'login_submit' ] ) ) {
		chumly_process_login( $redirect );
	}
	
	chumly_form_header();
	
	chumly_input( chumly_get_input( array(
		'name' => 'username'
	) ) );
	
	chumly_input( chumly_get_input( array(
		'name' => 'password_one'
	) ) );
	
	echo '<a class="breathe" href="' . get_the_permalink( chumly_get_option( 'reset_password_page_id' ) ) . '">Reset Password</a>';
	
	chumly_form_footer( 'login_submit', NULL, 'Login' );
	
}


function chumly_process_login( $redirect ) {
	
	if ( isset( $_POST[ 'login_submit' ] ) ) {
		
		$credentials = array(
			'user_login'    => esc_attr( $_POST[ 'username' ] ),
			'user_password' => esc_attr( $_POST[ 'password_one' ] )
		);
		
		$login = wp_signon( $credentials, TRUE );
		
		if ( is_wp_error( $login ) ) {
			
			foreach ( $login as $errors ) {
				
				foreach ( $errors as $error_type ) {
					
					foreach ( $error_type as $error ) {
						
						chumly_alert( 'error', array( $error ) );
						
						return;
						
					}
					
				}
				
			}
			
		} else {
			
			wp_redirect( $redirect . '?logged_in=true' );
			exit;
			
		}
		
		/*		if(isset($_GET['email_invite'])){
					
					if(isset($_GET['group_id'])){
						wp_redirect(home_url('/'));
						exit;
					}
					
				}*/
	}
}


function chumly_logout() {
	
	if ( is_user_logged_in() ) {
		
		wp_redirect( ( isset( $_SERVER[ 'HTTPS' ] ) && $_SERVER[ 'HTTPS' ] !== 'off' ? 'https' : 'http' ) . '://' . $_SERVER[ 'HTTP_HOST' ] . $_SERVER[ 'REQUEST_URI' ] . '?logged_out=true' );
		
	}
	
	if ( ! is_user_logged_in() ) {
		
		if ( isset( $_GET[ 'logged_out' ] ) ) {
			echo chumly_alert( 'standard', array( 'You Are Now Logged Out. <a href="' . home_url() . '">Return Home?</a>' ) );
		}
		echo '<p>You\'re currently logged out, would you like to log in?</p><br>';
		chumly_login_form();
		
	} else {
		
		echo chumly_alert( 'error', array( 'An issue has occurred, you\'re still logged in.' ) );
		
	}
	
}


function chumly_login_alert( $message = '' ) {
	
	if ( ! empty( $message ) ) {
		
		echo chumly_alert( 'success', array( 'Password successfully updated.' ) );
		
		return TRUE;
	}
	
	if ( isset( $_GET[ 'logged_in' ] ) ) {
		
		echo chumly_alert( 'success', array( 'You are now logged in.' ) );
		
	}
	
}


function chumly_user_blocks() {
	
	global $chumly_user;
	
	if ( $chumly_user->dashboard_access == FALSE ) {
		//echo 'No Access';
	}
	
	if ( $chumly_user->admin_approval == FALSE ) {
		//echo 'Need Approving';
	}
	
}

add_action( 'wp_head', 'chumly_user_blocks' );


function chumly_password_reset_url() {
	
	return get_the_permalink( chumly_get_option( 'reset_password_page_id' ) );
	
}

add_filter( 'lostpassword_url', 'chumly_password_reset_url' );


function chumly_new_password_url() {
	
	return get_the_permalink( chumly_get_option( 'new_password_page_id' ) );
	
}


function chumly_login_url() {
	
	return home_url( '/' ) . chumly_get_option( 'login_page' );
	
}


function chumly_registration_url() {
	
	return home_url( chumly_get_option( 'registration_page' ) );
	
}


function chumly_send_password_reset_email( $user_id ) {
	
	$user       = get_user_by( 'email', $user_id );
	$firstname  = $user->first_name;
	$email      = $user->user_email;
	$key        = get_password_reset_key( $user );
	$user_login = $user->user_login;
	$rp_link    = '<a href="' . site_url( "wp-login.php?action=rp&key=$key&login=" . rawurlencode( $user_login ), 'login' ) . '">Reset Password</a>';
	
	
	$message = "Hi " . $firstname . ",<br><br>";
	$message .= "A request to change your password on " . get_bloginfo( 'name' ) . " for email address " . $email . " has been submitted.<br>";
	$message .= "Click here to set the password for your account: <br><br>";
	$message .= $rp_link . '<br>';
	
	
	$subject = __( 'Reset password for your account on ' . get_bloginfo( 'name' ) );
	$headers = array();
	
	add_filter( 'wp_mail_content_type', function ( $content_type ) {
		return 'text/html';
	} );
	
	$headers[] = 'From: ' . get_bloginfo( 'name' ) . ' <' . get_bloginfo( 'admin_email' ) . ' >\r\n';
	
	$email = wp_mail( $email, $subject, $message, $headers );
	
	// Reset content-type to avoid conflicts -- http://core.trac.wordpress.org/ticket/23578
	remove_filter( 'wp_mail_content_type', 'set_html_content_type' );
	
	return $email;
	
}