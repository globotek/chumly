<?php
function chumly_user_id( $url = NULL ) {
	
	if ( $url == NULL ) {
		global $wp;
		
		$url = home_url( $wp->request );
	}
	
	return chumly_explode_url( $url )->ID;
	
}


function chumly_get_user_id( $name ) {
	
	$user_id = chumly_get_user( array( 'name' => $name ) )->ID;
	
	return $user_id;
	
}


function chumly_get_role( $user_id ) {
	
	$user_role = get_user_meta( $user_id, '_chumly_user_role', TRUE );
	
	if ( ! $user_role ) {
		
		$user_role = 'default';
		
	}
	
	return $user_role;
	
}


/*
 * $parts = 'first', 'last', 'full'
 */
function chumly_username( $user_id = NULL, $parts = 'full' ) {
	
	if ( $user_id == NULL ) {
		$user_id = get_current_user_id();
	}
	
	$user_first_name = chumly_get_profile_field( 'first_name', 'name', $user_id )->value;
	$user_last_name  = chumly_get_profile_field( 'last_name', 'name', $user_id )->value;
	
	//$user_first_name = get_user_meta(get_current_user_id(), 'required_3', TRUE);
	//$user_last_name = get_user_meta(get_current_user_id(), 'required_4', TRUE);
	
	switch ( $parts ) {
		
		case 'first':
			
			if ( $user_first_name ) {
				
				return $user_first_name;
				
			} else {
				
				return get_userdata( $user_id )->first_name;
				
			}
		
		case 'last':
			
			if ( $user_last_name ) {
				
				return $user_last_name;
				
			} else {
				
				return get_userdata( $user_id )->last_name;
				
			}
		
		case 'full':
		default:
			
			if ( $user_first_name && $user_last_name ) {
				
				return $user_first_name . ' ' . $user_last_name;
				
			} else {
				
				$user_data = get_userdata( $user_id );
				
				return $user_data->first_name . ' ' . $user_data->last_name;
				
			}
	}
	
	
}


function chumly_get_user( $args = array() ) {
	
	if ( $args[ 'name' ] ) {
		
		$user_name = explode( ' ', $args[ 'name' ] );
		
		$user = get_users(
			array(
				'meta_query' => array(
					array(
						'key'     => 'first_name',
						'value'   => $user_name[ 0 ],
						'compare' => '=='
					),
					array(
						'key'     => 'last_name',
						'value'   => $user_name[ 1 ],
						'compare' => '=='
					)
				)
			)
		);
		
		return $user[ 0 ];
		
	} elseif ( $args[ 'id' ] ) {
		
		$user = get_userdata( $args[ 'id' ] );
		
		return $user;
		
	} else {
		
		$user = get_userdata( get_current_user_id() );
		
		return $user;
	}
	
}


function chumly_prepare_required_fields( $args = array() ) {
	
	$inputs   = chumly_get_input_group( $args );
	$userdata = array();
	
	foreach ( $inputs as $input ) {
		
		switch ( $input->input_name ) {
			
			case 'username':
				
				$userdata[ 'user_login' ] = $input->input_id;
				break;
			
			case 'password_one':
				
				$userdata[ 'password' ] = $input->input_id;
				break;
			
			case 'password_two':
				
				$userdata[ 'user_pass' ] = $input->input_id;
				break;
			
			case 'user_email':
				
				$userdata[ 'user_email' ] = $input->input_id;
				break;
			
			case 'first_name':
				
				$userdata[ 'first_name' ] = $input->input_id;
				break;
			
			case 'last_name':
				
				$userdata[ 'last_name' ] = $input->input_id;
				break;
			
		}
		
	}
	
	return $userdata;
	
}


function chumly_create_user( $args = array() ) {
	
	$inputs = chumly_prepare_required_fields( array( 'group' => $_POST[ 'meta_data' ][ '_chumly_user_role' ] ) );
	//var_dump( $inputs );
	$defaults = array(
		'user_login'   => $_POST[ $inputs[ 'user_login' ] ][ 'value' ],
		'user_pass'    => $_POST[ $inputs[ 'user_pass' ] ][ 'value' ],
		'user_email'   => $_POST[ $inputs[ 'user_email' ] ][ 'value' ],
		'first_name'   => $_POST[ $inputs[ 'first_name' ] ][ 'value' ],
		'last_name'    => $_POST[ $inputs[ 'last_name' ] ][ 'value' ],
		'display_name' => $_POST[ $inputs[ 'first_name' ] ][ 'value' ] . ' ' . $_POST[ $inputs[ 'last_name' ] ][ 'value' ],
		'role'         => $_POST[ 'signup_data' ][ 'wp_user_role' ]
	);
	
	$userdata = wp_parse_args( $args, $defaults );
	
	//var_dump( $userdata );
	//var_dump($_POST);
	$new_user_ID = wp_insert_user( $userdata );
	
	if ( is_wp_error( $new_user_ID ) ) {
		
		return $new_user_ID;
		
	} else {
		
		if ( ! empty( $_POST[ 'meta_data' ] ) ) {
			
			foreach ( $_POST[ 'meta_data' ] as $meta_key => $meta_value ) {
				update_user_meta( $new_user_ID, $meta_key, $meta_value );
			}
			
		}
		
		chumly_create_media_bucket( $new_user_ID );
		
		chumly_update_profile( $new_user_ID );
		
		wp_new_user_notification( $new_user_ID );
		
		$message_body = '<h3>Welcome to ' . get_bloginfo( 'name' ) . '</h3>';
		new Chumly_Email( array(
			array(
				'id'    => $new_user_ID,
				'email' => $userdata[ 'user_email' ]
			)
		), 'Thank you for signing up to ' . get_bloginfo( 'name' ), 'registration-confirmation', $message_body );
		
		do_action( 'chumly_after_new_user_email', $new_user_ID, $userdata );
		
		return $new_user_ID;
		
	}
	
}


function chumly_create_media_bucket( $user_id ) {
	
	$user_media_post = wp_insert_post( array(
		'post_author' => $user_id,
		'post_title'  => $user_id,
		'post_status' => 'private',
		'post_type'   => 'chumly_user_media',
		'meta_input'  => array(
			'user_id' => $user_id
		)
	) );
	
	if ( ! is_wp_error( $user_media_post ) ) {
		update_user_meta( $user_id, '_media_post', $user_media_post );
	}
	
	return $user_media_post;
	
}


function chumly_get_user_roles() {
	
	global $wpdb;
	
	return $wpdb->get_results( "SELECT user_role FROM " . $wpdb->prefix . "chumly_input_groups WHERE user_type = 'user'", ARRAY_A );
	
}