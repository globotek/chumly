<?php do_action( 'chumly_before_content' ); ?>

<?php if ( is_user_logged_in() && ! current_user_can( 'manage_options' ) ) { ?>
	
	<?php wp_redirect( chumly_profile_url() ); ?>

<?php } else { ?>
	
	<?php if ( isset( $_GET[ 'requested_url' ] ) ) { ?>
		
		<?php $redirect = $_GET[ 'requested_url' ]; ?>
	
	<?php } elseif ( isset( $_GET[ 'redirect_to' ] ) ) { ?>
		
		<?php $redirect = $_GET[ 'redirect_to' ]; ?>
	
	<?php } else { ?>
		
		<?php $redirect = chumly_profile_url(); ?>
	
	<?php } ?>
	
	<?php if ( $_GET[ 'password' ] == 'changed' ) { ?>
		
		<?php chumly_alert( 'success', array( 'Password changed successfully.' ) ); ?>
	
	<?php } elseif ( $_GET[ 'login' ] == 'invalidkey' || $_GET[ 'login' ] == 'expiredkey' ) { ?>
		
		<?php chumly_alert( 'error', array( 'Please request a new password again.<br>If the problem persists, contact support.' ) ); ?>
	
	<?php } ?>
	
	<?php chumly_login_form( array( 'redirect' => $redirect ) ); ?>


<?php } ?>

<?php do_action( 'chumly_after_content' ); ?>
