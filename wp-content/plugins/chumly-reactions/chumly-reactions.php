<?php
/**
 * Plugin Name: Chumly Reactions
 * Version: 1.0
 * Author: GloboTek
 * Author URI: https://globotek.net
 * Description: Add reactions to content such as liking.
 * Tags: Chumly, Users, Profiles, Groups, Social Networking, Instant Messaging
 * Initial Release Date: xx xxx 2018
 * Created by Matt Campbell
 * Date: February 2 2018
 */

function chumly_like_notification( $notification_sources ) {
	
	$notification_sources[ 'post_like' ] = plugin_dir_path( __FILE__ ) . '/notifications/post-like.php';
	
	return $notification_sources;
	
}

add_filter( 'chumly_notification_templates', 'chumly_like_notification' );

function chumly_reactions() {
	
	function chumly_like_button( $interactions ) {
		
		global $chumly, $post;
		
		$interactions[ 'like' ] = '<button class="button button--clear" data-post_id="' . $post->ID . '" data-interaction_action="like">
				<svg class="button__icon button__icon--left button__icon--large icon" aria-hidden="true">
					<use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="' . $chumly->plugin_uri . 'frontend/images/icons/svg-symbols.svg#heart"></use>
				</svg>
				Like
			</button>';
		
		return $interactions;
		
	}
	
	add_filter( 'chumly_feed_interactions', 'chumly_like_button' );
	
}

add_action( 'plugins_loaded', 'chumly_reactions', 10 );
	
function chumly_reactions_settings_tabs( $active_tab ){
	
	echo '<a href="?page=chumly-settings&settings-tab=features" class="nav-tab ' . ($active_tab == 'features' ? 'nav-tab-active' : '') . '">Features</a>';
	echo '<a href="?page=chumly-settings&settings-tab=maintenance" class="nav-tab ' . ($active_tab == 'maintenance' ? 'nav-tab-active' : '') . '">Maintenance</a>';

}

add_action('chumly_settings_menu', 'chumly_reactions_settings_tabs');


function chumly_reactions_settings_panels( $sections ){
	
	$sections[] = array(
		'case' => 'features',
		'sections' => 'chumly_feature_settings'
	);
	$sections[] = array(
		'case' => 'maintenance',
		'sections' => 'chumly_maintenance_settings'
	);
	
	return $sections;
	
}

add_filter('chumly_settings_sections', 'chumly_reactions_settings_panels');