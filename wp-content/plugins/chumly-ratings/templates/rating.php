<?php
/**
 * Created by PhpStorm.
 * User: matthew
 * Date: 20/7/18
 * Time: 6:29 PM
 */

$user_rating = chumly_get_user_rating( $post_id );
$average_rating = chumly_get_average_rating( $post_id );
?>

<div class="chumly">

    <div class="star-rating <?php _e( is_user_logged_in() && $interactive !== FALSE ? 'star-rating--interactive' : NULL ); ?>"
         data-module="chumly-star-rating" data-post="<?php echo get_the_ID(); ?>">
		
		<?php if( $show_average == TRUE ) { ?>

            <div class="star-rating__figure">
				<?php
				if( !empty( $average_rating ) && empty($user_rating) ) {
					
					echo $average_rating . '&nbsp;<span class="is-hidden--text">out of 5 rating</span>';
					
				} ?>
            </div>
		
		<?php } ?>

        <div class="star-rating__decor" role="presentation"> <!-- content is reversed for css hover trick -->
			
			<?php
			if( !empty( $user_rating ) && $interactive == TRUE ) {
				
				for( $ia = 5; $ia > $user_rating; $ia-- ) {
					echo '<span class="star-rating__icon" data-rating="' . $ia . '">' . chumly_get_icon( 'star-full' ) . '</span>';
					
				}
				
				for( $ib = 0; $ib < $user_rating; $ib++ ) {
					echo '<span class="star-rating__icon star-rating__icon--fill" data-rating="' . $ia . '">' . chumly_get_icon( 'star-full' ) . '</span>';
					$ia--;
				}
				
			} else {
			    
			    $rounded_average = round($average_rating, 1);
				
				for( $ia = 5; $ia > $rounded_average; $ia-- ) {
					echo '<span class="star-rating__icon star-rating__icon--muted" data-rating="' . $ia . '">' . chumly_get_icon( 'star-full' ) . '</span>';
				}
				
				for( $ib = 0; $ib < $rounded_average; $ib++ ) {
					echo '<span class="star-rating__icon star-rating__icon--fill" data-rating="' . $ia . '">' . chumly_get_icon( 'star-full' ) . '</span>';
					$ia--;
				}
				
			} ?>

        </div>

    </div>

</div>
