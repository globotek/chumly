<?php
/**
 * Created by PhpStorm.
 * User: matthew
 * Date: 23/7/18
 * Time: 2:28 PM
 */
function chumly_trigger_save_rating() {
	
	chumly_save_rating();
	
	chumly_die();
	
}

add_action( 'wp_ajax_chumly_trigger_save_rating', 'chumly_trigger_save_rating' );
add_action( 'wp_ajax_nopriv_chumly_trigger_save_rating', 'chumly_trigger_save_rating' );


function chumly_save_rating() {
	
	$user_id     = sanitize_text_field( intval( $_POST[ 'user_id' ] ) );
	$post_id     = sanitize_text_field( intval( $_POST[ 'post_id' ] ) );
	$user_rating = sanitize_text_field( intval( $_POST[ 'user_rating' ] ) );
	
	
	$user_ratings         = get_user_meta( $user_id, 'chumly_user_ratings', TRUE );
	$existing_user_rating = $user_ratings[ $post_id ];
	$rating_sum_total     = get_post_meta( $post_id, 'chumly_total_rating', TRUE );
	
	if ( !$user_ratings ) {
		$user_ratings = array();
	}
	
	if ( !$rating_sum_total ) {
		//$rating_sum_total = $user_rating;
	}
	
	$total_ratings_count = chumly_get_ratings_count( $post_id );
	
	
	if ( !array_key_exists( $post_id, $user_ratings ) ) {
		
		if ( !$total_ratings_count ) {
			$total_ratings_count = 1;
		} else {
			$total_ratings_count++;
		}
		
		$rating_sum_total = $rating_sum_total + $user_rating;
		update_post_meta( $post_id, 'chumly_total_ratings_count', $total_ratings_count );
		
	} else {
		
		$deducted_rating_sum_total = ( $rating_sum_total - $existing_user_rating );
		$rating_sum_total          = ( $deducted_rating_sum_total + $user_rating );
		
	}
	
	$average_rating = number_format( $rating_sum_total / $total_ratings_count, 1 );
		
	update_post_meta( $post_id, 'chumly_total_rating', $rating_sum_total );
	update_post_meta( $post_id, 'chumly_average_rating', $average_rating );
	
	
	$user_ratings[ $post_id ] = $user_rating;
	$post_ratings[]           = $user_rating;
	
	update_user_meta( $user_id, 'chumly_user_ratings', $user_ratings );
	
	echo $average_rating;
	
}

