<?php
/**
 * Plugin Name: Chumly Ratings
 * Author: GloboTek
 * Version: 1.0
 */
/**
 * Created by PhpStorm.
 * User: matthew
 * Date: 20/7/18
 * Time: 6:27 PM
 */
function chumly_ratings() {
	
	include_once( ABSPATH . 'wp-admin/includes/plugin.php' );
	
	if( !is_plugin_active( 'chumly/chumly.php' ) ) {
		
		function chumly_ratings_activation_error() {
			
			$class = 'notice notice-error';
			$message = __( 'Chumly Ratings requires Chumly Core plugin. Plugin deactivated.', 'chumly' );
			
			printf( '<div class="%1$s"><p>%2$s</p></div>', esc_attr( $class ), esc_html( $message ) );
			
			deactivate_plugins( plugin_basename( __FILE__ ) );
			
		}
		
		add_action( 'admin_notices', 'chumly_ratings_activation_error' );
		
	} else {
		
		function chumly_ratings_styles() {
			
			wp_enqueue_style( 'chumly-ratings-css', plugin_dir_url( __FILE__ ) . 'css/chumly-ratings.css' );
			
		}
		
		add_action( 'wp_enqueue_scripts', 'chumly_ratings_styles' );
		
		function chumly_ratings_scripts() {
			
			wp_enqueue_script( 'chumly-ratings-js', plugin_dir_url( __FILE__ ) . 'scripts/chumly-ratings.js', array( 'chumly_js_lib' ), '', TRUE );
			
		}
		
		add_action( 'wp_enqueue_scripts', 'chumly_ratings_scripts' );
		
		
		include_once( 'includes/ratings-functions.php' );
		
		
		function chumly_get_average_rating( $post_id = NULL ) {
			
			return get_post_meta( $post_id, 'chumly_average_rating', TRUE );
			
		}
		
		
		function chumly_get_user_rating( $post_id = NULL, $user_id = NULL ) {
			
			$user_ratings = get_user_meta( get_current_user_id(), 'chumly_user_ratings', TRUE );
			
			if( !empty($user_ratings[ $post_id ] ) ){
				
				return $user_ratings[ $post_id ];
				
			} else {
				
				return FALSE;
				
			}
			
		}
		
		
		function chumly_get_ratings_count( $post_id = NULL ) {
			
			return intval( get_post_meta( $post_id, 'chumly_total_ratings_count', TRUE ) );
			
		}
		
		
		function chumly_get_ratings_output( $interactive = TRUE, $show_average = FALSE, $post_id = NULL ) {
			
			if( $post_id == NULL ) {
				
				$post_id = get_the_ID();
				
			}
			
			ob_start();
			include( plugin_dir_path( __FILE__ ) . 'templates/rating.php' );
			$output = ob_get_clean();
			
			return $output;
			
		}
		
		
		function chumly_output_ratings( $interactive = TRUE, $show_average = FALSE, $post_id = NULL ) {
			
			echo chumly_get_ratings_output( $interactive, $show_average, $post_id );
			
		}
		
		function chumly_ratings_shortcode() {
			
			return chumly_get_ratings_output();
			
		}
		
		add_shortcode( 'chumly_ratings', 'chumly_ratings_shortcode' );
		
	}
	
}

add_action( 'plugins_loaded', 'chumly_ratings', 15 );