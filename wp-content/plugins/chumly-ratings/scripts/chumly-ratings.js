(function($) {
	
	/*------------------------------------*\
	    ANY
	    
	    This will return true if there are any items
	    in a jQuery collection.
	    
	    EXAMPLE
	    
	    var items = $(".item");
	    
	    if(items.any()) {
			console.log("YAY!");
		}
	\*------------------------------------*/
	
	$.fn.any = function() {
		return $(this).length > 0;
	}


    /*------------------------------------*\
        PARSE SETTINGS
        
        This will try and parse inline json settings as an object literal to pass into a plugin
        
        EXAMPLE
        
        <div class="item" data-settings='{"setting1": true}'></div>

        var item = $(".item"),
            settings = item.parseSettings();
        
        console.log(settings.setting1);
        
        returns true;

    \*------------------------------------*/
    $.fn.parseSettings = function () {

        var elem = $(this),
            response = {};

        if (elem.attr("data-settings")) {

            try {
                response = $.parseJSON(elem.attr("data-settings"));
            }
            catch (ex) {
                console.log("Check input data. Message: " + ex.message);
                return {};
            }
        }

        return response;
    };
    
}(jQuery));
/**
 * Created by matthew on 23/7/18.
 */
(function ($) {
	
	$.fn.chumlyRating = function () {

		var elem = $(this),
			post_id = elem.attr('data-post'),
			trigger = elem.find('span.star-rating__icon');

		var init = function () {

			trigger.on('click', function (event) {
				
				event.preventDefault();
				
				var star_rating = $(this).attr('data-rating');

				trigger.removeClass('star-rating__icon--muted star-rating__icon--fill');
				
				for(var count = 1; count <= star_rating; count++){
					
					$('span[data-rating="' + count + '"]').addClass('star-rating__icon--fill');
					
				}
				
				$.ajax({
					url: chumly_vars.ajax_url,
					type: 'POST',
					data: {
						'action': 'chumly_trigger_save_rating',
						'user_rating': star_rating,
						'post_id': post_id,
						'user_id': chumly_vars.user_id
					},
					success: function (data) {
						
						$('.star-rating__figure').html(data);
						
					}
				});
				
			});


			
		};
		
		if(chumly_vars.user_id > 0 && elem.hasClass('star-rating--interactive')) {
		
			init();
		
		}
		
	}
	
}(jQuery));

/*------------------------------------*\
    CHUMLY MASTER 
    
    This file includes the module placeholders system that allows modular 
    binding of custom methods / plugins etc. 
    
    EXAMPLE
    
    <div data-module='example1,example2'></div> 
    
    The above would meet two conditions in the below switch statement.
    
\*------------------------------------*/
var chumlyRatings = (function($) {
	
	// This method will run when the DOM is ready. 
	var init = function() {
		
		// Find any module placeholders 
		var modulePlaceholders = $('[data-module]');
		
		if(modulePlaceholders.any()) {
			
			// Loop each placeholder
			modulePlaceholders.each(function() {
				
				var elem = $(this),
					modules = elem.attr('data-module');
				
				// If any modules found	
				if(modules) {
					
					// Split on the comma 
					modules = modules.split(', ');
					
					// Loop each module key
					$.each(modules, function(i, module) {
						
						// Run switch to bind each module to each key
						switch(module) {
							
							case 'chumly-star-rating':
								
								elem.chumlyRating();
								console.log('Go!!!');
								break;
							
						}
						
					});
				}
			});
		}
		
	};
	
	return {
		init: init
	}
	
}(window.$));

// RUN!!
chumlyRatings.init();
//# sourceMappingURL=chumly-ratings.js.map
