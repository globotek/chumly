/**
 * Created by matthew on 23/7/18.
 */
(function ($) {
	
	$.fn.chumlyRating = function () {

		var elem = $(this),
			post_id = elem.attr('data-post'),
			trigger = elem.find('span.star-rating__icon');

		var init = function () {

			trigger.on('click', function (event) {
				
				event.preventDefault();
				
				var star_rating = $(this).attr('data-rating');

				trigger.removeClass('star-rating__icon--muted star-rating__icon--fill');
				
				for(var count = 1; count <= star_rating; count++){
					
					$('span[data-rating="' + count + '"]').addClass('star-rating__icon--fill');
					
				}
				
				$.ajax({
					url: chumly_vars.ajax_url,
					type: 'POST',
					data: {
						'action': 'chumly_trigger_save_rating',
						'user_rating': star_rating,
						'post_id': post_id,
						'user_id': chumly_vars.user_id
					},
					success: function (data) {
						
						$('.star-rating__figure').html(data);
						
					}
				});
				
			});


			
		};
		
		if(chumly_vars.user_id > 0 && elem.hasClass('star-rating--interactive')) {
		
			init();
		
		}
		
	}
	
}(jQuery));
