(function($) {

	$.fn.toggle = function() {
		var elem = $(this),
			targets = $('.js-toggle__target'),
			triggers = elem.find('.js-toggle__trigger'),
			reset = $('.js-toggle__reset'),
			settings = {
				activeClass: 'is-active',
				visibleClass: 'is-active'
			};

		var init = function() {
			
			triggers.off('click').on('click', function(evt) {

				evt.preventDefault();

				// Load trigger and target
				var trigger = $(this),
					target = null,
					relatedTriggers = triggers.filter('[data-target="' + trigger.attr('data-target') + '"]');
				
				// Find parent target
				if(trigger.attr('data-target') == "parent") {
					
					// Find target and add active class to force off method
					target = trigger.parents('.js-toggle__target');
					trigger.addClass(settings.activeClass);
				}
				
				// Standard target
				else {
					target = targets.filter('#' + trigger.attr('data-target'));
					
					// If there are multiple triggers targeting the same elem, just pass them all
					// to the toggle method for class toggling
					if(relatedTriggers.any()) {
						trigger = relatedTriggers;
					}


				}

				// Toggle menu state accordingly
				if(target.hasClass(settings.activeClass)) {
					toggle(trigger, target, 'off');
				}
				else {
					toggle(trigger, target, 'on');
				}

			});

			reset.off('click').on('click', function(evt) {
				evt.preventDefault();

				toggle('reset');
			});
		},

		toggle = function(trigger, target, state) {

			switch(state) {
				case 'off':
					target.removeClass(settings.visibleClass);
					trigger.removeClass(settings.activeClass);
					reset.removeClass(settings.visibleClass);
					
					if(trigger.hasClass('site-head__hamburger') && $(window).width() < 1025){
						
						$('.site-head__nav__list').css({
                           'height': '0',
                           'overflow': 'auto',
                           'padding-bottom': '0'
                        });
						
						$('html, body').css({
						    overflow: 'auto',
						    height: 'auto'
						});
					}
					
					
					break;
				case 'on':
					target.addClass(settings.visibleClass);
					trigger.addClass(settings.activeClass);
					reset.addClass(settings.visibleClass);
					
					
					if(trigger.hasClass('site-head__hamburger') && $(window).width() < 1025){
												
						if($('body').hasClass('admin-bar')){
							var height_reducer = $('#wpadminbar').height() + 50;
						} else {
							var height_reducer = '50';
						}
						
						var window_height = $( window ).height() - height_reducer;
												
						$('.site-head__nav__list').css({
							'height': window_height + 'px',
							'overflow': 'auto',
							'padding-bottom': '60px'
						});
						
						$('html, body').css({
						    overflow: 'hidden',
						    height: '100%'
						});
						

					}
					
					break;
				case 'reset':
				default:
					triggers.removeClass(settings.activeClass);
					targets.removeClass(settings.visibleClass);
					reset.removeClass(settings.visibleClass);
					break;
			}



			// Quick fix: 2016-02-05 to help with the modals on mobile
			// Since setting the body to fixed will make it seem to scroll up, a negative top value is used
			if(target.hasClass('modal') && state != 'reset') {
				var $body = $('body' ),
					isModalOpen = $body.hasClass('modal-open' ),
					windowScroll = window.pageYOffset,
					scrollTo = $body.data('scroll-to' ),
					$modalWindow = target.find('.modal__window');


				if(isModalOpen) {

					$body.removeClass('modal-open');

					if( scrollTo ) {
						$('html,body' ).scrollTop(scrollTo ).css('top', 'auto');
					}

				} else {
					$body.data('scroll-to', windowScroll);
					$body.addClass('modal-open');
					$body.css('top', -windowScroll);

					window.scrollTo(0,windowScroll);

				}
			}
		}

		init();
		return this;
	};

}(jQuery));