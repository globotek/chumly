(function($){

	var svg_cross = '<svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 32 32" class="icon"><path class="icon__primary" d="M32 27.4L27.4 32 16 20.6 4.6 32 0 27.4 11.4 16 0 4.6 4.6 0 16 11.4 27.4 0 32 4.6 20.6 16 32 27.4z"></path></svg>';

	var term_object = {
		    keyword: {},
		    duration: {},
		    intensity: {},
		    type: {},
		    teacher: {},
		    favorite: {},
		    category: {}
	    };

	$.fn.pyPaginate = function(){

		$('#load_more_posts').on('click', function(e){

			e.preventDefault();

			if($(this).attr('data-action') == 'load'){

				loadPosts(term_object, 'append');

			} else {

				$('html, body').animate({scrollTop: 0}, 800);
				return false;

			}

		});


		function loadPosts(term_object, pagination){
			var post_count = $('#post_count_field').val(),
			    post_type = $('#search_post_type').val();

			$.ajax({
				url: ajax_variables.ajax_url,
				type: 'POST',
				dataType: 'json',
				data:{
					'action': 'load_blog_posts',
					'term_object': term_object,
					'post_count': post_count,
					'post_type': 'post'
				},
				success: function(data){

					$('#post_count_field').val(data.post_count);
					$('#load_more_posts').css({'background': 'transparent', 'color': '#ff9b59', 'border': '2px solid #ff9b59'});
					$('#load_more_posts').hover(function () {
						$(this).css({'background': '#ff9b59', 'color': '#fff', 'text-decoration': 'none', 'outline': 'none'});
					}, function () {
						$(this).css({'background': 'transparent', 'color': '#ff9b59', 'border': '2px solid #ff9b59'});
					});


					if(data.posts !== null) {

						if(pagination === 'replace'){

							$('.content-tiles__list').empty().append(data.posts).hide().fadeIn('250');

						} else if(pagination === 'append'){

							$('.content-tiles__list').append(data.posts);
							$('.content-tiles__list').find($('.content-tiles__list .is-hidden')).fadeIn({
								'duration': '350',
								'start': function () {
									$(this).css('display', 'flex');
								}
							});

						}

					}


					if($('#post_count_field').val() == data.found_posts){

						$('#load_more_posts').html('Back to Top').attr('data-action', 'scroll').on('click', function(){
							if($('#load_more_posts').attr('data-action') == 'scroll'){
								$('html, body').animate({scrollTop: 0}, 800);
								return false;
							}
						});

					} else {

						$('#load_more_posts').html('View More').attr('data-action', 'load');

					}

				}
			}); // Close Ajax
		}


	};

}(jQuery));