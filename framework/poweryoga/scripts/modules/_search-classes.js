/**
 * Created by matthew on 16/8/18.
 */
(function ($) {

    var svg_cross = '<svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 32 32" class="icon"><path class="icon__primary" d="M32 27.4L27.4 32 16 20.6 4.6 32 0 27.4 11.4 16 0 4.6 4.6 0 16 11.4 27.4 0 32 4.6 20.6 16 32 27.4z"></path></svg>';

    var term_object = {
            keyword: {},
            duration: {},
            intensity: {},
            type: {},
            teacher: {},
            favorite: {},
            category: {}
        },
        archiveView = $('.videos__archive'),
        searchView = $('.videos__search-results'),
        libraryView = $('.videos__library'),
        activeClass = 'is-visible';

    console.log(term_object);

    $.fn.pySearch = function () {

        function delay(callback, ms) {
            var timer = 0;
            return function() {
                var context = this, args = arguments;
                clearTimeout(timer);
                timer = setTimeout(function () {
                    callback.apply(context, args);
                }, ms || 0);
            };
        }


        //Search for terms when key pressed on keyword search
        $('#keyword-search').keyup(delay(function() {
			term_object['keyword'] = $(this).val();
			$('#post_count_field').val('0');

			perform_search(term_object, 'replace');
		}, 250));

        $('input[type=search]').on('search', function () {

            //delete term_object['keyword'];
            //perform_search(term_object, 'replace');

        });


        /** Search for terms when a dropdown option is clicked, output UI prompt */
        jQuery('.filter-menu select').on('change', function () {

            var term_id = jQuery(this).val(), // Term ID we'll be searching for.
                term_title = jQuery(this).find($('option:selected')).html(), // Is the actual name of the term we'll be searching.
                term_type = jQuery(this).data('type'); // Come from the data-type attribute and is the parent term name.

            term_object[term_type][term_id] = term_title;
            term_object['keyword'] = $('#keyword-search').val();

            $('#post_count_field').val('0');

            perform_search(term_object, 'replace');

        });


        /** Search for terms having removed a term */
        $(document).on('click', '.bullet-list__item .button__icon', function (event) {

            event.preventDefault();

            var term_id = $(this).data('term_id'),
                term_type = $(this).data('term_type');

            delete term_object[term_type][term_id];

            $(this).parents('.bullet-list__item').fadeOut('250', function () {
                $(this).remove();
                $('#post_count_field').val('0');
                perform_search(term_object, 'replace');
            });

        });

        /** Clear search terms and reset search form & UI */
        $('#clear_filters').on('click', function (event) {

            event.preventDefault();

            term_object['keyword'] = '';
            $('#keyword-search').val('');
            $('#post_count_field').val(0);

            $.each(term_object, function (term_type, term_type_value) {
                if ($.isEmptyObject(term_type_value) === false) {
                    $.each(term_type_value, function (term_key, term_value) {

                        delete term_type_value[term_key];

                    });
                }
            });

            $('.bullet-list__item').fadeOut('250', function () {
                $(this).remove();
                $('#post_count_field').val('0');
            });

            hideSearchView();

        });


        $('#load_more_posts').on('click', function (e) {

            e.preventDefault();

            if ($(this).attr('data-action') == 'load') {

                perform_search(term_object, 'append');

            } else {

                $('html, body').animate({scrollTop: 0}, 800);
                return false;

            }

        });


        function showSearchView() {

            if (archiveView.hasClass(activeClass) || libraryView.hasClass(activeClass)) {

                searchView.addClass(activeClass);
                archiveView.add(libraryView).removeClass(activeClass);

            }

        }

        function hideSearchView() {

            if (searchView.hasClass(activeClass)) {

                searchView.removeClass(activeClass).find($('.class-tiles__list')).empty();
                archiveView.add(libraryView).addClass(activeClass);

            }

        }


        function perform_search(term_object, pagination) {

            var post_count = $('#post_count_field').val(),
                post_type = $('#search_post_type').val();

            $.ajax({
                url: ajax_variables.ajax_url,
                type: 'POST',
                dataType: 'json',
                data: {
                    'action': 'conduct_term_search',
                    'term_object': term_object,
                    'post_count': post_count,
                    'post_type': post_type
                },
                success: function (data) {

                    $('#ajax_output').html(data);

                    console.log(data);

                    $('#clear_filters').css({'opacity': '1'});
                    $('#clear_filters').hover(function () {
                        $(this).css({'opacity': '0.5'});
                    }, function () {
                        $(this).css({'opacity': '1'});
                    });

                    $('#post_count_field').val(data.post_count);
                    $('#load_more_posts').css({
                        'background': 'transparent',
                        'color': '#ff9b59',
                        'border': '2px solid #ff9b59'
                    });

                    $('#load_more_posts').hover(function () {
                        $(this).css({
                            'background': '#ff9b59',
                            'color': '#fff',
                            'text-decoration': 'none',
                            'outline': 'none'
                        });
                    }, function () {
                        $(this).css({'background': 'transparent', 'color': '#ff9b59', 'border': '2px solid #ff9b59'});
                    });

                    $('.filter-menu__item option[value="null"]').attr('selected', 'true');

                    $.each($('.filter-menu__item__text'), function () {
                        var placeholder_text = $(this).parent().find($('.filter-menu__item option[value="null"]')).data('title');
                        $(this).html(placeholder_text);
                    });


                    if (data.search == 1) {

                        $('.button-list__list').empty();

                        if (data.terms) {

                            $.each(data.terms, function (index, value) {

                                $('.button-list__list').append('\
							        <li class="bullet-list__item" data-term_id="' + value.term_id + '">\
								    <a href="#" role="button" class="button button--upper button--small button-icon">\
								    	<span class="button__content">' + value.term_title + '</span>\
								    	<span class="button__icon" data-icon="cross" role="presentation" data-term_id="' + value.term_id + '" data-term_type="' + value.term_type + '">' + svg_cross + '</span>\
								    </a>\
							    </li>\
							').hide().fadeIn('250');
                            });
                        }

                        $('.videos__features').hide();

                        showSearchView();

                    } else {

                        $('.videos__features').fadeIn('250');
                        $('.videos__inline, .videos__headline, .class-tiles').show();
                        hideSearchView();

                    }


                    if (data.posts !== null) {

                        if (pagination === 'replace') {

                            searchView.find($('.class-tiles__list')).empty().append(data.posts);

                            searchView.find($('.class-tiles__list .is-hidden')).fadeIn({
                                'duration': '350',
                                'start': function () {
                                    $(this).css('display', 'flex');
                                }
                            });

                        } else if (pagination === 'append') {

                            var newPosts = data.posts;

                            searchView.add(libraryView).add(archiveView).find($('.class-tiles__list')).append(newPosts);
                            searchView.add(libraryView).add(archiveView).find($('.class-tiles__list .is-hidden')).fadeIn({
                                'duration': '350',
                                'start': function () {
                                    $(this).css('display', 'flex');
                                }
                            });

                        }

                    } else {

                        searchView.find($('.class-tiles__list')).empty().append('<h3>Sorry, there\'s no classes for that search.</h3>');

                    }


                    if ($('#post_count_field').val() == data.found_posts) {
                        $('#load_more_posts').html('Back to Top').attr('data-action', 'scroll').on('click', function () {
                            if ($('#load_more_posts').attr('data-action') == 'scroll') {
                                $('html, body').animate({scrollTop: 0}, 800);
                                return false;
                            }
                        });
                    } else {
                        $('#load_more_posts').html('View More').attr('data-action', 'load');
                    }

                }
            }); // Close Ajax
        }

    }

}(jQuery));
