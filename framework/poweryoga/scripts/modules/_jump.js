(function ($) {

    $.fn.jump = function (options) {
        var elem = $(this),
            settings = {
                offsetPadding: 80,
                scrollSpeed: 800,
                target: []
            };

        // Main method
        var init = function () {

            // extend settings
            settings = $.extend(true, {}, settings, options);

            // if the trigger has already been bound, return out 
            if (elem.hasClass('is-bound')) {
                return;
            };

            // Store the target
            settings.target = $(elem.attr('href'));

            // If no target, return out
            if (settings.target.length < 1) {
                return;
            }
            else {

                // If there is some offset padding for the element, override the setting
                if (settings.target.attr('data-offset-padding')) {
                    settings.offsetPadding = parseInt(settings.target.attr('data-offset-padding'));
                }

                // Add the bound class to the trigger
                elem.addClass('is-bound');

                // If the window's current hash is this href, jump! 
                if (window.location.hash == elem.attr('href')) {
                    jump();
                }
            }

            // Bind click event
            elem.off('click').on('click', function (evt) {

                var trigger = $(this);

                // If pushstate is supported
                if (window.history && window.history.pushState) {

                    // prevent default event (stops blinkyness)
                    evt.preventDefault();

                    // Push the hash
                    history.pushState(null, null, trigger.attr('href'));
                }

                // 'Jump... For my love'
                jump();
            });

            return elem;
        },

        // Jump method
        jump = function () {

            // check the target again
            if (settings.target.length > 0) {

                // Scrolly scroll time
                $('html, body').animate({
                    scrollTop: settings.target.offset().top - settings.offsetPadding
                }, settings.scrollSpeed);
            }

        };

        // Fire main method
        init();
        return this;
    };
	
	// Bind to all the elements!!
	var bindJump = function () {
	
		$('.js-jump').each(function () {
			$(this).jump();
		});
	
	};
	
	$(document).ready(function () {
		// Fire global bind
		bindJump();
	
		// When new elements added to dom (ajax etc) run bind again
		$('body').on('DOMNodeInserted DOMNodeRemoved', function () {
			bindJump();
		});
	});

}(jQuery));