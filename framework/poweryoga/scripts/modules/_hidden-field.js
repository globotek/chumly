(function($) {
	
	$.fn.hiddenField = function() {
		var elem = $(this),
			input = elem.find('.js-hidden-field__input'),
			value = elem.find('.js-hidden-field__value'),
			settings = {
				changeEvent: 'change',
				pickerClass: '',
				activeClass: 'is-active',
				followUrl: false
			};
		
		var init = function() {
			
			// Check that it's worth even tiying up this code
			if(input.any() && value.any()) {
				
				// Set url setting
				if(input.attr('data-url')) {
					
					if(input.attr('data-url') == 'true') {
						settings.followUrl = true;
					}
				}
				
				// Set a timeout and update the display element's value
				setTimeout(function() {
					update();
				}, 50);

				// Run an update for every change
				input.on(settings.changeEvent, function() {
					update(settings.followUrl);
				});

				// Add active class on focus and remove on blur
				input.on('focus', function() {
					input.parent().addClass(settings.activeClass);
				}).
				on('blur', function() {
					input.parent().removeClass(settings.activeClass);
				});
			}
		},
		
		// Update value of display element. Follow if needed
		update = function(follow) {

			var val = input.val(),
				title = val,
				option = input.find(':selected');
			
			if(typeof(follow) == 'undefined') {
				follow = false;
			}

			if(val) {
				
				// Find selected option and try to get title
				if(option.any()) {
					if(option.attr('data-title')) {
						title = option.attr('data-title');
					}
				}
				
				// Set title
				value.text(title);

				// Check if not hash url and follow
				if(follow & val.indexOf('#') < 0) {
					window.location.href = val;
				}
			}
		};
		
		init();
		return this;
			
	};
	
}(jQuery));