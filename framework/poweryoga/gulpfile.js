/*------------------------------------
 MODULES
 ------------------------------------*/
var gulp        = require('gulp'),
    wrap        = require('gulp-wrap'),
    del         = require('del'),
    sass        = require('gulp-sass'),
    watch       = require('gulp-watch'),
    sync        = require('browser-sync').create(),
    reload      = sync.reload,
    sequence    = require('run-sequence'),
    autoprefix  = require('gulp-autoprefixer'),
    sourcemaps  = require('gulp-sourcemaps'),
    minify_css  = require('gulp-clean-css'),
    minify_svg  = require('gulp-svgmin'),
    svg_symbols = require('gulp-svg-symbols'),
    concat      = require('gulp-concat'),
    uglify      = require('gulp-uglify'),
    filesToJson = require('gulp-files-to-json'),
    svgmin      = require('gulp-svgmin');


/*------------------------------------
 GLOBAL VARS
 ------------------------------------*/

var PROJECT_SCSS    = 'scss/project/**/*.scss',
    PROJECT_SVG     = 'svg/*.svg',
    PROJECT_SCRIPTS = 'scripts',
    THEME_DIR       = '../../wp-content/themes/poweryoga',
    THEME_CSS       = THEME_DIR + '/css',
    THEME_SVG       = THEME_DIR + '/images/icons/',
    THEME_SCRIPTS   = THEME_DIR + '/scripts',
    THEME_PHP       = THEME_DIR + '/**/*.php';


gulp.task('process_scss', function () {
	
	return gulp.src(PROJECT_SCSS)
	.pipe(sass().on('error', sass.logError))
	.pipe(sourcemaps.init())
	.pipe(autoprefix({
		browsers: ['last 2 versions'],
		cascade:  false
	}))
	.pipe(sourcemaps.write('.'))
	.pipe(gulp.dest(THEME_CSS))
	.pipe(sync.reload({
		stream: true
	}));
	
});


// Find all SVG and smash into a js file
gulp.task('process_svg', function () {

	return gulp.src(PROJECT_SVG)
		.pipe(svg_symbols())
		//.pipe(filesToJson('_site-icons.js'))
		//.pipe(wrap('var site_icons = <%= (contents) %>' + ';'))
		.pipe(gulp.dest(THEME_SVG));

});


gulp.task('delete_svg_css', function () {

	del(THEME_SVG + '/*.css', {force: true});

});


gulp.task('process_script_libraries', function () {
	
	var sources = [
		PROJECT_SCRIPTS + '/lib/*.js'
	];
	
	return gulp.src(sources)
	.pipe(concat('lib.js'))
	.pipe(uglify())
	.pipe(gulp.dest(THEME_SCRIPTS));
	
});


gulp.task('process_scripts', function () {
	
	var sources = [
		PROJECT_SCRIPTS + '/_site-icons.js',
		PROJECT_SCRIPTS + '/_helpers.js',
		PROJECT_SCRIPTS + '/modules/*.js',
		PROJECT_SCRIPTS + '/app.js'
	];
	
	gulp.start('process_script_libraries');
	
	return gulp.src(sources)
	.pipe(sourcemaps.init())
	.pipe(concat('app.js'))
	.pipe(uglify())
	.pipe(sourcemaps.write('.'))
	.pipe(gulp.dest(THEME_SCRIPTS));
	
});


gulp.task('browser_sync', function () {
	
	sync.init({
		https:       false,
		open:        true,
		reloadDelay: 50
	});
	
});


gulp.task('default', function () {
	
	sequence('process_svg', 'process_scss', 'process_scripts', function () {
		
		gulp.start('browser_sync');
		
		gulp.watch(PROJECT_SVG, ['process_svg']);
		
		gulp.watch(PROJECT_SCSS, ['process_scss']);
		
		gulp.watch(PROJECT_SCRIPTS + '/**/*.js', ['process_scripts']);
		
		gulp.watch(THEME_PHP).on('change', reload);
		
		gulp.watch(THEME_SCRIPTS + '/**/*.js').on('change', reload);
	});
	
});


gulp.task('unwatch-php', function () {
	
	sequence('process_svg', 'delete_svg_css', 'process_scss', 'process_scripts', function () {
		
		gulp.start('browser_sync');
		
		gulp.watch(PROJECT_SVG, ['process_svg', 'delete_svg_css']);
		
		gulp.watch(PROJECT_SCSS, ['process_scss']);
		
		gulp.watch(PROJECT_SCRIPTS + '/**/*.js', ['process_scripts']);
		
		gulp.watch(THEME_SCRIPTS + '/**/*.js').on('change', reload);
	});
	
});


gulp.task('css', function () {
	
	gulp.start('browser_sync');
	
	gulp.start('process_scss');
	gulp.watch(PROJECT_SCSS, ['process_scss']);
	
	
});
