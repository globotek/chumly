/**
 * Created by matthew on 5/7/18.
 */
(function ($) {
	
	$.fn.chumlyCourseActions = function () {
		
		console.log('Course Actions');
		
		var elem = $(this),
			triggers = elem.find('.js-action__trigger'),
			courseID = elem.data('course_id'),
			userID = chumly_vars.user_id;
		
		var init = function () {
				
				triggers.off('click').on('click', function (event) {
					
					event.preventDefault();
					
					var trigger = $(this),
						moduleAction = trigger.data('module_action');
					
					if (moduleAction == 'update_course_playlist') {
						
						var updateAction = trigger.attr('data-update_action');
						
						if(updateAction == 'save') {
							
							
							updateCoursePlaylist(trigger, updateAction);
							
						} else {
							
							
							updateCoursePlaylist(trigger, updateAction);
							
						}
							
					}
					
				});
				
			},
			updateCoursePlaylist = function ( trigger, updateAction ) {
				console.log(updateAction);
				$.ajax({
					url: chumly_vars.ajax_url,
					type: 'POST',
					data: {
						'action': 'chumly_update_course_playlist',
						'course_id': courseID,
						'user_id': userID,
						'update_action': updateAction
					},
					success: function (data) {
						
						if(updateAction == 'save') {
							console.log('Add to playlist');
							console.log(trigger);
							trigger.html('Remove from playlist').attr('data-update_action', 'delete');
						
						} else {
						 console.log(trigger);
							if(trigger.hasClass('button')) {
								console.log('Remove from playlist');
								trigger.html('Add to playlist').attr('data-update_action', 'save');
								
							} else if(trigger.is('i')){
								
								console.log('Icon delete');
								
								elem.fadeOut('250', function(){
									$(this).remove();
								});
								
							}
								
						}
					}
				});
								
			};
		
		init();
		
	}
	
}(jQuery));