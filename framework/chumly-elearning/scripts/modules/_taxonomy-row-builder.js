(function ($) {

	$.fn.chumlyTaxonomyRowBuilder = function() {
console.log('Go!!!');
		var index = 0,
		    count = 0,
		    ID, name;

		$('.elearning-builder ul').sortable({
			handle: '.elearning-builder-column-order',
			//cancel: '.row-content',
			axis: 'y',
			create: function () {
				update_elearning_order_count();
			},
			update: function () {
				update_elearning_order_count();
			}
		});

		$('.elearning-builder ul').disableSelection();


		function update_elearning_order_count() {

			count = 0;

			var builderList = $('.elearning-builder ul').children('li.elearning-builder-row');

			builderList.each(function () {

				var countIndex = count;

				count++;

				$(this).children('.elearning-builder-column-order').html('<span class="circle">' + count + '</span>');
				$(this).children('[name="elearning-subject[course_order]"]').val(count);

				$(this).children('.elearning-builder-id').attr('name', 'chumly-elearning-linked-children[' + countIndex + '][ID]');
				$(this).children('.elearning-builder-name').attr('name', 'chumly-elearning-linked-children[' + countIndex + '][name]');

			});


		}


		$('.elearning-builder-add').on('click', function (event) {
			console.log('new Row');
			event.preventDefault();

			var selection = $('.elearning-builder-selector select option:selected').get().reverse(),
			    lastRow   = $('.elearning-builder').find('.elearning-builder-row').last(),
			    deleteLastRow;

			index = $('input[name="total-elearning-rows"]').val();

			if (index == 0) {
				deleteLastRow = true;
			}

			$.each(selection, function (key, elem) {

				var elem   = $(elem),
				    newRow = lastRow.clone();

				ID = elem.val();
				name = elem.text();

				lastRow.after(newRow);

				$('input[name="total-elearning-rows"]').val(++index);
				newRow.find('.elearning-builder-id').val(ID);
				newRow.find('.elearning-builder-name').val(name);
				newRow.find('.elearning-builder-placeholder p').text(name);

				newRow.css('display', 'flex');


			});

			if (deleteLastRow) {
				lastRow.remove();
			}

			update_elearning_order_count();

		});


		$('.elearning-builder').on('click', 'a[href="#delete"]', function (event) {

			event.preventDefault();

			var row      = $(this).parents('.elearning-builder-row'),
			    rowCount = $(this).parents('.elearning-builder').find($('.elearning-builder-row')).length;

			console.log('Row Count', rowCount);

			if (rowCount > 1) {

				row.remove();

			} else {

				row.find($('.elearning-builder-id')).val('');
				row.hide();

			}

			var deleteIndex = $('input[name="total-elearning-rows"]').val();

			$('input[name="total-elearning-rows"]').val(--deleteIndex);

			update_elearning_order_count();

		});

	}


}(jQuery));