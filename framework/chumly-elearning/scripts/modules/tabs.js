(function ($) {

    $.fn.tabs = function () {
        
        var elem = $(this),
            triggers = elem.find(".js-tabs__trigger"),
            targets = elem.find(".js-tabs__target"),
            settings = {
                activeClass: "is-active"
            };

        var init = function () {

            if (triggers.any()) {

                // Attach click to triggers
                triggers.off("click").on("click", function (evt) {

                    evt.preventDefault();

                    // Load up the trigger, it's data, any related triggers and the target that we're looking for
                    var trigger = $(this),
                        data = trigger.attr("data-target"),
                        relatedtriggers = triggers.filter("[data-target='" + data + "']"),
                        target = targets.filter("#" + data);

                    // If the target is there and this trigger is not active
                    if (target.any() && !trigger.hasClass(settings.activeClass)) {

                        // Clean up targets and add active class to targeted target
                        targets.removeClass(settings.activeClass);
                        target.addClass(settings.activeClass);

                        // Clean up triggers and add active class to targeted trigger
                        triggers.removeClass(settings.activeClass);
                        trigger.addClass(settings.activeClass);

                        // Find triggers, if any add active class to them
                        if (relatedtriggers.length > 0) {
                            relatedtriggers.each(function () {
                                $(this).addClass(settings.activeClass);
                            });
                        }

                    }
                });
            }

            $(window).on("hashchange", function() {
                processUrl();
            });
        },

        // Try and find a trigger based on hash. If trigger found run click method
        processUrl = function () {

            var hash = window.location.hash,
                trigger = triggers.filter("[data-target='" + hash.replace("#", "") + "']");

                console.log(hash);

            if (trigger.length > 0) {

                if (trigger.length > 1) {
                    trigger = trigger.eq(0);
                }

                trigger.trigger("click");
            }

        };
        
        // run methods
        init();
        processUrl();

        return this;
    };

}(jQuery));