(function($) {

	$.fn.toggle = function() {
		var elem = $(this),
			targets = $('.js-toggle__target'),
			triggers = elem.find('.js-toggle__trigger'),
			reset = $('.js-toggle__reset'),
			settings = {
				activeClass: 'is-active',
				visibleClass: 'is-visible'
			};

		var init = function() {
			
			triggers.off('click').on('click', function(evt) {

				evt.preventDefault();

				// Load trigger and target
				var trigger = $(this),
					target = targets.filter('#' + trigger.attr('data-target'));

				// Toggle menu state accordingly
				if(trigger.hasClass(settings.activeClass)) {
					toggle(trigger, target, 'off');
				}
				else {
					toggle(trigger, target, 'on');
				}

			});

			reset.off('click').on('click', function(evt) {
				evt.preventDefault();

				toggle('reset');
			});
		},

		toggle = function(trigger, target, state) {

			switch(state) {
				case 'off':
					target.removeClass(settings.visibleClass);
					trigger.removeClass(settings.activeClass);
					reset.removeClass(settings.visibleClass);
					break;
				case 'on':
					target.addClass(settings.visibleClass);
					trigger.addClass(settings.activeClass);
					reset.addClass(settings.visibleClass);
					break;
				case 'reset':
				default:
					triggers.removeClass(settings.activeClass);
					targets.removeClass(settings.visibleClass);
					reset.removeClass(settings.visibleClass);
					break;
			}
		}

		init();
		return this;
	};

}(jQuery));