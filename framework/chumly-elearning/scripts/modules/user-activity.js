/**
 * Created by matthew on 5/7/18.
 */
(function ($) {
	
	$.fn.chumlyUpdateActivity = function () {
		
		var elem           = $(this),
		    moduleAction   = elem.data('module_action'),
		    objectID       = elem.data('object_id'),
		    parentObjectID = elem.data('parent_object_id');
		
		console.log('Activity Module');
		console.log(moduleAction);
		//console.log(postID);
		//console.log(user_id);
		
		var init               = function () {
			
			    //console.log('Single Lesson');
			    switch (moduleAction) {
				
				    case 'chumly_elearning_lesson_activity':
					    updateLatestLesson();
					    break;
				
				    case 'chumly_elearning_course_activity':
					    updateLatestCourse();
					    break;
				
			    }
			
			
		    },
		    updateLatestLesson = function () {
			
			    //console.log(chumly_vars.user_id);
			
			    $.ajax({
				    url:  chumly_vars.ajax_url,
				    type: 'POST',
				    data: {
					    'action':         'chumly_elearning_update_lesson_activity',
					    'post_id':        objectID,
					    'parent_post_id': parentObjectID,
					    'user_id':        chumly_vars.user_id
				    }
			    });
			
			
		    },
		    updateLatestCourse = function () {
			
			    console.log('Update Course', objectID);
			
			    $.ajax({
				    url:  chumly_vars.ajax_url,
				    type: 'POST',
				    data: {
					    'action':    'chumly_elearning_update_course_activity',
					    'course_id': objectID,
					    'user_id':   chumly_vars.user_id
				    }
			    });
			
		    };
		
		init();
		
	};
	
	
}(jQuery));
