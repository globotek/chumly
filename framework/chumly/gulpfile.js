/*------------------------------------
 MODULES
 ------------------------------------*/
var gulp        = require('gulp'),
    del         = require('del'),
    sass        = require('gulp-sass'),
    watch       = require('gulp-watch'),
    sync        = require('browser-sync').create(),
    reload      = sync.reload,
    sequence    = require('run-sequence'),
    autoprefix  = require('gulp-autoprefixer'),
    sourcemaps  = require('gulp-sourcemaps'),
    minify_css  = require('gulp-clean-css'),
    minify_svg  = require('gulp-svgmin'),
    svg_symbols = require('gulp-svg-symbols'),
    concat      = require('gulp-concat'),
    uglify      = require('gulp-uglify'),
    gutil       = require('gulp-util');

/*------------------------------------
 GLOBAL VARS
 ------------------------------------*/

var PROJECT_SCSS        = 'scss/project/**/*.scss',
    PROJECT_SVG         = 'svg/*.svg',
    PROJECT_SCRIPTS     = 'scripts',
    THEME_DIR           = '../../wp-content/plugins/chumly/frontend',
    THEME_ADMIN_DIR     = '../../wp-content/plugins/chumly/admin',
    THEME_CSS           = THEME_DIR + '/css',
    THEME_SVG           = THEME_DIR + '/images/icons/',
    THEME_SCRIPTS       = THEME_DIR + '/scripts',
    THEME_ADMIN_SCRIPTS = THEME_ADMIN_DIR + '/scripts',
    THEME_PHP           = '../../wp-content/plugins/chumly/**/*.php';


gulp.task('process_scss', function () {

	return gulp.src(PROJECT_SCSS)
		.pipe(sass().on('error', sass.logError))
		.pipe(sourcemaps.init())
		.pipe(autoprefix({
			browsers: ['last 2 versions'],
			cascade: false
		}))
		.pipe(sourcemaps.write('.'))
		.pipe(gulp.dest(THEME_CSS))
		.pipe(sync.reload({
			stream: true
		}));

});


gulp.task('process_svg', function () {

	return gulp.src(PROJECT_SVG)
		.pipe(svg_symbols())
		//.pipe(filesToJson('_site-icons.js'))
		//.pipe(wrap('var site_icons = <%= (contents) %>' + ';'))
		.pipe(gulp.dest(THEME_SVG));

});


gulp.task('delete_svg_css', function () {

	del(THEME_SVG + '/*.css', {force: true});

});


gulp.task('process_script_libraries', function () {

	var sources = [
		PROJECT_SCRIPTS + '/lib/*.js'
	];

	return gulp.src(sources)
		.pipe(concat('lib.js'))
		.pipe(gulp.dest(THEME_SCRIPTS));

});


gulp.task('process_scripts', function () {

	var sources = [
		PROJECT_SCRIPTS + '/_site_icons.js',
		PROJECT_SCRIPTS + '/_helpers.js',
		PROJECT_SCRIPTS + '/modules/*.js',
		PROJECT_SCRIPTS + '/chumly.js'
	];

	gulp.start('process_script_libraries');

	return gulp.src(sources)
		.pipe(sourcemaps.init())
		.pipe(concat('chumly.js'))
		//.pipe(uglify())
		.pipe(sourcemaps.write('.'))
		.pipe(gulp.dest(THEME_SCRIPTS));

});


gulp.task('process_admin_scripts', function () {

	var sources = [
		PROJECT_SCRIPTS + '/_site_icons.js',
		PROJECT_SCRIPTS + '/_helpers.js',
		PROJECT_SCRIPTS + '/admin-modules/*.js',
		PROJECT_SCRIPTS + '/chumly_admin.js'
	];

	gulp.start('process_script_libraries');

	return gulp.src(sources)
		.pipe(sourcemaps.init())
		.pipe(concat('chumly_admin.js'))
		//.pipe(uglify())
		.pipe(sourcemaps.write('.'))
		.pipe(gulp.dest(THEME_ADMIN_SCRIPTS));

});


gulp.task('browser_sync', function () {

	sync.init({
		https: false,
		open: true,
		reloadDelay: 50,
		ghostMode: false
	});

});


gulp.task('default', function () {

	sequence('process_svg', 'delete_svg_css', 'process_scss', 'process_scripts', 'process_admin_scripts', function () {

		gulp.start('browser_sync');

		gulp.watch(PROJECT_SVG, ['process_svg', 'delete_svg_css']);

		gulp.watch(PROJECT_SCSS, ['process_scss']);

		gulp.watch(PROJECT_SCRIPTS + '/**/*.js', ['process_scripts', 'process_admin_scripts']);

		gulp.watch(THEME_PHP).on('change', reload);

		gulp.watch(THEME_SCRIPTS + '/**/*.js').on('change', reload);

		gulp.watch(['js/production.min.js', 'css/**', 'images/optimized/**', 'index.html'], ['deploy-dev']);
	});

});


gulp.task('unwatch-php', function () {

	sequence('process_svg', 'delete_svg_css', 'process_scss', 'process_scripts', function () {

		gulp.start('browser_sync');

		gulp.watch(PROJECT_SVG, ['process_svg', 'delete_svg_css']);

		gulp.watch(PROJECT_SCSS, ['process_scss']);

		gulp.watch(PROJECT_SCRIPTS + '/**/*.js', ['process_scripts', 'process_admin_scripts']);

		gulp.watch(THEME_SCRIPTS + '/**/*.js').on('change', reload);
	});

});


gulp.task('css', function () {

	gulp.start('browser_sync');

	gulp.start('process_scss');
	gulp.watch(PROJECT_SCSS, ['process_scss']);


});


/**
 * Deploy task.
 * Copies the new files to the server
 *
 * Usage: `FTP_USER=someuser FTP_PWD=somepwd gulp ftp-deploy`
 */
gulp.task('ftp-deploy', function () {
	var conn = getFtpConnection()

	return gulp
		.src(localFilesGlob, {base: '../../', buffer: false})
		.pipe(conn.newer(remoteFolder)) // only upload newer files
		.pipe(conn.dest(remoteFolder))
});

/**
 * Watch deploy task.
 * Watches the local copy for changes and copies the new files to the server whenever an update is detected
 *
 * Usage: `FTP_USER=someuser FTP_PWD=somepwd gulp ftp-deploy-watch`
 */
gulp.task('ftp-deploy-watch', function () {
	var conn = getFtpConnection()

	gulp.watch(localFilesGlob).on('change', function (event) {
		console.log(
			'Changes detected! Uploading file "' + event.path + '", ' + event.type
		)

		return gulp
			.src([event.path], {base: '.', buffer: false})
			.pipe(conn.newer(remoteFolder)) // only upload newer files
			.pipe(conn.dest(remoteFolder))
	})
});