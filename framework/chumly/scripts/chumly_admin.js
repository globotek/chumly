jQuery(document).ready(function ($) {

	if ($('.chumly').length > 0) {

		$('.openDrawer').click(function (e) {

			e.preventDefault();

			//Expand or collapse this panel
			$(this).parents('tr').next().slideToggle(600);

		});

		$('.closeDrawer').click(function () {

			//Expand or collapse this panel
			$(this).parents('tr').slideToggle();

		});

	}

});