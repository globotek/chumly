/*------------------------------------*\
 CHUMLY MASTER
 
 This file includes the module placeholders system that allows modular
 binding of custom methods / plugins etc.
 
 EXAMPLE
 
 <div data-module='example1,example2'></div>
 
 The above would meet two conditions in the below switch statement.
 
 \*------------------------------------*/
var chumly = (function ($) {
	
	// This method will run when the DOM is ready. 
	var init = function () {
		
		// Find any module placeholders 
		var modulePlaceholders = $('[data-module]');
		
		if (modulePlaceholders.any()) {
			
			// Loop each placeholder
			modulePlaceholders.each(function () {
				
				var elem    = $(this),
				    modules = elem.attr('data-module');
				
				// If any modules found	
				if (modules) {
					
					// Split on the comma 
					modules = modules.split(', ');
					
					// Loop each module key
					$.each(modules, function (i, module) {
						
						// Run switch to bind each module to each key
						switch (module) {
							
							case 'chumly-alert':
								elem.chumlyAlert();
								break;
							
							case 'chumly-prompt':
								elem.chumlyPrompt();
								break;
							
							case 'chumly-tabs':
								elem.chumlyTabs();
								break;
							
							case 'chumly-toggle':
								elem.chumlyToggle();
								break;
							
							case 'chumly-upload-status':
								elem.chumlyUploadStatus();
								break;
							
							case 'chumly-comment-form':
								elem.chumlyCommentFormTriggers();
								break;
							
							case 'chumly-modal':
								elem.chumlyModal();
								break;
							
							case 'chumly-search':
								elem.chumlySearch();
								break;
							
							case 'chumly-friend-search':
								elem.chumlyFriendSearch();
								break;
							
							case 'chumly-notification':
								elem.chumlyNotification();
								break;
							
							case 'chumly-connect':
								elem.chumlyConnect();
								break;
						}
						
					});
				}
			});
		}
		
		// Load always run scripts
		misc();
		
	};
	
	return {
		init: init
	}
	
}(window.$));

// RUN!!

if (jQuery('.chumly').length > 0) {
	
	chumly.init();
	
}