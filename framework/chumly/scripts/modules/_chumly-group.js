/**
 * Created by matthew on 15/1/18.
 */
$('.update_group').on('click', function (e) {
	
	e.preventDefault();
	
	console.log('Saving Group');
	
	var trigger   = $(this),
	    form      = trigger.parents('form'),
	    group_id  = form.find('input[name="group_id"]').val(),
	    form_data = new FormData(form[0]);

	if (form.valid()) {
		

		trigger.val('Updating...').attr('disabled', 'disabled');
		$('.upload__meter').css('visibility', 'visible');
		form_data.append('action', 'chumly_save_group');
		form_data.append('group_id', group_id);
		
		$.ajax({
			url:         chumly_vars.ajax_url,
			type:        'POST',
			processData: false,
			contentType: false,
			cache:       false,
			dataType: 'json',
			data:        form_data,
			success:     function (data) {

				$('#saved_response').html(data);

				chumlyUploadFiles(form, data.linked_post_id, 'group', null, function () {
					
					//	$('#ajax_response').empty().append(data);
					trigger.val('Update Group').removeAttr('disabled');
					
				});
				
				
			}
		});
		
	} else {
		
		console.log('Invalid form');
		
	}
	
});


$('#delete_group').on('click', function (e) {
	
	e.preventDefault();
	
	var trigger  = $(this),
	    group_id = trigger.data('group_id');
		
	$.ajax({
		url:      chumly_vars.ajax_url,
		type:     'POST',
		dataType: 'json',
		data:     {
			'action':   'chumly_delete_group',
			'group_id': group_id
		},
		success:  function (data) {
						
			if (data === 1) {
				
				chumlyAlertModal(trigger, 'success', 'Group deleted successfully', function(){
					window.location.replace('/newsfeed');
				});
				
			} else {
				
				chumlyAlertModal(trigger, 'error', 'There was an error deleting the group.<br>Please try again or contact support.');
				
			}
			
		}
	})
	
});


/**
 * Delete a user from a group
 */
$('.delete_group_member').on('click', function (event) {
	
	event.preventDefault();
	
	var elem = $(this);
	
	$.ajax({
		url:     chumly_vars.ajax_url,
		type:    'POST',
		data:    {
			'action':   'chumly_remove_group_member',
			'group_id': elem.data('group_id'),
			'user_id':  elem.data('user_id')
		},
		success: function (data) {
			
			elem.parents('li').fadeOut(400, 'swing', function () {
				$(this).remove();
			});
			
		}
	});
});


var current_user_id   = chumly_vars.user_id,
    current_timestamp = $.now();


$('button.group_connection_action').on('click', function () {
	
	var trigger           = $(this),
	    membership_status = trigger.attr('membership-status'),
	    target_id         = trigger.attr('connection-id'),
	    connection_action = trigger.attr('connection-action');
		
	$.ajax({
		url:      chumly_vars.ajax_url,
		type:     'POST',
		dataType: 'json',
		data:     {
			action:            'chumly_update_membership_state',
			current_user:      current_user_id,
			status:            membership_status,
			group_id:          target_id,
			connection_action: connection_action
		},
		success:  function (data) {
			
			//$('.chumly').prepend('<pre>' + JSON.stringify(data, null, 2) + '</pre>');
			
			//$('.hero').after('<pre>' + JSON.stringify(data, null, 2) + '</pre>');
			
			//data = $.parseJSON(data);
			console.log(data);
			console.log('Complete', trigger);
			
			
			trigger.removeAttr('class')
			.addClass('button ' + data.css_class)
			.attr('membership-status', data.status)
			.attr('connection-id', data.group_id)
			.attr('connection-action', data.action)
			.html(data.button_label);
			
		}
	});
	
});


$('.approve_group_member').on('click', function (event) {
	
	event.preventDefault();
	
	var elem     = $(this),
	    user_id  = elem.data('user_id'),
	    group_id = elem.data('group_id');
	
	$.ajax({
		url:     chumly_vars.ajax_url,
		type:    'POST',
		data:    {
			action:   'chumly_approve_group_member',
			user_id:  user_id,
			group_id: group_id
		},
		success: function (data) {
			
			elem.html('Approved').removeClass('approve_group_member');
			elem.siblings('.decline_group_member').remove();
			
		}
	})
	
});


$('.decline_group_member').on('click', function (event) {
	
	event.preventDefault();
	
	var elem     = $(this),
	    user_id  = elem.data('user_id'),
	    group_id = elem.data('group_id');
	
	$.ajax({
		url:     chumly_vars.ajax_url,
		type:    'POST',
		data:    {
			action:   'chumly_decline_group_member',
			user_id:  user_id,
			group_id: group_id
		},
		success: function (data) {
			
			elem.html('Declined').removeClass('decline_group_member');
			elem.siblings('.approve_group_member').remove();
			
		}
	})
	
})

