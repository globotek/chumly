/**
 * Created by matthew on 15/11/17.
 */
/**
 * TRIGGER PROFILE SAVE PROCESS
 *
 * Run the processes to upload files and
 * update a user profile.
 */
$('.chumly').on('click', '.update_profile', function (event) {
	
	event.preventDefault();
	chumlyPrepareProfileForm(event, $(this));
	
});

/** PROCESS FORM
 *
 * Process anything that's required before the form is submitted
 * such as saving the tinyMCE content to the textarea.
 *
 */
function chumlyPrepareProfileForm(event, trigger) {
	
	event.preventDefault();
	
	var form          = trigger.parents('form'),
	    tinymceEditor = $(form).find('textarea.form__group__field--wysiwyg');
	
	if (tinymceEditor.length) {
		
		tinymce.get(tinymceEditor.attr('id')).save();
		
		chumlySaveProfile(trigger);
		
	} else {
		
		//console.log('No tinyMCE');
		chumlySaveProfile(trigger);
		
	}
	
}

/** SAVE PROFILE
 *
 * Save Chumly profile data
 *
 * @param event Event from trigger - e.g. click
 * @param trigger The element which has triggered the event
 */
function chumlySaveProfile(trigger) {
	
	var form     = trigger.parents('form'),
	    formData = new FormData(form[0]);
	
	formData.append('action', 'chumly_update_profile');
	
	if(acf) {
		acf.unload.active = false;
	}
	
	if (form.valid()) {
		
		trigger.val('Updating...').attr('disabled', 'disabled');
		
		$.ajax({
			url:         chumly_vars.ajax_url,
			type:        'POST',
			processData: false,
			contentType: false,
			cache:       false,
			data:        formData,
			success:     function (data) {
								
				if ($(form).find('input[type="file"]').length) {
					
					chumlyUploadFiles(form, 0, null, null, function (data) {
						
						if (data) {
							
							data.action = 'chumly_trigger_update_filename';
							
							$.ajax({
								url:     chumly_vars.ajax_url,
								type:    'POST',
								data:    data,
								success: function (data) {
									
									$('#upload_output').html(data);
									chumlyPrompt(trigger, 'success', 'Success!', "Your profile has been updated. <a href='/profile'>Go back to your profile?</a>");
									trigger.val('Update Profile').removeAttr('disabled');
									
								}
							});
							
						} else {
							
							chumlyPrompt(trigger, 'success', 'Success!', "Your profile has been updated. <a href='/profile'>Go back to your profile?</a>");
							trigger.val('Update Profile').removeAttr('disabled');
							
						}
						
						
						//chumlyUploadFiles(form, 0, null, null, function () {
							
							//chumlyPrompt(trigger, 'success', "Profile Updated <a href='../'>Back to profile</a>");
						//	trigger.val('Update Profile').removeAttr('disabled');
							
						//});
						
					});
					
				} else {
					
					chumlyPrompt(trigger, 'success', 'Success!', "Your profile has been updated. <a href='/profile'>Go back to your profile?</a>");
					trigger.val('Update Profile').removeAttr('disabled');
					
				}
				
			}
			
		});
		
	}
	
}
