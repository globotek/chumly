/**
 * Created by matthew on 6/2/18.
 */

/**
 * When comment Reply button is clicked, focus the reply textarea
 */
$('.comments').on('click', '.chumly-toggle__trigger', function () {

    $(this).parent().find('textarea').focus();
    
});


/**
 * Focus & scroll to comment textara when Comment interaction button is clicked
 */
$('.chumly').on('click', '[data-interaction_action="comment"]', function (event) {
    var post_id = $(this).data('post_id'),
        elem    = $('[data-comment_thread="' + post_id + '"]');

    elem.chumlyScrollTo(function () {
        elem.parents('.news-feed__item').find('.news-feed__item__comment-form textarea').focus();
    });

});


/**
 * Trigger save mechanism for comments
 */
$.fn.chumlyCommentFormTriggers = function () {

    var elem = $(this);

    elem.on('keydown', 'textarea', function (event) {

        if (event.keyCode == 13 && !event.shiftKey) {
    
            chumlyTriggerCommentSave($(this));

        }
        
    });
    
    elem.find('button').on('click', function(event){
       
        event.preventDefault();
    
        chumlyTriggerCommentSave(elem.find('textarea'));
        
    });

};


/**
 * Save a comment
 *
 * @param elem Textarea containing comment content
 */
function chumlyTriggerCommentSave(elem) {

    event.preventDefault();

    if (elem.val().length > 0) {

        var comment      = elem.val(),
            post_id      = elem.data('post_id'),
            username     = elem.data('username'),
            user_id      = elem.data('user_id'),
            reply_parent = elem.parents('.comments__form--reply').data('reply_parent');
        
        elem.val('');

        $.ajax({
            url: chumly_vars.ajax_url,
            method: 'POST',
            data: {
                'action': 'chumly_trigger_save_comment',
                'comment_content': comment,
                'post_id': post_id,
                'user_id': user_id,
                'comment_author': username,
                'comment_parent': reply_parent
            },
            success: function (data) {

                if (reply_parent) {

                    var elem = $('#nested_comments_' + reply_parent);
                    elem.append(data).chumlyScrollTo();
                    
                } else {

                    var elem = $('.new_comment_anchor[data-comment_thread="' + post_id + '"]');
                    elem.before(data).chumlyScrollTo();

                }

                $('.comments__item__inner').chumlyToggle().chumlyCommentFormTriggers();
                autosize($('textarea'));

            }
        });


    }

}

