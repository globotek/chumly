/**
 * Created by matthew on 25/7/17.
 */
var search_results    = $('.search__results'),
    recipients_list   = $('.message-centre__feed__recipients .user-list'),
    message_window    = $('.message-centre__feed__content'),
    message_container = $('.message-centre__feed__content__inner');

function chumlyScrollMessages(duration) {
	$(message_window).animate({scrollTop: $(message_window)[0].scrollHeight}, duration);
}

if ($('.message-centre').length) {
	chumlyScrollMessages(0);
}

// Search for users
$('#find_recipient').on('keyup', function () {
	
	var elem         = $(this),
	    query_string = elem.val();
	
	if (elem.val() != '') {
		
		recipients_list.hide();
		search_results.show();
		
	} else {
		
		recipients_list.show();
		search_results.hide();
		
	}
	
	$.ajax({
		url:     chumly_vars.ajax_url,
		type:    'POST',
		data:    {
			action:        'chumly_search_members',
			query:         query_string,
			output_option: 'message_center'
		},
		success: function (data) {
			$('.search__results').html(data);
		}
	});
});


// Start conversation between users or load existing
$('.message-centre').on('click', '.user-list__item', function (event) {
	
	event.preventDefault();
	
	var elem = $(this);
	
	$('#find_recipient').val('');
	$('.message-centre .user-list__item').removeClass('is-active');
	
	elem.find('.unread_state').attr('class', 'icon unread_state is-hidden');
	elem.find('.read_state').attr('class', 'icon read_state is-visible');
	
	elem.removeClass('is-notification').addClass('is-active');
	
	search_results.hide();
	recipients_list.show();
	
	var trigger   = elem,
	    thread    = trigger.attr('thread_id'),
	    recipient = trigger.attr('receiver_id');
	
	$.ajax({
		url:      chumly_vars.ajax_url,
		type:     'POST',
		dataType: 'json',
		data:     {
			action:       'chumly_trigger_conversation',
			thread_id:    thread,
			recipient_id: recipient,
			sender_id:    chumly_vars.user_id
		},
		success:  function (data) {
			
			//console.log(data);
			
			$('#recipient_id').val(recipient);
			$('#thread_id').val(data.thread_id);
			$('.message-centre__feed__content__inner').html(data.messages);
			
			if (data.new_conversation == 1) {
				
				$('.message-centre__feed__recipients').find('.user-list').prepend('<li class="user-list__item is-active"  receiver_id="' + recipient + '" thread_id="' + data.thread_id + '">' +
					'<a href="#" class="user-list__item__inner user-list__item__inner--media-icon" role="button">' +
					'<div class="user-list__item__media user-list__item__media--small">' +
					'<figure class="avatar">' +
					'<img class="avatar__image" src="' + data.avatar_url + '"/>' +
					'</figure>' +
					'</div>' +
					'<div class="user-list__item__text">' +
					'<span class="user-list__item__text--primary">' + data.username + '</span>' +
					'</div>' +
					'<div class="user-list__item__icon">' +
					'<svg class="icon" aria-hidden="true">' +
					'<use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="' + chumly_vars.plugin_url + 'frontend/images/icons/svg-symbols.svg#angle-right"></use>' +
					'</svg>' +
					'</div>' +
					'</a>' +
					'</li>'
				);
				
			} else {
				
				//console.log(data.thread_id);
				$('[thread_id=' + data.thread_id + ']').addClass('is-active');
				
			}
			
			chumlyScrollMessages(0);
			
		}
	});
	
});


function chumlyPollForMessage() {
	
	var lastLoadedMessage = message_container.find('.message').last().attr('timestamp');
	//console.log('polling');
	
	$.ajax({
		url:     chumly_vars.ajax_url,
		type:    'POST',
		data:    {
			action:       'chumly_poll_new_message',
			last_message: lastLoadedMessage,
			receiver_id:  $('#recipient_id').val(),
			thread_id:    $('#thread_id').val()
		},
		success: function (data) {
			
			var scroll_height   = message_window[0].scrollHeight,
			    scroll_position = message_window.scrollTop(),
			    feed_height     = message_window.height();
			
			message_container.find('.message').last().after(data);
			
			if ((scroll_height - feed_height) == scroll_position) {
				//console.log('Bottom');
				chumlyScrollMessages(0);
			}
		}
	});
	
}


$('#poll_message').on('click', function (e) {
	e.preventDefault();
	chumlyPollForMessage();
});


if ($('.message-centre').length) {
	setInterval(function () {
		chumlyPollForMessage();
	}, 1500);
}


$('#message_editor').keypress(function (event) {
	
	if (event.keyCode == 13 && event.shiftKey) {
		var content = this.value;
		var caret   = getCaret(this);
		
		this.value = content.substring(0, caret);
		event.stopPropagation();
		
	} else if (event.keyCode == 13) {
		
		$('#send_message').click();
		return false;
		
	}
	
});

function getCaret(el) {
	if (el.selectionStart) {
		
		return el.selectionStart;
		
	} else if (document.selection) {
		
		el.focus();
		var r = document.selection.createRange();
		
		if (r == null) {
			
			return 0;
			
		}
		
		var re = el.createTextRange(),
		    rc = re.duplicate();
		
		re.moveToBookmark(r.getBookmark());
		rc.setEndPoint('EndToStart', re);
		
		return rc.text.length;
	}
	
	return 0;
}

$('#send_message').on('click', function (e) {
	
	e.preventDefault();
	var messageContent = $('#message_editor').val(),
	    userdata       = chumly_vars.user_data,
	    profileImage   = chumly_vars.avatar;
	
	$('#message_editor').val('');
	
	message_container.find('.message').last().after('' +
		'<div class="message" timestamp="' + Math.floor(new Date().getTime() / 1000) + '">' +
		'<div class="message__media">' +
		'<figure class="avatar">' +
		'<img class="avatar__image" src="' + profileImage + '">' +
		'</figure>' +
		'</div>' +
		'<div class="message__content">' +
		'<a class="message__sender" href="#">' + userdata.data.display_name + '</a>' +
		'<div class="message__body wysiwyg" style="white-space: pre">' +
		messageContent +
		'</div>' +
		'</div>' +
		'</div>'
	);
	
	chumlyScrollMessages(1000);
	
	if ($.trim(messageContent)) {
		$.ajax({
			url:  chumly_vars.ajax_url,
			type: 'POST',
			data: {
				action:          'chumly_send_message',
				message_content: messageContent,
				receiver_id:     $('#recipient_id').val(),
				thread_id:       $('#thread_id').val()
			}
		});
	}
});

