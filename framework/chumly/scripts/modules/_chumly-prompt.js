/**
 * Created by matthew on 18/10/18.
 */
(function($) {
	
	$.fn.chumlyPrompt = function(method) {
		var elem = $(this),
		    promptElem = $(),
		    closeButton = elem.find('.chumly-prompt__close'),
		    showButton = elem.find('.chumly-prompt__show'),
		    settings = {
			    activeClass: 'is-active',
			    destroyOnClose: true,
			    isAlertChildElem: false,
			    hideTimeout: 600
		    };

		var init = function(method) {

			    settings = $.extend(true, {}, settings, elem.parseSettings());

			    // Load alert elem based on setting
			    if(settings.isAlertChildElem) {
				    promptElem = elem.find('.chumly-prompt__elem');
			    } else {
				    promptElem = elem;
			    }

			    // If there's a close button, add a listener
			    if(closeButton.any()) {

				    closeButton.on('click', function(evt) {
					    evt.preventDefault();

					    toggle('close');

				    });

			    }

			    // The same for show button
			    if(showButton.any()) {

				    showButton.on('click', function(evt) {

					    evt.preventDefault();

					    toggle('show');
				    });

			    }

			    if(method == 'show'){
				    toggle('show');
			    }

		    },

		    // Hide or show alert element
		    toggle = function(command) {

			    switch(command) {
				    case 'hide':
				    default:

						scrollOn();
					    promptElem.removeClass(settings.activeClass);

					    // If required, wait a second then remove element
					    setTimeout(function() {

						    if(settings.destroyOnClose) {
							    elem.remove();
						    } else {
							    promptElem.css('visibility', 'hidden');
						    }

					    }, settings.hideTimeout);

					    break;

				    case 'show':

					    $(document).chumlyModal('close');
						scrollOff();
					    promptElem.css('visibility', 'visible').addClass(settings.activeClass);

					    break;
			    }
		    };
		
		init(method);

		return this;

	};
	
}($));


function chumlyPrompt (target, alertType, promptTitle, promptMessage, callback) {
	
	//console.log(chumly_vars);

	var output = $('<div class="chumly-prompt" data-module="chumly-prompt" data-settings=\'{"destroyOnClose": true, "isAlertChildElem": true}\'>' +
		//'<button class="button chumly-prompt__show">Show Default Prompt</button>' +
		'<div class="prompt chumly-prompt__elem prompt--' + alertType + '" role="alert" style="visibility: hidden">' +
		'<div class="prompt__window">' +
		'<div class="prompt__header">' +
		'<h3 class="prompt__heading">' + promptTitle + '</h3></div>' +
		'<div class="prompt__content wysiwyg">' +
		'<p>' + promptMessage + '</p>' +
		'</div>' +
		'<div class="prompt__button">' +
		'<button class="button chumly-prompt__close">Okay, thanks</button>' +
		'</div>' +
		'</div>' +
		'<div class="prompt__overlay chumly-prompt__close" aria-hidden="true"></div>' +
		'</div>' +
		'</div>');
		
	
	//var output = '<div class="alert__modal" data-module="chumly-modal">' +
	//	'<div class="modal is-active" style="visibility: visible">' +
	//	'<div class="modal__inner is-active" style="visibility: visible">' +
	//	'<div class="alert chumly-alert__elem is-active alert--' + alertType + '" data-module="chumly-alert" role="alert">' +
	//	'<div class="alert__content">' + message + '</div>' +
	//	'<button class="alert__close chumly-alert__close">' +
	//	'<span class="is-hidden--text">Click here to close this alert</span>' +
	//	'<svg aria-hidden="true" class="alert__content__icon icon" aria-hidden="true">' +
	//	'<use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="' + chumly_vars.plugin_url + 'frontend/images/icons/svg-symbols.svg#cross"></use>' +
	//	'</svg>' +
	//	'</button>' +
	//	'</div>' +
	//	'</div>' +
	//	'<div class="modal__mask chumly-modal__trigger"></div>' +
	//	'</div>' +
	//	'</div>';
	
	
	target.parents('.chumly').prepend(output);
	
	output.chumlyPrompt('show');
	
	if(callback){
		callback();
	}
	
	/*$.ajax({
		url:     chumly_vars.ajax_url,
		type:    'POST',
		data:    {
			'action':     'chumly_load_alert_modal',
			'alert_type': alertType,
			'message':    message
		},
		success: function (data) {
			//console.log(data);
			//modal.empty().append(data);
			
			target.parents('.chumly').append(data).chumlyModal().chumlyAlert(true);
			
			if (callback) {
				
				callback();
				
			}
			
		}
	});*/
	
}
	
