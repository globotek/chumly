/**
 * Created by matthew on 31/5/18.
 */
$('.chumly form').validate({
    errorClass: 'is-error'
});


$('input[type="submit"]').on('click', function (event) {

    var form = $(this).parents('form');

    if ($(this).data('validate') == 1 && !form.valid()) {

        event.preventDefault();

    }

});


$('#new_form_row').on('click', function (event) {

    event.preventDefault();

    var trigger = $(this),
        count = parseInt(trigger.data('count')) + 1,
        lastRow = trigger.parents('.button-group').prev(),
        newRow = lastRow.clone(),
        newInputs = newRow.find('input');

    lastRow.after(newRow);

    newRow.hide().fadeIn(150);

    if (count == 1) {
        newRow.append('<button class="button button--negative button--small delete_form_row"><i class="fa fa-minus-circle"></i></button>');
    }

    $.each(newInputs, function (key, element) {

        var input = $(element),
            inputName = input.attr('name').replace(/\[\d*]/g, '[' + count + ']');

        input.attr('name', inputName);
        input.val('');

    });

    trigger.data('count', count);

});


$('input[type="radio"]').on('click', function () {

    $(this).parents('.form__group').find('input[type="radio"]').removeClass('active').parent('label').removeClass('button--primary');
    $(this).addClass('active').parent('label').addClass('button--primary');

});


$('input[type="file"]').on('change', function () {

    var filepath = $(this).val(),
        filename = filepath.substring(filepath.lastIndexOf('\\') + 1)

    $(this).prev().html(filename);

});


$('input.datepicker').on('click', function () {

    $('.ui-datepicker').css('z-index', '10000');

});


$('body').on('click', '.delete_form_row', function (event) {

    event.preventDefault();

    var addTrigger = $('#new_form_row'),
        count = parseInt(addTrigger.data('count') - 1);

    $(this).parents('.form__group__inline').fadeOut(150, function () {
        $(this).remove();
    });

    addTrigger.data('count', count);

});

$('.datepicker').datepicker();