/**
 * Created by matthew on 13/6/18.
 */
// Search for users
(function ($) {
	
	$.fn.chumlyFriendSearch = function () {
		
		var elem = $(this),
			object_id = elem.data('object_id'),
			search_form = elem.find('form'),
			search_input = elem.find('.search__form__input'),
			search_output = elem.find('.search__output'),
			search_results = elem.find('.search__results'),
			search_mask = elem.find('.search__mask');
		
		var init = function () {
			    
				elem.on('keyup', function () {
					
					var query_string = search_input.val(),
						output = elem.attr('data-output');
					
					//console.log(query_string);
					
					if (query_string != '') {
						
						search_mask.show();
						search_results.show();
						$.ajax({
							url: chumly_vars.ajax_url,
							type: 'POST',
							data: {
								action: 'chumly_search_friends',
								query: query_string,
								object_id: object_id,
								output_option: output
							},
							success: function (data) {
								
								search_results.html(data);
								
							}
						});
						
					} else {
						
						search_results.hide();
						
					}
					
				});
				
				search_mask.on('click', function (event) {
					
					resetSearch();
					
				});
				
				search_results.on('click', '.list-view__item, .user-list__item', function (event) {
					
					event.preventDefault();
					
					var user_id = $(this).data('user_id'),
						search_result = $(this).find('.list-view__text__primary, .user-list__item__text span').html();
					
					
					resetSearch();
					search_input.focus();
					
					
					var svg = '<svg class="icon button__icon button__icon--right" aria-hidden="true">' +
						'<use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="' + chumly_vars.plugin_url + '/frontend/images/icons/svg-symbols.svg#cross"></use>' +
						'</svg>';

					search_output.append('<button type="button" class="button button-group__item button--small button--primary" data-user_id="' + user_id + '"><p>' + search_result + '</p>' + svg + '</button>');
					
				});
			
				search_output.on('click', '.button', function(){
					
					$(this).fadeOut('150', function(){
						$(this).remove();
					})
					
				});
			
			search_form.on('submit', function(event){
				
				event.preventDefault();
				
				
				$.each(search_output.find('button'), function(key, element){
					
					var user_id = $(element).data('user_id');
					
					$.ajax({
						url: chumly_vars.ajax_url,
						type: 'POST',
						data: {
							action: 'chumly_invite_group_member',
							user_id: user_id,
							group_id: object_id
						},
						success: function(data){
							
							resetSearch();
							search_output.empty();
							
						}
					})
					
				});
				
			});
				
			},
			resetSearch = function () {
				search_input.val('');
				search_mask.hide();
				search_results.hide();
			};
		
		init();
		
	}
	
	
}(jQuery));
	
