(function ($) {
	
	$.fn.chumlyUploadStatus = function () {
		var elem              = $(this),
		    statusRing        = elem.find('.js-upload-status__status-ring').get(0),
		    bodyElem          = $('body'),
		    completeDashArray = 0,
		    settings          = {
			    completeClass: 'is-complete',
			    errorClass:    'is-error',
			    activeClass:   'is-active'
		    };
		
		var init     = function () {
			
			    // Work out what our completed state will be
			    completeDashArray = parseFloat(statusRing.getAttribute('data-complete-dasharray'), 10);
			
			    // Bind triggers
			    bodyElem.on('chumly-upload-status-update-data', function (evt, data) {
				    update(data.percent);
			    });
			
			    bodyElem.on('chumly-upload-status-complete', complete);
			    bodyElem.on('chumly-upload-status-error', error);
			    bodyElem.on('chumly-upload-status-reset', function () {
				    reset(true);
			    });
		    },
		
		    update   = function (percent) {

			    elem.css('visibility', 'visible');
			
			    // Remove any classes
			    reset(false);
			
			    // Get the current percentage representation of the stroke dasharray
			    var currentVal = completeDashArray * percent;
			
			    // Find the difference between that and the complete value
			    var remainingVal = completeDashArray - currentVal;
			
			    // Set the attribute and increment the index
			    statusRing.setAttribute('stroke-dasharray', currentVal + ' ' + remainingVal);
		    },
		
		    // Show the upload has completed
		    complete = function () {
			    
			    //If we're at the end, add the complete modifier
			    //if (percent >= 1) {
			    elem.addClass(settings.completeClass);
			    statusRing.setAttribute('stroke-dasharray', completeDashArray + ' 0');
			    //return;
			    //}
		    },
		
		    // Something has gone wrong so set a visual error
		    error    = function () {
			
			    elem.removeClass(settings.completeClass).addClass(settings.errorClass);
			    statusRing.setAttribute('stroke-dasharray', completeDashArray + ' 0');
		    },
		
		    // Reset to factory settings
		    reset    = function (resetStroke) {
			
			    elem.removeClass(settings.completeClass).removeClass(settings.errorClass);
			
			    if (resetStroke) {
				    statusRing.setAttribute('stroke-dasharray', '0 ' + completeDashArray);
			    }
		    };
		
		init();
		return this;
	};
	
}($));