(function ($) {

    $.fn.chumlyTabs = function () {

        var elem = $(this),
            triggers = elem.find('.chumly-tabs__trigger'),
            targets = elem.find('.chumly-tabs__target'),
            settings = {
                activeClass: 'is-active',
                singleToggle: false // When this is true, when a trigger is clicked when it's active, nothing will happen
            };

        var init = function () {

            // Parse settings
            settings = $.extend(true, {}, settings, elem.parseSettings());

            if (triggers.any()) {

                // Attach click to triggers
                triggers.off('click').on('click', function (evt) {

                    evt.preventDefault();

                    // Load up the trigger, it's data, any related triggers and the target that we're looking for
                    var trigger = $(this),
                        targetID = (trigger.attr('href') ? trigger.attr('href') : '#' + trigger.attr('data-target')), // Load target id from href if link or data-target if button
                        relatedtriggers = triggers.filter('[data-target="' + targetID + '"],[href*="' + targetID + '"]'),
                        target = targets.filter(targetID);

                    // If the target is there and this trigger is not active
                    if (target.any() && !trigger.hasClass(settings.activeClass)) {

                        // Clean up targets and add active class to targeted target
                        targets.removeClass(settings.activeClass);
                        target.addClass(settings.activeClass);

                        // Clean up triggers and add active class to targeted trigger. Trigger a 'trigger_active' event for others modules to tap into.
                        // Trigger a 'parent_active' for when a module lives within a hidden tab element that might need to be visual to work (like a google map)
                        triggers.removeClass(settings.activeClass);
                        trigger.addClass(settings.activeClass).trigger('trigger_active').trigger('parent_active');

                        // Find triggers, if any add active class to them
                        if (relatedtriggers.length > 0) {
                            relatedtriggers.each(function () {
                                $(this).addClass(settings.activeClass);
                            });
                        }

                    }

                    // If the target is there but the trigger is active and singleToggle is set to false
                    else if ((target.any() && trigger.hasClass(settings.activeClass)) && !settings.singleToggle) {

                        // Remove active class from target
                        target.removeClass(settings.activeClass);

                        // Remove active class from the trigger
                        trigger.removeClass(settings.activeClass);

                        // Find triggers, if any remove active class
                        if (relatedtriggers.length > 0) {
                            relatedtriggers.each(function () {
                                $(this).removeClass(settings.activeClass);
                            });
                        }
                    }
                });
            }

            $(window).on('hashchange', function() {
                processUrl();
            });
        },

        // Try and find a trigger based on hash. If trigger found run click method
        processUrl = function () {

            var hash = window.location.hash,
                trigger = triggers.filter('[data-target="' + hash.replace('#', '') + '"],[href="' + hash + '"]');

            if (trigger.length > 0) {

                if (trigger.length > 1) {
                    trigger = trigger.eq(0);
                }

                trigger.trigger('click');
            }

        };

        // run methods
        init();
        processUrl();

        return this;
    };

}(jQuery));
