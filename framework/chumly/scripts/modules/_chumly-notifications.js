/**
 * Created by matthew on 21/6/18.
 */
(function ($) {
	
	$.fn.chumlyNotification = function () {
		
		var elem    = $(this),
		    trigger = elem.find('svg'),
		    ID      = elem.data('notification_id');
		
		var init                  = function () {
			
			    elem.on('click', 'a', function () {
							    
				    markNotificationsRead(ID);
				
			    });
			
			    trigger.on('click', function (event) {
				
				    var action = $(this).parent().data('action');
				
				    if (action) {
					    event.preventDefault();
				    }
				
				    if (action == 'mark_notifications_read') {
					
					    markNotificationsRead(ID);
					
				    }
				
			    });
			
		    },
		    markNotificationsRead = function (ID, callback) {
			
			    $.ajax({
				    url:     chumly_vars.ajax_url,
				    type:    'POST',
				    data:    {
					    action:          'chumly_mark_notification_read',
					    notification_id: ID
				    },
				    success: function (data) {
					
					    elem.removeClass('notification--unread');
					
					    var notificationIndicator = $('.user-menu__indicator.notifications'),
					        count                 = notificationIndicator.html();
					
					    if (count > 1) {
						
						    notificationIndicator.html((count - 1));
						
					    } else {
						
						    notificationIndicator.hide();
						
					    }
					
					    if(callback){
						    callback();
					    }
					    
				    }
			    });
			
		    }
		
		
		init();
		
	}
	
}(jQuery));