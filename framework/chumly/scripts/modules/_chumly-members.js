/**
 * Created by matthew on 16/1/18.
 */

$.fn.chumlyConnect = function () {

	var trigger = $(this).find('[ajax-trigger="member_connection_action"]');
	
	trigger.on('click', function (event) {
		
		var	request_receiver_id = trigger.attr('target-user'),
			request_sender_id = chumly_vars.user_id,
			request_timestamp = $.now(),
			connection_status = trigger.attr('connection-status'),
			connection_id = trigger.attr('connection-id'),
			connection_action = trigger.attr('connection-action');
		
		event.preventDefault();
		
		$.ajax({
			url: chumly_vars.ajax_url,
			type: 'POST',
			dataType: 'json',
			data: {
				action: 'chumly_update_connection_state',
				receiver_id: request_receiver_id,
				sender_id: request_sender_id,
				timestamp: request_timestamp,
				status: connection_status,
				connection_id: connection_id,
				connection_action: connection_action
			},
			success: function (data) {
				//$('#connection_ajax_response').html(data);
				
				if (!trigger.hasClass('dropdown__menu__item')) {

					trigger.removeAttr('class')
					.addClass('button button--small ' + data.css_class)
					.attr('connection-status', data.status)
					.attr('connection-id', data.connection_id)
					.attr('connection-action', data.connection_action)
					.html(data.button_label);
					
				} else {

					trigger.parents('.dropdown__inner').find('button').removeAttr('class')
					.addClass('button button--small ' + data.css_class)
					.attr('connection-status', data.status)
					.attr('connection-id', data.connection_id)
					.attr('connection-action', data.connection_action)
					.attr('target-user', data.target_user)
					.attr('ajax-trigger', 'member_connection_action')
					.html(data.button_label);
					
					trigger.parents('.dropdown__menu').remove();
					
				}
			},
			error: function (xhr, status, error) {
				var err = eval("(" + xhr.responseText + ")");

			}
		});
		
	});
	
};
	
