/**
 * IMAGE UPLOAD PREVIEW
 *
 * Trigger File Upload Image Preview
 */
var uploadPreview, hiddenFields, ajaxResponse;

$('.chumly').on('change', 'input[data-upload="true"]', function () {
	
	var trigger = $(this),
	    target  = trigger.parents('form');
	
	trigger.attr('data-new_file', 1);
	
	if (trigger.parents('.form__group').length > 0) {
		
		uploadPreview = trigger.parents('.form__group').find('.upload__preview');
		
	} else {
		
		uploadPreview = trigger.parents('form').find('.upload__preview');
		
	}
	
	hiddenFields = target.find('.hidden_upload_fields');
	ajaxResponse = target.find('.ajax_response');
	
	uploadPreview.empty();
	hiddenFields.empty();
	ajaxResponse.empty();
	
	var files   = (this.files),
	    cropper = $(this).data('croppable');
	
	for (var count = 0, file; file = files[count]; count++) {
		
		var file_type = chumlyDetermineFiletype(file);
		
		if (file_type == 'image') {
						
			chumlyPreviewImageUpload(file, count, cropper);
			
		} else {
			
			chumlyPrompt(trigger, 'negative', 'Invalid Filetype', 'This type of file is not permitted for uploading.');
			
		}
		
	}
	
});


/**
 * IMAGE PREVIEW
 *
 * Grab the image(s) and output in the preview area.
 */
function chumlyPreviewImageUpload(file, count, cropper) {
	
	var fileReader = new FileReader();
	
	fileReader.onload = function (e) {
		
		new ImageCompressor(file, {
			quality: .8,
			checkOrientation: true,
			success: function (result) {
				console.log(result);
				var imageSrc = URL.createObjectURL(result),
				    image    = '<img id="image_' + count + '" class="upload__preview__item__image" src="' + imageSrc + '" data-filename="' + file['name'] + '"/>';
				
				
				uploadPreview.append('<div id="upload_preview_item_' + count + '" class="upload__preview__item">' +
					image +
					'<div class="upload__meter" data-module="chumly-upload"></div>' +
					'</div>'
				);
				
				if (cropper) {
					chumlyFireImageCropper($('#image_' + count + ''), count);
				}
				
				$.ajax({
					url:     chumly_vars.ajax_url,
					type:    'GET',
					data:    {
						action: 'chumly_upload_meter'
					},
					success: function (data) {
						
						$('.upload__meter').html(data);
						uploadPreview.find($('[data-module="chumly-upload-status"]')).chumlyUploadStatus();
						
					}
				});
			}
		});
	};
	
	return fileReader.readAsDataURL(file);
	
}


/**
 * IMAGE ORIENTATION
 *
 * Determine what the EXIF orientation of the image is and
 * return relevant correction
 */
function chumlyOrientateImage(file, outputTarget, imgAttrs) {
	
	loadImage(file, function (canvas) {
		
		var image = new Image();
		image.src = canvas.toDataURL("image/png");
		
		$('#' + outputTarget).prepend(image);
		
		$.each(imgAttrs, function (attrName, attrValue) {
			
			$(image).attr(attrName, attrValue);
			
		})
		
	}, {orientation: true});
	
}


/**
 * SAVE FILES
 *
 * Save posts from the Post Form such as the
 * status post form on the default profile page.
 *
 * @param form Form that contains the file inputs
 * @param parent_post For what entity the file is for. e.g - profile, status post, IM message, group profile, gallery etc.
 *                 This is used for saving metadata such as the URL and attachment ID to link to a profile.
 */
function chumlyUploadFiles(form, parent_post, post_type, post_format, callback) {
	
	if ($(form).find('input[type="file"]').length == 0) {
		
		if (callback) {
			callback();
		}
		
	} else {
		
		$.each($(form).find('input[type="file"]'), function (key, input) {
			
			var media_classification = $(input).data('media_classification');
			
			if (input.files.length < 1 || $(input).attr('data-new_file') != 1) {
				
				if (callback) {
					callback();
				}
				
				
			} else {
				
				$.each(input.files, function (key, file) {
					
					var fileType   = chumlyDetermineFiletype(file),
					    uploadData = new FormData();
					
					uploadData.append('action', 'chumly_save_file');
					uploadData.append('media_classification', media_classification);
					uploadData.append('term', media_classification + '-' + $(input).attr('id'));
					uploadData.append('user_id', chumly_vars.user_id);
					uploadData.append('media_bucket', chumly_vars.media_bucket);
					uploadData.append('parent_post', parent_post);
					uploadData.append('field_id', $(input).attr('id'));
					
					var xhr = new XMLHttpRequest();
					if (fileType == 'image') {
						
						if ($('.upload__preview__item__image--cropped').data('filename', file['name']).length > 0) {
							
							var upload_source = $('.upload__preview__item__image--cropped[data-filename="' + file['name'] + '"]').attr('src');
							
							
						} else {
							
							var upload_source = $('.upload__preview__item__image[data-filename="' + file['name'] + '"]').attr('src');
							
						}
						
						xhr.open('GET', upload_source, true);
						xhr.responseType = 'blob';
						
					} else {
						
						var upload_source = $('.upload__preview__item__file[data-filename="' + file['name'] + '"]').attr('src');
						
						xhr.open('GET', upload_source, true);
						uploadData.append('file', file);
						
					}
					
					xhr.onload = function (e) {
						
						if (this.status == 400) {
							console.log('Error - Check with host.');
						}
						
						if (this.status == 200) {
							
							var imageBlob = this.response;
							
							if (fileType == 'image') {
								
								uploadData.append(0, imageBlob, file['name']);
								
							}
							
							$.ajax({
								url:         chumly_vars.ajax_url,
								type:        'POST',
								processData: false,
								contentType: false,
								dataType:    'json',
								cache:       false,
								data:        uploadData,
								xhr:         function () {
									//upload Progress
									var xhr = $.ajaxSettings.xhr();
									if (xhr.upload) {
										xhr.upload.addEventListener('progress', function (event) {
											
											var percent  = 0,
											    position = event.loaded || event.position,
											    total    = event.total;
											
											if (event.lengthComputable) {
												percent = Math.ceil(position / total).toFixed(2);
											}
											
											//update upload meter
											$('body').trigger('chumly-upload-status-update-data', [{percent: percent}]);
										}, true);
									}
									return xhr;
								},
								success:     function (data) {
									
									if (data.state == 'success') {
										
										$(input).attr('data-new_file', 0);
										
										$(input).trigger('chumly-upload-status-complete');
										
									} else if (data.state == 'error') {
										$('body').trigger('chumly-upload-status-error');
									}
									
									ajaxResponse.append(data);
									
									if (callback) {
										callback(data);
									}
									
								}
							});
						}
					};
					
					xhr.send(null);
					
				});
				
			}
			
		});
		
	}
	
}


/**
 * FILE TYPE ROUTER
 *
 * Determine what type of file we're uploading
 * and decide what happens from there.
 */
function chumlyDetermineFiletype(file) {
	
	return file['type'].split('/')[0];
	
}


/**
 * LOAD IMAGE CROPPER
 *
 * Not all files and images uploaded will require or
 * want to be cropped so we'll only load the cropping
 * mechanism conditionally. From here will follow on
 * the necessary mechanism to save the cropped image.
 */
function chumlyFireImageCropper(target, count) {
	
	ajaxResponse.empty();
	
	target.rcrop({
		grid: true,
		full: true
	});
	
	//console.log(target.rcrop('getValues'));
	
	target.on('rcrop-ready rcrop-changed', function () {
		
		chumlyProcessCrop(target, count);
		
	});
	
}


/** GET CROPPED IMAGE DIMENSIONS & COORDINATES
 *
 * Get the coordinates for our new image.
 */
function chumlyGetCropCoordinates(target) {
	
	return target.rcrop('getValues');
	
}


/**
 * PROCESS CROPPED IMAGE
 */
function chumlyProcessCrop(target, count) {
	
	var image_data   = chumlyGetCropCoordinates(target),
	    image_source = target.rcrop('getDataURL');
	
	$.ajax({
		url:     chumly_vars.ajax_url,
		type:    'POST',
		data:    {
			'action':           'process_crop',
			'image_source':     image_source,
			'image_id':         count,
			'image_height':     image_data.height,
			'image_width':      image_data.width,
			'image_x_position': image_data.x,
			'image_y_position': image_data.y
		},
		success: function (data) {
			
			var image_string = data,
			    image_blob   = chumlyImageStringBlob(image_string, 'image/png'),
			    image_src    = URL.createObjectURL(image_blob);
			
			$('.upload__preview__item').find('.upload__preview__item__image--cropped').remove();
			target.parents('.upload__preview__item')
			.append('<img class="upload__preview__item__image upload__preview__item__image--cropped" src="' + image_src + '" data-filename="' + target.data('filename') + '"/>');
			
		}
	}); // Close Ajax
}


/**
 * PREPARE UPLOAD OBJECTS
 *
 * We need to take the data we've input and prepare it
 * for being uploaded by getting our Ajax data objects
 * in the correct format as well as feed in any metadata
 * we'll need such as the page we're on.
 */
function chumlyPrepareFileUpload(count) {
	
	var files = $(document).find('#profile_image')[0].files,
	    data  = new FormData();
	
	
	for (var count = 0, file; file = files[count]; count++) {
		data.append('file_' + count, file);
	}
	
	return data;
	
}


/**
 * CREATE IMAGES FROM STRINGS
 *
 * We want to have a uniform way of interacting with the images
 * we're outputting to preview and working with. As such, all images
 * become Blobs because we can easily draw new images and have a uniform
 * image object to save via media_handle_upload and such like - be it cropped,
 * rotated, rotated & cropped or just vanilla!
 *
 * @param image_string base64_encoded image source string from FileReader
 * @param content_type What kind of image it is
 * @param slice_size As it suggests - byte slice length
 * @returns {*} A Blob
 */
function chumlyImageStringBlob(image_string, content_type, slice_size) {
	
	content_type = content_type || '';
	slice_size   = slice_size || 512;
	
	var byte_characters = atob(image_string),
	    byte_arrays     = [];
	
	for (var offset = 0; offset < byte_characters.length; offset += slice_size) {
		
		var slice        = byte_characters.slice(offset, offset + slice_size),
		    byte_numbers = new Array(slice.length);
		
		for (var i = 0; i < slice.length; i++) {
			byte_numbers[i] = slice.charCodeAt(i);
		}
		
		byte_arrays.push(new Uint8Array(byte_numbers));
		
	}
	
	return new Blob(byte_arrays, {type: content_type});
	
}
	
