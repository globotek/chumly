/**
 * Created by matthew on 24/10/18.
 */
function chumlyLoadFeedTemplate(post_id, post_format, post_type) {
	
	$.ajax({
		url:     chumly_vars.ajax_url,
		type:    'POST',
		data:    {
			action:      'chumly_load_feed_part',
			post_id:     post_id,
			post_format: post_format,
			post_type:   post_type
		},
		success: function (template) {
			//console.log('Loading template', template);
			$('.news-feed').prepend(template).chumlyCommentFormTriggers();
			$('.news-feed__item').first().chumlyScrollTo();

			$('body').find($('a[rel="prettyPhoto"]')).prettyPhoto({
				social_tools: ''
			});
			
		}
	});
	
}
