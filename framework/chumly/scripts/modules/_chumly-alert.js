(function ($) {

	$.fn.chumlyAlert = function (childElem) {

		scrollOff();
		
		if(!childElem){
			childElem = false;
		}

		var elem        = $(this),
		    alertElem   = $(),
		    closeButton = elem.find('.chumly-alert__close'),
		    showButton  = elem.find('.chumly-alert__show'),
		    settings    = {
			    activeClass:      'is-active',
			    destroyOnClose:   true,
			    isAlertChildElem: childElem,
			    hideTimeout:      600
		    };

		var init   = function () {

			    settings = $.extend(true, {}, settings, elem.parseSettings());

			    // Load alert elem based on setting
			    if (settings.isAlertChildElem) {
				    alertElem = elem.find('.chumly-alert__elem');
			    } else {
				    alertElem = elem;
			    }

			    // If there's a close button, add a listener
			    if (closeButton.any()) {

				    closeButton.on('click', function (evt) {
					    
					    evt.preventDefault();
						
					    toggle('close');

						scrollOn();

				    });
			    }

			    // The same for show button
			    if (showButton.any()) {

				    showButton.on('click', function (evt) {

					    evt.preventDefault();

					    toggle('show');
				    });
			    }

		    },

		    // Hide or show alert element
		    toggle = function (command) {

			    switch (command) {
				    case 'close':
				    case 'hide':
				    default:

				    	$(document).chumlyModal('close');

					    alertElem.removeClass(settings.activeClass);
					    
						alertElem.removeClass(settings.activeClass);
						
					    // If required, wait a second then remove element
					    setTimeout(function () {

						    if (settings.destroyOnClose) {
							    alertElem.remove();
						    } else {
							    alertElem.css('visibility', 'hidden');
						    }

					    }, settings.hideTimeout);

					    break;

				    case 'show':
						alertElem.css('visibility', 'visible').addClass(settings.activeClass);
						
					    break;
			    }
		    };

		init();
		return this;
	};

}($));