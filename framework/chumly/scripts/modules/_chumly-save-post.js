/**
 * Created by matthew on 25/7/17.
 */
/**
 * TRIGGER POST CREATION PROCESS
 *
 * Run the processes to upload files
 * and create a post.
 */
$('.chumly form.editor').find('input, textarea').on('change keyup', function (event) {
	//console.log(event);
	var form         = $(this).parents('form'),
	    text         = form.find('textarea'),
	    submitButton = form.find('#status_submit');
	
	if(event.keyCode != 13) {
		
		if ($.trim(text.val())) {
			
			submitButton.removeAttr('disabled');
			
		} else {
			
			submitButton.attr('disabled', 'disabled');
			
		}
		
		$.each(form.find('input[type="file"]'), function (index, value) {
			
			if (value.files.length > 0) {
				
				submitButton.removeAttr('disabled');
			}
			
		});
		
	}
	
});

$('#status_submit').on('click', function (event) {
	
	chumlySaveStatusPost(event, $(this), $(this).parents('form'));
	
});

$('.chumly form.editor textarea').on('keydown', function (event) {
	
	var form         = $(this).parents('form'),
	    text         = $(this),
	    submitButton = form.find('#status_submit');
	
	if (event.which == 13) {
		
		event.preventDefault();
		
		if ($.trim(text.val())) {
			
			chumlySaveStatusPost(event, submitButton, form);
			
		}
		
	}
	
});

$(document).on('click', '.share_post', function (event) {
	
	chumlyShareStatusPost(event, $(this), $(this).parents('.modal').find('.modal__body form'));
	
});


/**
 * SAVE POST
 *
 * Save posts from the Post Form such as the
 * status post form on the default profile page.
 *
 * @param event Event from trigger - e.g. click
 * @param trigger The element which has triggered the event
 */
function chumlySaveStatusPost(event, trigger, form) {
	/**
	 * Submit a post status to the database and load into news feed
	 */
	event.preventDefault();
	
	var form_data      = new FormData(form[0]),
	    upload_trigger = false,
	    post_format    = form.find('input[name="post_format"]').val();

	if(!trigger.prop('disabled')) {
		
		trigger.attr('disabled', 'disabled').text('Saving...');
		
		$.each($(form).find('input[type="file"]'), function (index, value) {
			
			if (value.files.length > 0) {
				
				form_data.append('has_files', true);
				post_format    = 'image';
				upload_trigger = true;
				
			}
			
		});
		
		form_data.append('action', 'chumly_save_post');
		
		if (post_format) {
			
			form_data.append('post_format', post_format);
			
		}
		
		form_data.append('target_user', chumly_vars.chumly_profile.id);
		
		$.ajax({
			url:         chumly_vars.ajax_url,
			processData: false,
			contentType: false,
			type:        'POST',
			cache:       false,
			dataType:    'json',
			data:        form_data,
			success:     function (data) {
				
				$('.news-feed--empty-prompt').remove();
				
				if (upload_trigger == true) {
					
					chumlyUploadFiles(form, data.parent_post, data.post_type, data.post_format, function () {

						chumlyLoadFeedTemplate(data.parent_post, data.post_format, data.post_type);
						//$('.editor__feedback').empty();
						$(form).trigger('reset');
						trigger.prop('disabled', 'disabled').text('Post Message');
						
					});
					
				} else {
					
					chumlyLoadFeedTemplate(data.parent_post, data.post_format, data.post_type);
					form.trigger('reset');
					//$('.editor__feedback').empty();
					trigger.prop('disabled', '').text('Post Message');
					
				}
				
			}
		})
		
	}
		
}


function chumlyShareStatusPost(event, trigger, form) {
	
	event.preventDefault();
	
	var form_data      = new FormData(form[0]),
	    upload_trigger = false,
	    modal          = trigger.parents('.modal__inner');
	
	trigger.prop('disabled', 'disabled').val('Sharing...').css('opacity', 0.6);
	
	var target_user_id  = [],
	    target_selector = trigger.parents('.modal').find('select[name="target_select"] option:selected').val(),
	    post_id         = $('.modal.is-active').find($('input[name="loaded_post_id"]')).val();
	
	if (!isNaN(target_selector)) {
		
		target_user_id.push(target_selector);
		
	}
	
	$.each($(form).find('input[type="file"]'), function (index, value) {
		
		if (value.files.length > 0) {
			
			form_data.append('has_files', true);
			post_format    = 'image';
			upload_trigger = true;
			
		}
		
	});
	
	$.each(modal.find('.form__group.is-active .search__output button').get(), function (index, value) {
		
		target_user_id.push($(value).data('user_id'));
		
	});
	
	form_data.append('action', 'chumly_share_post');
	form_data.append('post_format', 'quote');
	form_data.append('target_id', target_user_id);
	form_data.append('source_profile_id', chumly_vars.chumly_profile.id);
	form_data.append('source_user_id', chumly_vars.user_id);
	form_data.append('shared_content_id', post_id);
		
	$.ajax({
		url:         chumly_vars.ajax_url,
		processData: false,
		contentType: false,
		type:        'POST',
		cache:       false,
		dataType:    'json',
		data:        form_data,
		success:     function (data) {
						
			form.trigger('reset');
			trigger.prop('disabled', '').text('Share').css('opacity', 1);
			
			$(document).chumlyModal('close');
			chumlyPrompt(trigger, 'success', 'Success!', 'Post Shared');
			
		}
	});
	
	
}