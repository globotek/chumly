/**
 * Created by matthew on 25/7/17.
 */
function misc() {
	
	autosize($('textarea'));
	
	var userMenu   = $('.user-menu__text'),
	    sourceElem = $('.user-menu__text').parents('.chumly').parents('ul').find('a').first(),
	    fontFamily = sourceElem.css('font-family'),
	    fontSize   = sourceElem.css('font-size');
	
	userMenu.css('font-family', fontFamily);
	userMenu.css('font-size', fontSize);
	
	$("a[rel^='prettyPhoto']").prettyPhoto({
		social_tools: ''
	});
	
	
	tinymce.init({
		selector:                    '#profile_10',
		mode:                        "exact",
		elements:                    'pre-details',
		theme:                       "modern",
		skin:                        "lightgray",
		menubar:                     false,
		statusbar:                   false,
		toolbar:                     [
			"bold italic | alignleft aligncenter alignright | bullist numlist outdent indent | undo redo"
		],
		plugins:                     "paste",
		paste_auto_cleanup_on_paste: true,
		paste_postprocess:           function (pl, o) {
			o.node.innerHTML = o.node.innerHTML.replace(/&nbsp;+/ig, " ");
		}
	});
		
}