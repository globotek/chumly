/**
 * Created by matthew on 8/2/18.
 */
$.fn.chumlyScrollTo = function (callback) {
	
	var elem = $(this),
		elem_offset = elem.offset().top,
		elem_height = elem.height(),
		window_height = $(window).height(),
		dom = $('html, body'),
		offset = elem_offset - ((window_height / 2) - (elem_height / 2)) + 'px';
		
	dom.animate({
		scrollTop: offset
	}, 500, 'swing', function () {
		
		if (callback) {
			callback();
		}
		
	});
	
};
