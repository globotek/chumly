/**
 * Created by matthew on 8/2/18.
 */
(function ($) {		
	
	$.fn.chumlyModal = function (method) {
		
		var elem = $(this),
		    triggers = elem.find('.chumly-modal__trigger'),
			target = elem.find('.modal'),
			settings = {
				activeClass: 'is-active',
				visibleClass: 'is-active'
			};
		
			var init = function (method) {
				
				triggers.off('click').on('click', function (event) {

					event.preventDefault();
					
					var trigger = $(this);
					
					if (target.hasClass(settings.activeClass)) {
						toggle(target, 'off');
					} else {
						toggle(target, 'on');
						var postID = trigger.data('post_id');
						
						target.find('.modal__header, .modal__body').remove();
						
						loadPost(postID, function(){
							
							target.find($('input[name="loaded_post_id"]')).val(postID);
							target.find($('.chumly .search')).chumlyFriendSearch();
							toggleHeaderContent();
							target.find('.modal__footer').addClass('is-active');
							
						});
					}
					
				});
			},
			
			toggle = function (target, state) {
				
				switch (state) {
					case 'off':
					default:
	
						scrollOn();
						target.removeClass(settings.visibleClass);
						target.css('visibility', 'hidden').find('.modal__inner').removeClass(settings.visibleClass);
						
						break;
					case 'on':

						scrollOff();
						target.css('visibility', 'visible').addClass(settings.visibleClass);
						
						break;
				}
			},
			
			loadPost = function (postID, callback) {
				
				$.ajax({
					url: chumly_vars.ajax_url,
					type: 'POST',
					data: {
						'action': 'chumly_load_modal_body',
						'modal_template': target.data('modal_template'),
						'post_id': postID
					},
					success: function (data) {
						target.find('.modal__inner').addClass('is-active').prepend(data);
						callback();
						
					}
				});
				
			},
			
			toggleHeaderContent = function(callback){
				
				var selector = target.find($('select[name="target_select"]'));
				
				selector.on('change', function(){
					var targetElem = $('.' + $(this).val());
					
					target.find($('.search').parent().addClass('is-hidden').removeClass('is-active'));
					targetElem.removeClass('is-hidden').addClass('is-active');
					target.find($('.search__output')).empty();
					
					if(callback) {
						callback();
					}
				});
				
			};
		
		
		if (method == 'close') {
			
			toggle(target);
			
		} else if (method == 'open') {
			
			toggle(target, 'on');
			
		} else {
			
			init();
			
		}
		
		return this;
		
	};

}(jQuery));