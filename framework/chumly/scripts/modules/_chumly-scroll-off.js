/**
 * Created by alex on 31/10/18.
 */
var scrollOff = function () {
	
	var current = $(window).scrollTop();
	$(window).scroll(function() {
		$(window).scrollTop(current);
	});
	
};

var scrollOn = function () {
	
	$(window).off('scroll');
	
};
